        function validateLogin()
    {
        var login_uid = $("#login_uid").val();
        var login_pwd = $("#login_pwd").val();
        
        if(login_uid=="")
        {
            $("#response2").html("<em>Please Enter Your Email-ID</em>");$("#login_uid").focus(); 

        }
        if(login_pwd=="")
        {
            $("#response2").html("<em>Please Enter Your Password</em>");$("#login_pwd").focus();
        }

        $.post('actions.php',
        { 
             action:"loginAccount",login_uid:login_uid,login_pwd:login_pwd             
         }
         , validateLoginResponse
        );   
            function validateLoginResponse(response)
          {
              if(response.indexOf("Successfully")=="-1")
              {
                $("#response2").html(response);

              }
              else
              {
                window.location.href = "dashboard.php?menuid=1";
              }             
          }
        
    }


    function sendForgotMail()
{
    var forgot_email = $("#forgot_email").val();
    if(forgot_email=="")
    {
        $("#response1").html("<em>Please Enter Your Email-ID</em>");$("#forgot_email").focus();     
        exit();
    }
    else
    {
          $.post('actions.php',
        { 
             action:"forgotPassword",forgot_email:forgot_email
         }
         , sendForgotMailResponse
        );   
            function sendForgotMailResponse(response)
          {
              if(response.indexOf("Successfully")=="-1")
              {
                $("#response1").html(response);
              }
              else
              {
                alert(response);
              }             
          }
    }
}




// register form script

    $("#phone").keydown(function(e) {
    var oldvalue=$(this).val();
    var field=this;
    setTimeout(function () {
        if(field.value.indexOf('+91') !== 0) {
            $(field).val(oldvalue);
        } 
    }, 1);
});

      function createAccount()
    {
        var name = $("#name").val();
        var lname = $("#lname").val();
        var gender = $("#gender").val();
        var email = $("#email").val();
        var phone = $("#phone").val();        
        var pwd = $("#pwd").val();
        var re_pwd = $("#re_pwd").val();        
        var city = $("#city").val();        
        
        if(name=="")
        {
            $("#response").html("<em>Please Enter Your First Name</em>");$("#name").focus();            
            return;
        }
        if(lname=="")
        {
            $("#response").html("<em>Please Enter Your Last Name</em>");$("#lname").focus();            
            return;
        }
        if(gender=="")
        {
            $("#response").html("<em>Please Select Your Gender</em>");$("#gender").focus();            
            return;
        }
        if(email=="")
        {
            $("#response").html("<em>Please Enter Valid Email-ID</em>");$("#email").focus();
            return;
        }
        if(phone=="")
        {
            $("#response").html("<em>Please Enter Valid Mobile Number</em>");$("#phone").focus();
            return;
        }
        if(pwd=="")
        {
            $("#response").html("<em>Please Enter Password</em>");$("#pwd").focus();
            return;
        }
        if(pwd!=re_pwd)
        {
            $("#response").html("<em>Password and Confirm Password Mismatching</em>");$("#re_pwd").focus();
            return;
        }

        $.post('actions.php',
        { 
             action:"createAccount",name:name,lname:lname,gender:gender,
             email:email,pwd:pwd,re_pwd:re_pwd,mobile:phone,city:city
         }
         , createUserResponse
        );   
            function createUserResponse(response)
          {
              if(response.indexOf("Successfully")=="-1")
              {
                $("#response").html(response);
              }
              else
              {
                window.location.href = "verifyAccount.php";
              }             
          }
        
    }    