$(document).ready(function() {
    
    // button to top
    $(window).scroll(function() {
        if($(this).scrollTop() > 90){
            $('#toTop').stop().animate({
                bottom: '20px'    
                }, 500);
        }
        else{
            $('#toTop').stop().animate({
               bottom: '-100px'    
            }, 500);
        }
    });
    $('#toTop').click(function() {
        $('html, body').stop().animate({
           scrollTop: 0
        }, 500, function() {
           $('#toTop').stop().animate({
               bottom: '-100px'    
           }, 500);
        });
    });

    //alsdfsdf
    function setCookie(name, value, lifetime_days) {
        var exp = new Date();
        exp.setDate(new Date().getDate() + lifetime_days);
        document.cookie = name + '=' + value + ';expires=' + exp.toUTCString() + ';path=/';
    }
    
    function getCookie(name) {
        if(document.cookie) {
            var regex = new RegExp(escape(name) + '=([^;]*)', 'gm'),
            matches = regex.exec(document.cookie);
            if(matches) {
                return matches[1];
            }
        }
    }
    // show list if cookie exists
    if(getCookie('showfooter') == 5) {
        $('#footer').show();
    }   
    if(getCookie('showfooter') == 9) {
        $('#footer').hide();
    }  
    
    // click handler to toggle elements and handle cookie
    $('#footericon').click(function() {
        // check the current state
        if($('#footer').is(':hidden')) {
            // set cookie
            setCookie('showfooter', '5', 365);
        } else {
            // delete cookie
            setCookie('showfooter', '9', 365);
        }
        // toggle
        $('#footer').slideToggle('fast');
        return false;
    });

    // dropdown toggle
	$(".click-toggle,.btn-group,#userNav").click(function () {
        $('.click-toggle > ul').not($(this).children("ul")).hide();
        $(this).find("ul.dropdown-menu").toggle();
        $(this).find("#option-list").toggle();
        $(this).find(".userSubMenu").toggle();
    });
    $(".click-toggle,.hsds,#userNav").mouseleave(function(){
        $(this).find("ul.dropdown-menu").hide();
        $(this).find("#option-list").hide();
        $(this).find(".userSubMenu").hide();
    });

    // active menu static page
    $(".menuSection li").click(function(){
      $(".menuSection li").removeClass("active");
      $(this).addClass("active");
    });

    // tabs
    $("#tabs a:first-child").click(function(){
        $("#tabs a").removeClass("selected");
        $(this).addClass("selected");
        $("#tab-review").css("display", "none");
        $("#tab-description").css("display", "block");
    });
    $("#tabs a:last-child").click(function(){
        $("#tabs a").removeClass("selected");
        $(this).addClass("selected");
        $("#tab-description").css("display", "none");
        $("#tab-review").css("display","block");
    });

    // tabs page retailers
    $(".subMenu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("active");
        $(this).parent().siblings().removeClass("active");
        var tab = $(this).attr("href");
        $(".tabRetailer").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
    // dropdown menu responsive
    $('.rsp-cat').click(function(e){
        $(".drsp ul.rsp-listcat").toggleClass('menu-rsp-active');
        e.preventDefault();
    });

    // fancybox
    $(".various").fancybox({
        maxWidth    : 630,
        fitToView   : false,
        width       : '70%',
        height      : '70%',
        autoSize    : false,
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none'
    });
})