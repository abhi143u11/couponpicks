  var canvas;
  var GeometryShapeObj = (function() {
    var _curX, _curY;

    canvas = new fabric.Canvas("canvasArea");
    canvas.setHeight(300);
    canvas.setWidth(700);
    fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';

    canvas.on('object:selected', function(e) {
  //    console.log(canvas.getActiveObject().get('id'));
        //find the selected object type
        var objType = e.target.get('type');
        if (objType == 'line') {
            _curX = e.e.clientX;
            _curY = e.e.clientY;
        }
    });

    canvas.on('object:moving', function(e) {
      var objType = e.target.get('type');
      var objName = e.target.get('name');

      var p = e.target;

      /*If a circle is moved, change the size of the lines*/
      if (objType == 'circle') {
        if(objName == "sc1") {
          p.line && p.line.set({ 'x1': p.left, 'y1': p.top });
          p.line.setCoords();
        }else if(objName == "sc2") {
          p.line && p.line.set({ 'x2': p.left, 'y2': p.top });
          p.line.setCoords();
        }
        canvas.renderAll();
      }else if(objType == 'line'){
        var _curXm = (_curX - e.e.clientX);
          var _curYm = (_curY - e.e.clientY);

          p.circle1.set({
          'left': (p.circle1.left - _curXm),
          'top': (p.circle1.top - _curYm)
        });

        p.circle1.setCoords();

        p.circle2.set({
          'left': (p.circle2.left - _curXm),
          'top': (p.circle2.top - _curYm)
        });

        p.circle2.setCoords();

        p.circle1.line && p.circle1.line.set({
          'x1': p.circle1.left,
          'y1': p.circle1.top
        });

        p.circle2.line && p.circle2.line.set({
          'x2': p.circle2.left,
          'y2': p.circle2.top
        });
        
        p.circle1.line.setCoords();
        p.circle2.line.setCoords();

        _curX = e.e.clientX;
          _curY = e.e.clientY;
      }
    });


    function makeEndpoint(left, top, line1, line2, line3, line4) {
      var c = new fabric.Circle({
        left: left,
        top: top,
        strokeWidth: 1,
        radius: 1,
        fill: 'blue',
        stroke: 'blue'
      });

      c.hasControls = c.hasBorders = false;

      c.line1 = line1;
      c.line2 = line2;
      c.line3 = line3;
      c.line4 = line4;

      return c;
    }

    function makeLine(coords, name) {
        var l = new fabric.Line(coords, {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        name: name
        });

        l.hasBorders = l.hasControls = false;

        return l;
     }

     var drawTriangle = function() {

      var line1 = makeLine([ 250, 50, 150, 150 ], "l1"),
        line2 = makeLine([ 150, 150, 350, 150 ], "l2"),
        line3 = makeLine([ 350, 150, 250, 50], "l3");

      //canvas.add(line1, line2, line3);



      var endpoint1 = makeEndpoint(line1.get('x2'), line1.get('y2'), line1, line2),
        endpoint2 = makeEndpoint(line2.get('x2'), line2.get('y2'), line2, line3),
        endpoint3 = makeEndpoint(line3.get('x2'), line3.get('y2'), line3, line1);

      canvas.add(line1, line2, line3, endpoint1, endpoint2, endpoint3);

      
      canvas.on('object:moving', function(e) {
        var objType = e.target.get('type');

        var p = e.target;

        /*If a circle is moved, change the size of the lines*/
        if (objType == 'circle') {
          p.line1 && p.line1.set({ 'x2': p.left, 'y2': p.top });
          p.line2 && p.line2.set({ 'x1': p.left, 'y1': p.top });
          p.line3 && p.line3.set({ 'x1': p.left, 'y1': p.top });
          p.line4 && p.line4.set({ 'x1': p.left, 'y1': p.top });
          canvas.renderAll();
        } else if (objType == 'line') { //if a line is dragged, move the complete triangle
          var _curXm = (_curX - e.e.clientX);
              var _curYm = (_curY - e.e.clientY);

              for (var i = 0; i < canvas.getObjects('circle').length; i++) {
                var currentObj = canvas.getObjects('circle')[i];

            currentObj.set({
              'left': (currentObj.left - _curXm),
              'top': (currentObj.top - _curYm)
            });

            currentObj.setCoords();

            currentObj.line1 && currentObj.line1.set({
              'x2': currentObj.left,
              'y2': currentObj.top
            });
            currentObj.line2 && currentObj.line2.set({
              'x1': currentObj.left,
              'y1': currentObj.top
            });

            currentObj.line2.setCoords();
            currentObj.line1.setCoords();
            }
        
          _curX = e.e.clientX;
            _curY = e.e.clientY;
        }
      });
    }

    var drawCircle = function() {
      var c = new fabric.Circle({
        left: 200,
        top: 200,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        radius: 100,
        fill: '#fff'
      });

      canvas.add(c);
    };

    var drawParallelogram = function() {
      var p = new fabric.Polygon([
          {x: 100, y: 100},
          {x: 300, y: 100},
          {x: 400, y: 200},
          {x: 200, y: 200}
      ], {
        left: 200,
        top: 200,
        fill: '#fff',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
      });

      canvas.add(p);
    };

    var drawCube = function() {

      /*Draw squares*/
      var s1 = new fabric.Rect({
        left: 200,
        top: 200,
        fill: '#fff',
        width: 100,
        height: 100,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
      });

      var s2 = new fabric.Rect({
        left: 250,
        top: 150,
        fill: '#fff',
        width: 100,
        height: 100,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        opacity: 0.4
      });

      /*draw lines*/
      var l1 = new fabric.Line([150, 150, 200, 100], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

        var l2 = new fabric.Line([250, 150, 300, 100], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

      var l3 = new fabric.Line([150, 250, 200, 200], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        opacity: 0.4
        });

        var l4 = new fabric.Line([250, 250, 300, 200], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });       

      /*add to group*/
      var group = new fabric.Group([ s1, s2, l1, l2, l3, l4], {
      })

      canvas.add(group);

    };

    var drawCylinder = function() {
      /*draw lines*/
      var l1 = new fabric.Line([200, 200, 200, 300], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

        var l2 = new fabric.Line([300, 200, 300, 300], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

        /*draw arcs*/
        var e1 = new fabric.Ellipse({
        left: 250,
        top: 200,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        rx: 50,
        ry: 15,
        fill: '#fff'
      });

      var e2 = new fabric.Ellipse({
        left: 250,
        top: 300,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        rx: 50,
        ry: 15,
        fill: '#fff',
        opacity: 0.6
      });

        /*add to group*/
      var group = new fabric.Group([ l1, l2, e1, e2], {
      })

      canvas.add(group);

      console.log(canvas.getActiveObject().get('id'));
    };

    var drawSphere = function() {
      //Draw a circle
      var c = new fabric.Circle({
        left: 200,
        top: 200,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        radius: 100,
        fill: '#fff'
      });

      //Draw ellipse
      var e1 = new fabric.Ellipse({
        left: 200,
        top: 200,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        rx: 100,
        ry: 25,
        fill: '#fff',
        opacity: 0.6
      });

      //Add to group
      var group = new fabric.Group([c, e1], {
      })

      canvas.add(group);
    };

    var drawCone = function() {
      //Draw two lines
      /*draw lines*/
      var l1 = new fabric.Line([250, 150, 175, 300], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

        var l2 = new fabric.Line([250, 150, 325, 300], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

      //Draw ellipse
      var e1 = new fabric.Ellipse({
        left: 250,
        top: 300,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        rx: 75,
        ry: 15,
        fill: '#fff',
        opacity: 0.7
      });

      //Add to group
      var group = new fabric.Group([l1, l2, e1], {
      })

      canvas.add(group);
    };

    var addText = function() {
      var text = new fabric.IText('Edit text...', { 
        fontFamily: 'arial black',
        left: 100, 
        top: 100,
        fontSize: 16,
        fontWeight: 'normal'
      });

      canvas.add(text);
    };

    var addPoint = function() {
      var point = new fabric.Rect({
        left: 200,
        top: 200,
        fill: 'rgba(100,200,200)',
        width: 8,
        height: 8,
        stroke: 'rgba(100,200,200)',
        strokeWidth: 2
      });

      canvas.add(point);
    };  

    var drawPyramid = function() {
      //Draw parallelogram
      var p = new fabric.Polygon([
          {x: 200, y: 200},
          {x: 400, y: 200},
          {x: 300, y: 300},
          {x: 100, y: 300}
      ], {
        left: 200,
        top: 200,
        fill: '#fff',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        opacity: 0.6
      });

      //Draw two triangles and join parallelogram
      var t1 = new fabric.Polygon([
          {x: 225, y: 100},
          {x: 300, y: 300},
          {x: 100, y: 300}
      ], {
        left: 150,
        top: 150,
        fill: '#fff',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        opacity: 0.6
      });

      var t2 = new fabric.Polygon([
          {x: 175, y: 100},
          {x: 150, y: 200},
          {x: 350, y: 200}
      ], {
        left: 250,
        top: 100,
        fill: '#fff',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        opacity: 0.4
      });

      //Add to group
      var group = new fabric.Group([p, t1, t2], {
      })

      canvas.add(group);
    };

    var drawTrapezoid = function() {
      var p = new fabric.Polygon([
          {x: 200, y: 100},
          {x: 300, y: 100},
          {x: 400, y: 200},
          {x: 100, y: 200}
      ], {
        left: 200,
        top: 200,
        fill: '#fff',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
      });

      canvas.add(p);
    };

    var drawSquare = function(left, top, width, height) {
      var square = new fabric.Rect({
        left: left,
        top: top,
        fill: '#fff',
        width: width,
        height: height,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
      });

      canvas.add(square);
    };

    var drawRectangle = function() {
      var rect = new fabric.Rect({
        left: 200,
        top: 200,
        fill: '#fff',
        width: 200,
        height: 100,
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
      });
      canvas.add(rect);
    };

    function makeCircularEndpoint(left, top, name) {
      var c = new fabric.Circle({
        left: left,
        top: top,
        strokeWidth: 3,
        radius: 5,
        fill: 'rgba(100,200,200,0.7)',
        stroke: 'rgba(100,200,200,0.7)',
        name: name
      });

      c.hasControls = c.hasBorders = false;

      return c;
    };

    function makeTriangularEndpoint(left, top, name) {
      var c = new fabric.Polygon([
          {x: 200, y: 95},
          {x: 210, y: 100},
          {x: 200, y: 105}
      ], {
        left: left,
        top: top,
        fill: 'rgba(100,200,200,0.7',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        opacity: 0.8,
        name: name
      });

      c.hasControls = c.hasBorders = false;

      return c;
    };

    function makeSegmentLine(coords, name, circle1, circle2) {
        var l = new fabric.Line(coords, {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        name: name
        });

        l.hasBorders = l.hasControls = false;

        l.circle1 = circle1;
        l.circle2 = circle2;

        return l;
    }

    var drawSegment = function() {

      var e1 = makeCircularEndpoint(100, 100, "sc1");
      var e2 = makeCircularEndpoint(200, 200, "sc2");

      var line = makeSegmentLine([ 100, 100, 200, 200 ], "sline", e1, e2);

      e1.line = line;
      e2.line = line;
      
      canvas.add(line, e1, e2);
    };

    var drawRay = function() {
      var c = new fabric.Circle({
        left: 100,
        top: 100,
        strokeWidth: 3,
        radius: 5,
        fill: 'rgba(100,200,200,0.7)',
        stroke: 'rgba(100,200,200,0.7)'
      });

      var t = new fabric.Polygon([
          {x: 200, y: 95},
          {x: 210, y: 100},
          {x: 200, y: 105}
      ], {
        left: 200,
        top: 200,
        fill: 'rgba(100,200,200,0.7',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        angle: 45
      });

      var line = new fabric.Line([100, 100, 200, 200], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

        //Add to group
      var group = new fabric.Group([c, t, line], {
      })

      canvas.add(group);

      /*var e1 = makeCircularEndpoint(100, 100, "sc1");
      var e2 = makeTriangularEndpoint(200, 100, "sc2");

      var line = makeSegmentLine([ 100, 100, 195, 100 ], "sline", e1, e2);

      e1.line = line;
      e2.line = line;
      
      canvas.add(line, e1, e2);*/
    };

    var drawDualRay = function() {
      var t1 = new fabric.Polygon([
          {x: 200, y: 95},
          {x: 210, y: 100},
          {x: 200, y: 105}
      ], {
        left: 100,
        top: 100,
        fill: 'rgba(100,200,200,0.7',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        angle: 225
      });

      var t2 = new fabric.Polygon([
          {x: 200, y: 95},
          {x: 210, y: 100},
          {x: 200, y: 105}
      ], {
        left: 200,
        top: 200,
        fill: 'rgba(100,200,200,0.7',
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2,
        angle: 45
      });

      var line = new fabric.Line([100, 100, 200, 200], {
        stroke: 'rgba(100,200,200,0.7)',
        strokeWidth: 2
        });

        //Add to group
      var group = new fabric.Group([t1, t2, line], {
      })

      canvas.add(group);
    };

    var init = function() {
      /*Add listeners to each tool*/
      $("#square").on('click', function(e) {
        GeometryShape.drawSquare(200, 200, 100, 100);
      });

      $("#rectangle").on('click', function(e) {
        GeometryShape.drawRectangle();
      });

      $("#triangle").on('click', function(e) {
        GeometryShape.drawTriangle();
      });

      $("#circle").on('click', function(e) {
        GeometryShape.drawCircle();
      });

      $("#parallelogram").on('click', function(e) {
        GeometryShape.drawParallelogram();
      });

      $("#trapezoid").on('click', function(e) {
        GeometryShape.drawTrapezoid();
      });

      $("#cube").on('click', function(e) {
        GeometryShape.drawCube();
      });

      $("#cylinder").on('click', function(e) {
        GeometryShape.drawCylinder();
      });

      $("#cone").on('click', function(e){
        GeometryShape.drawCone();
      });

      $("#pyramid").on('click', function(e){
        GeometryShape.drawPyramid();
      });

      $("#sphere").on('click', function(e){
        GeometryShape.drawSphere();
      });

      $("#label").on('click', function(e){
        GeometryShape.addText();
      }); 

      $("#point").on('click', function(e){
        GeometryShape.addPoint();
      });   

      $("#segment").on('click', function(e) {
        GeometryShape.drawSegment();
      });

      $("#ray").on('click', function(e){
        GeometryShape.drawRay();
      });

      $("#line").on('click', function(e) {
        GeometryShape.drawDualRay();
      }); 


      $(document).on('keydown', function(e) {
        var charCode = (typeof e.which == "number") ? e.which : e.keyCode;
        if(charCode == 46) { //delete key
          var selectedObj = canvas.getActiveObject();
          canvas.remove(selectedObj);
        }
      });
    };

    init();

    return {
      drawTriangle: drawTriangle,
      drawSquare: drawSquare,
      drawRectangle: drawRectangle,
      drawCircle: drawCircle,
      drawParallelogram: drawParallelogram,
      drawTrapezoid: drawTrapezoid,
      drawCube: drawCube,
      drawCylinder: drawCylinder,
      drawCone: drawCone,
      drawPyramid: drawPyramid,
      drawSphere: drawSphere,
      addText: addText,
      addPoint: addPoint,
      drawSegment: drawSegment,
      drawRay: drawRay,
      drawDualRay: drawDualRay,
      init: init
    };

});


var prevFocus;
var GeometryShape;

function getFocussedTextArea() {  
  $("textarea").off('focus.mathkeyspace');

  $("textarea").on('focus.mathkeyspace', function() {
    prevFocus = $(this).parent().parent();
  });  
}

function bindGeometryKeys() {
  getFocussedTextArea();
  $('#canvasArea').show();
  GeometryShape = GeometryShapeObj();
}

function bindAlgebraKeys() {
  getFocussedTextArea();
  /*algebra*/
  $('#union_symbol').on('click', function(e) {
      prevFocus.mathquill('write', '\\cup').focus();
  });

  /*LTE*/
  $('#intersection_symbol').on('click', function(e) {
      prevFocus.mathquill('write', '\\cap').focus();
  });

  /*LT*/
  $('#euler_symbol').on('click', function(e) {
      prevFocus.mathquill('write', 'e^').focus();
  });

  /*GT*/
  $('#imaginary_symbol').on('click', function(e) {
      prevFocus.mathquill('write', '\\iota').focus();
  });

  /*Equal To*/
  $('#infinity_symbol').on('click', function(e) {
      prevFocus.mathquill('write', '\\infty').focus();
  });

  /*Parantheses*/
  $('#logn_symbol').on('click', function(e) {
      prevFocus.mathquill('write', 'log_?(?)').focus();
  });

}

function bindPrealgebraKeys() {
  getFocussedTextArea();
   /*Greate than Equal to*/
  $('#gte').on('click', function(e) {
      prevFocus.mathquill('write', '\\ge').focus();
  });

  /*LTE*/
  $('#lte').on('click', function(e) {
      prevFocus.mathquill('write', '\\le').focus();
  });

  /*LT*/
  $('#lt').on('click', function(e) {
      prevFocus.mathquill('write', '\\lt').focus();
  });

  /*GT*/
  $('#gt').on('click', function(e) {
      prevFocus.mathquill('write', '\\gt').focus();
  });

  /*Equal To*/
  $('#equal').on('click', function(e) {
      prevFocus.mathquill('write', '=').focus();
  });

  /*Divide*/
  $('#divide').on('click', function(e) {
      prevFocus.mathquill('write', '\\div').focus();
  });

  /*Pi*/
  $('#pi').on('click', function(e) {
      prevFocus.mathquill('write', '\\pi').focus();
  });

  /*Parantheses*/
  $('#parenthesis').on('click', function(e) {
      prevFocus.mathquill('write', '(?)').focus();
  });

  /*Brackets*/
  $('#brackets').on('click', function(e) {
      prevFocus.mathquill('write', '\\[?\\]').focus();
  });

  /*Absolute*/
  $('#absolute').on('click', function(e) {
      prevFocus.mathquill('write', '|?|').focus();
  });

  /*fraction*/
  $('#fraction').on('click', function(e) {
      prevFocus.mathquill('write', '\\frac{}{}').focus();
  });

  /*mixed number*/
  $('#mixed_number').on('click', function(e) {
      prevFocus.mathquill('write', '?\\frac{}{}').focus();
  });

  /*exponent*/
  $('#exponent').on('click', function(e) {
      prevFocus.mathquill('write', '^').focus();
  });

  /*subscript*/
  $('#subscript').on('click', function(e) {
      prevFocus.mathquill('write', '_').focus();
  });

  /*Square Root*/
  $('#squareroot').on('click', function(e) {
      prevFocus.mathquill('write', '\\sqrt{}').focus();
  });

  /*nroot*/
  $('#nroot').on('click', function(e) {
      prevFocus.mathquill('write', '\\sqrt[]{}').focus();
  });

  /*Point*/
  $('#point').on('click', function(e) {
      prevFocus.mathquill('write', '(?,?)').focus();
  });

  /*Scientific*/
  $('#scientific').on('click', function(e) {
      prevFocus.mathquill('write', '? \ 10^?').focus();
  });
}

function bindStatisticsKeys() {
  getFocussedTextArea();
  $('#alpha').on('click', function(e) {
      prevFocus.mathquill('write', '\\alpha').focus();
  });

  /*LTE*/
  $('#mu').on('click', function(e) {
      prevFocus.mathquill('write', '\\mu').focus();
  });

  /*LT*/
  $('#sigma').on('click', function(e) {
      prevFocus.mathquill('write', '\\sigma').focus();
  });

  /*GT*/
  $('#xbar').on('click', function(e) {
      prevFocus.mathquill('write', '\\bmatrix & b\\c & d\\end{bmatrix}').focus();

  });

  /*Equal To*/
  $('#permutation').on('click', function(e) {
      prevFocus.mathquill('write', '_{n}P_{k}').focus();
  });

  /*Divide*/
  $('#combination').on('click', function(e) {
      prevFocus.mathquill('write', '_{n}C_{k}').focus();
  });
}

function bindCalculusKeys() {
  getFocussedTextArea();
  /*Calculus*/
  $('#degree').on('click', function(e) {
      prevFocus.mathquill('write', '\\deg').focus();
  });

  /*LTE*/
  $('#theta').on('click', function(e) {
      prevFocus.mathquill('write', '\\theta').focus();
  });

  /*LT*/
  $('#limit').on('click', function(e) {
      prevFocus.mathquill('write', '\\lim_{x\\to0} f(x)').focus();
  });

  /*GT*/
  $('#series').on('click', function(e) {
      prevFocus.mathquill('write', '\\sum_{}^{} x').focus();
  });

  /*Equal To*/
  $('#def_integral').on('click', function(e) {
      prevFocus.mathquill('write', '\\int_{}^{}').focus();
  });

  /*Divide*/
  $('#indef_integral').on('click', function(e) {
      prevFocus.mathquill('write', '\\int').focus();
  });
}

function switchMathKeyboard(keyboard) {
  $('#canvasArea').hide();

  switch(keyboard) {
    case "algebra":
      bindAlgebraKeys();break;
    case "calculus":
      bindCalculusKeys();break;
    case "prealgebra":
      bindPrealgebraKeys();break;
    case "statistics":
      bindStatisticsKeys();break;
    case "geometry":
      bindGeometryKeys();break;
    default:
      break;
  }
}
