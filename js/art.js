var last_product_id = 0;

$(function() {

	$('#productModal').on('hide.bs.modal', function (e) {
		if (typeof countdown != 'undefined') clearInterval(countdown);
	});


});

function refresh_products(page) {

    var search = $('#filters input[name=filter-search]').val();
    var search_previous = $('#filters input[name=filter-search-previous]').val();

    if ($('#filters input[name=filter-best-discounts]').length) {
    	var best_discounts = $('#filters input[name=filter-best-discounts]').is(':checked');
    }
    else var best_discounts = "false";

	var domain = $('#filters select[name=filter-domain]').val();
    var product_group = $('#filters input[name=filter-product-group]').val();
	var sort = $('#filters select[name=filter-sort]').val();
    if (page == -1) page = $('#product-grid-page').val();

    if (search != search_previous) page = 0;

	$('#box-container .box').remove();
	$('#box-container').html("<div style='text-align: center; font-size: 18px; font-weight: bold; text-shadow: 0 0 10px #fff; padding: 50px 0;'><i class='fa fa-spin fa-spinner'></i> Please Wait...</div>");

    $.ajax({
        url: "product_grid.php",
        method: 'POST',
        cache: false,
        data: { domain: domain, product_group: product_group, page: page, sort: sort, search: search, best_discounts: best_discounts },
        success: function(data) {
        	$('#box-container').html(data);
			refresh_layout();

			$('#filters input[name=filter-search]').focus().select().on("keyup", function(e) {
				if (e.which == 13) refresh_products(-1);
			});

            $('#box-container .has-tooltip').tooltip({html: true});

		}
    });
}

$(window).resize(function() {
	refresh_layout();
});

function refresh_layout() {
	$('#wait').hide();

	var box_height = Array();
	var final_y = Array();
	var final_x = Array();

	$('.box').each(function(e) {
		box_height.push($(this).height());
	});

	var width = $('#box-container').width();
	var cols = Math.floor(width / 250);
	var start = Math.round((width - cols * 250) / 2);
	var container_height = 0;

	jQuery.each(box_height, function(index) {
		var x = start + (index % cols) * 250;

		var row = Math.floor(index / cols);
		var y = 0;

		for (i = 0; i < row; i++) {
			y += box_height[i * cols + (index % cols)] + 14;
		}

		final_y.push(y);
		final_x.push(x);

		container_height = y + 150;
	});

	$('.box').each(function(index) {
		$(this).css({ top: final_y[index], left: final_x[index] });
	});

    $('#box-container').height(container_height + 740);
	$('#box-container-inner').height(container_height + 450);
}

function show_product(product_id) {
	$('#productModal .modal-body').html('<div style="padding: 20px 20px 0 20px;"><i class="fa fa-spin fa-spinner"></i> Please Wait...</div>');
	$('#productModal').modal('show');

    $.ajax({
        url: "product.php",
        method: 'POST',
        cache: false,
        data: { product_id: product_id },
        success: function(data) {
        	$('#productModal .modal-body').html(data);
            $('#productModal .has-tooltip').tooltip({html: true});
		}
    });
}

function show_last_product() {
	$('.modal').modal('hide');

	if (last_product_id > 0) {
		show_product(last_product_id);
	}
}

function account(id) {
	$('.modal').modal('hide');
	//$('#accountModal .modal-body').html('<i class="fa fa-spin fa-spinner"></i> Please Wait...');
	$('#accountModal').modal('show');

   
    $.ajax({
        url: "account.php",
        method: 'POST',
        cache: false,
        data: { id: id },
        success: function(data) {
            console.log(data);
        	$('#accountModal .modal-body').html(data);
		}
    });
}


function sign_in() {
	$('#sign-in-form .btn-primary').attr('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner"></i> Please Wait');
    $.ajax({
        url: "sign_in.php",
        method: 'POST',
        cache: false,
        data: $('#sign-in-form form').serialize(),
        success: function(data) {
          
        	if (data == "error") {
                modal_message('Details Incorrect', 'The email or password you entered was incorrect. Please try again.', 'Try Again', 'account("sign-in")');
        	}
        	else if (data == "signed in") {
	        	refresh_menu();
				$('.modal').modal('hide');
				//$('#postSignInModal').modal('show');
               window.location.href="index.php";
        	}
            else if (data == "reset password") {
                window.location.replace("reset_password.php");
            }
            else {
                modal_message('Email Not Verified', 'The email address needs verification. <span onclick="resend_confirmation(' + data + ')" style="text-decoration: underline; cursor: pointer;">Click here</span> to resend verification email.', 'Sign In Again', 'account("sign-in")');
            }
		}
    });
}

function sign_out() {
	$('#accountModal .btn').attr('disabled', 'disabled');
	$('#accountModal .btn-sign-out').html('<i class="fa fa-spin fa-spinner"></i> Please Wait');

  	$.ajax({
        url: "sign_out.php",
        cache: false,
        success: function(data) {
        	$('.modal').modal('hide');
        	refresh_menu();
		}
    });
}

function create_account() {
	$('#create-account-form .btn-primary').attr('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner"></i> Please Wait');

    $.ajax({
        url: "create_account.php",
        method: 'POST',
        cache: false,
        data: $('#create-account-form form').serialize(),
        success: function(data) {

			$('#create-account-form .btn-primary').removeAttr('disabled', 'disabled').html('Create Account');

        	if (data == "error") {
				$('#create-account-form .error').html('The email is invalid or password is less than 8 characters.').show();
        	}
        	else if (data == "email exists") {
				$('#sign-in-form .error').html('The email you used is already in our system. Please use it to sign in.').show();

				$('.sign-groups').hide();
				$('#sign-in-form').show();
        	}
            else if (data == "confirmation sent") {
                $('#sign-in-form .error').html('A confirmation email has been sent to you. Click the link in that email to verify this account before signing in.').show();

                $('.sign-groups').hide();
                $('#sign-in-form').show();
            }
        	else if (data == "signed in") {
	        	refresh_menu();
				$('.modal').modal('hide');
				$('#postSignUpModal').modal('show');
        	}
		}
    });
}

function forgot_password() {
    var email = $('#forgot-password-form input[name=email]').val();

	$('#forgot-password-form .btn-primary').attr('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner"></i> Please Wait');

    $.ajax({
        url: "send_email.php",
        method: 'POST',
        cache: false,
        data: $('#forgot-password-form form').serialize(),
        success: function(data) {
            
			$('#forgot-password-form .btn-primary').removeAttr('disabled', 'disabled').html('Reset Password');

            if (data == "password reset") {
                modal_message('Email Sent!', 'Instructions on resetting your password have been emailed to you.<br><br>Also, check your <strong>spam folder</strong> for this email. Sometimes it accidentally ends up there.', 'OK, Thanks!', '');
            }
            else modal_message('Invalid Email', 'The email you entered is invalid. Are you sure you typed it correctly?<br><br><strong>' + email + '</strong>', 'Try Again', 'account("forgot-password")');
		}
    });
}

function refresh_menu() {
    $.ajax({
        url: "menu.php",
        method: 'GET',
        cache: false,
        data: { ajax: 1 },
        success: function(data) {
        	$('.nav.nav-main .menu-top').html(data);
		}
    });
}

function account_update() {

    var email = $('#accountModal #sign_email').val();

    if (check_email(email)) {
        alert("Invalid Email.");
        return false;
    }

    var password = $('#accountModal #sign_password').val();
    if ((password.length > 0) && (password.length < 8)) {
        alert("Password must be longer than 8 characters.");
        return false;
    }

	$('#account-form .btn-primary').attr('disabled', 'disabled').html('<i class="fa fa-spin fa-spinner"></i> Please Wait');


	$.ajax({
		url: "account_update_check.php",
		method: 'POST',
		cache: false,
		data: $('#account-form form').serialize(),
		success: function(data) {
			$('#account-form .btn-primary').removeAttr('disabled', 'disabled').html('Update Details');

			if (data != "") {
				alert(data);
				return;
			}

			$.ajax({
				url: "account_update.php",
				method: 'POST',
				cache: false,
				data: $('#account-form form').serialize(),
				success: function(data) {
					$('#account-form .btn-primary').removeAttr('disabled', 'disabled').html('Update Details');

					if (data != "") {
						modal_message("Oh no!", data, '', '');
					}
					else {
						show_last_product();
					}
				}
			});
		}
	});
}

function check_profile_url() {

	var profile_url = $('#sign_profile').val().trim();
    $('#sign_profile').val(profile_url);

    if (profile_url.indexOf("http") != 0) profile_url = "http://" + profile_url;

	window.open(profile_url);

}


function check_email(email) {

    email = email.trim();
    var error = 0;

    if (email.match(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/ig) === null) error = 1;
    if (email.match(/\.\./) !== null) error = 1;
    if (email.charAt(0) == ".") error = 1;

    if (email.split("@").length - 1 != 1) error = 1;
    else {
        var parts = email.split("@");
        if (parts[0].charAt(parts[0].length - 1) == ".") error = 1;
        if (parts[1].charAt(parts[1].length - 1) == "-") error = 1;
        if (parts[1].charAt(0) == "-") error = 1;
    }

    return error;
}

/*function review_now(product_id) {

	if ($('.review_now_' + product_id).hasClass('disabled')) return;

	$('.review_now_' + product_id).addClass('disabled').html('<i class="fa fa-spin fa-spinner"></i> Please Wait');

    $.ajax({
        url: "request.php",
        method: 'POST',
        cache: false,
        data: { product_id: product_id },
        success: function(data) {
            console.log(data);
        	if (data == "error") {
	        	$('.review_now_' + product_id).addClass('danger').html('There was a error!');
	        	$('.product-message').html('Something went wrong... refresh your browser and try again.').show();
	        	return;
	        }

	        if (data == "success") {
	        	$('.review_now_' + product_id).addClass('success').removeClass('disabled').html('Review Requested');
	        	$('.product-message').html('Your request has been sent to the seller. If you are chosen to review this product you will receive an email with further instructions. <br><br><a href="review-requests.php">Click here</a> to manage your review requests.').show();

	        	$('#product_' + product_id + ' .btn-review').addClass('success').html("<span class='glyphicon glyphicon-ok'></span>");
	        	return;
	        }

	        if (data == "removed") {
	        	$('.review_now_' + product_id).removeClass('success disabled').html('Request Removed');
	        	$('.product-message').html('Your request to review this product has been removed. If this was a mistake just click the button above again.').show();

	        	$('#product_' + product_id + ' .btn-review').removeClass('success').html("Review Now");
	        	return;
	        }

            if (data == "over limit") {
                $('.review_now_' + product_id).addClass('danger').removeClass('disabled').html('There was a problem!');
                $('.product-message').html('You are over your request limit! You need to review a product to request more vouchers. <br><br><a href="review-requests.php">Click here</a> to manage your review requests.').show();

                $('#product_' + product_id + ' .btn-review').removeClass('success').html("Review Now");
            }
		}
    });
}*/

function mc_subscribe(cl) {
	var email = $('.' + cl + ' input[name=email]').val();

	$('.' + cl + ' button').html('<i class="fa fa-spin fa-spinner"></i> Please Wait').attr('disabled', 'disabled');


    $.ajax({
        url: "mc_subscribe.php",
        method: 'POST',
        cache: false,
        data: { email: email },
        success: function(data) {

        	$('.' + cl).html(data);

        }
    });
}

function modal_message(title, message, button, onclick) {
    $('.modal').modal('hide');
    $('#messageModal .modalName').html(title);
    $('#messageModal .modal-body').html(message);

    if (button == '') $('#messageModal .modal-footer').hide();
    else {
        $('#messageModal .modal-footer').show();
        $('#messageModal .modal-footer .btn').html(button).show();
        $('#messageModal .modal-footer .btn').attr('onclick', onclick);
    }

    $('#messageModal').modal('show');
}

function resend_confirmation(reviewer_id) {
    $('.modal').modal('hide');
    $('#accountModal .modal-body').html('<i class="fa fa-spin fa-spinner"></i> Please Wait...');
    $('#accountModal').modal('show');

    $.ajax({
        url: "resend_confirmation.php",
        method: 'POST',
        cache: false,
        data: { reviewer_id: reviewer_id },
        success: function(data) {
            $.ajax({
                url: "account.php",
                method: 'POST',
                cache: false,
                data: { id: 'sign-in' },
                success: function(data) {
                    $('#accountModal .modal-body').html(data);
                    $('#accountModal .modal-body .error').html('A verification email has been sent to you. Click the link in that email to verify your account before signing in.').show();
                }
            });
        }
    });
}

function update_product_groups() {

    var product_groups = [];

    $('#productGroupModal .product-group-option.selected').each(function() {
        product_groups.push($(this).html());
    });

    if (product_groups.length > 0) {
        $('div[name=filter-product-group]').html(product_groups.join(', '));
    }
    else $('div[name=filter-product-group]').html('All Products');

    $('#filters input[name=filter-product-group]').val(product_groups.join('*'));
}