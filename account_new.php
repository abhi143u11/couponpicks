<?php
 
?>
 <html>
 <head>
    <meta charset="UTF-8">
    <title> Amazon Coupons & Deals | Couponpicks</title>
   
   
         <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
             <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
         <link rel="stylesheet" type="text/css" href="css/style.css">
           <link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
           <link rel="stylesheet" type="text/css" href="css/default.css">
            <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</head>
<body>
<div class="modal fade in" id="accountModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<div class="row sign-groups" id="account-form">
    <div class="col-xs-12">
        <h3>Account</h3>

        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="93cdabfb9367c2cb43ee95540d9b4f4a">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off" value="aayushi@digitalsense.in">
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="Leave blank to keep old password" autocomplete="off">
                </div>
            </div>

            <div class="form-group" style="margin-bottom: 25px;">
                <label for="sign_profile" class="col-sm-3 control-label">Profile URL</label>
                <div class="col-sm-9">
                    <input type="test" class="form-control" id="sign_profile" name="profile_url" value="">
                    <button type="button" class="btn btn-xs" style="margin-top: 7px; float: right;" onclick="check_profile_url()">Check Profile URL</button>
                    <br>
                    <p>To find your profile url:</p>
                    <ul>
                        <li>Go here: <a href="https://www.amazon.com/profile" target="_blank">https://www.amazon.com/profile</a></li>
                        <li>Log into your Amazon account if you need to</li>
                        <li>The URL in the address bar is your profile URL</li>
                    </ul>

                    <p>Alternative method here: <a href="profile-url.php" target="_blank">How do I find my profile URL?</a></p>
                </div>
            </div>

            <button type="button" class="btn btn-default pull-left btn-sign-out" onclick="sign_out()">Sign Out</button>
            <button type="button" class="btn btn-primary pull-right" onclick="account_update()">Update Details</button>

        </form>

    </div>
</div>


</div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
      
            </body>
            </html>
 <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<div class="row sign-groups" id="forgot-password-form">
    <div class="col-xs-12">
        <h3>Forgot Password</h3>

        <p>Enter your email and we will send you password reset instructions.</p>
        <br>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal" onkeypress="return event.keyCode != 13;">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I know my log in details</span>

            <button type="button" class="btn btn-primary pull-right" onclick="forgot_password()">Reset Password</button>

        </form>

    </div>
</div>
</div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->