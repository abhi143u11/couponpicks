<?php 
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "couponpicks";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT oc_category_description.category_id, oc_category_description.name FROM oc_category, oc_category_description WHERE oc_category.category_id = oc_category_description.category_id AND STATUS =1 AND oc_category.parent_id = 0 ORDER BY oc_category_description.name ASC";

$cats = $conn->query($sql);

if(isset($_REQUEST['cat'])){
 $keyword = $_REQUEST['cat']; 

$sql ="SELECT query FROM oc_url_alias WHERE keyword = '".$keyword."'";

$product_query = $conn->query($sql);

    while($row = $product_query->fetch_assoc()) {
        
        $catid = $row['query'];
    }
    
    
  $catid = str_replace("-c[","",$catid);  
  $catid = str_replace("]","",$catid);
}else{

$catid = "";
}
if(isset($_POST['filter_name']))
{
$search_result = $_POST['filter_name'];
//if(isset($search_result)){ 
    //if isset value textbox search

    $sql ="SELECT oc_product.product_id,oc_product_description.name,
            oc_product.image,oc_product_description.description,oc_product.price 
        FROM oc_product,oc_product_description 
        WHERE oc_product.product_id=oc_product_description.product_id 
        AND oc_product_description.name like '%$search_result%' 
        ORDER BY oc_product.date_added DESC";
} else{ 

    if($catid<=0){
        $sql ="SELECT oc_product.product_id,oc_product_description.name,oc_product.image,oc_product.price FROM oc_product,oc_product_description WHERE oc_product.product_id=oc_product_description.product_id AND oc_product.status=1 ORDER BY oc_product.date_added DESC";
        $title="Shop";    
    }else{
        $sql = "SELECT name FROM oc_category_description WHERE category_id=".$catid;
        $cat = $conn->query($sql);
        $row = $cat->fetch_assoc();
        $title = $row['name'];

        $sql = "SELECT product_id FROM oc_product_to_category WHERE category_id = ".$catid;
        $list_product = $conn->query($sql);
        if ($list_product->num_rows > 0) {
            $str ='(';
            while($row = $list_product->fetch_assoc()) {
                $str .= $row['product_id'].',';
            }
            $str = substr($str,0,strlen($str)-1);
            $str .=')';
            $sql ="SELECT oc_product.product_id,oc_product_description.name,oc_product.image,oc_product.price FROM oc_product,oc_product_description WHERE oc_product.product_id=oc_product_description.product_id AND oc_product.product_id in $str AND oc_product.status=1   ORDER BY oc_product.date_added DESC";
        }
    }
}
//get total records

$num_rec_per_page=10;

$rs_result = $conn->query($sql); //run the query
$total_records = $rs_result->num_rows;  //count number of records

$total_pages = ceil($total_records / $num_rec_per_page); 

if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
$start_from = ($page-1) * $num_rec_per_page;

$sql .= " LIMIT $start_from, $num_rec_per_page";
$products = $conn->query($sql);
//echo $sql;
//print_r($products);
//$url_alias = $conn->query("SELECT * FROM oc_url_alias");   
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title;?> | Couponpicks</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
    <script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
    
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
    <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
    <script type="text/javascript" src="js/javascript.js"></script>
    <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.js"></script>
    
    
</head>
<body>
    <div class="navbar navbar-fixed-top topbar">
       <div class="navbar-inner kasdf">
          <div class="container container2 wrap-menu">
             <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
             <div class="pull-left firstsearch" id="search">
                   <div class="input-prepend">
                       <form method="POST">
                           <input type="text" onkeydown="this.style.color = '#000000';" placeholder="Search" name="filter_name" class="form-search">
                       
                           <button type="submit" id="buttn-search"  class="btn btn-success add-on handpoint">
                            <i class="icon-search icon-large icon-top"></i>
                        </button>
                       </form>
                   </div>
            </div>
             <div id="headerunder" class="pull-right colorback"></div>
             <div id="header" class="pull-right topcart colorback">
                <ul id="userNav">
                   <li>
                      <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
                      </a>
                      <div class="userSubMenu menu">
                         <div class="menuWrapper">
                            <ul>
                               <li><a href="#">My Profile</a></li>
                               <li><a href="#">My Deals</a></li>
                               <li><a href="#">My Coupons</a></li>
                               <li><a href="#">Notifications</a></li>
                               <li><a href="#">Messages</a></li>
                               <li><a href="#">Saved Stores</a></li>
                               <li><a href="#">Find Friends</a></li>
                               <li><a class="subUserMenuLink" href="#">Settings</a></li>
                               <li><a href="#">Sign Out</a></li>
                            </ul>
                         </div>
                      </div>
                   </li>
                </ul>
                <a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
                    <img class="icon-plus-button" alt="add" src="images/blank.png">
                </a>
             </div>
             <div class="topmenu">
                <div class="dropdown">
                    <div class="click-toggle">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
                       <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
                           <?php
                            if ($cats->num_rows > 0) {
                                while($row = $cats->fetch_assoc()) { ?>
                              <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="<?php
                          
                                
                            $catid = $row['category_id'];
                      $sql =  "SELECT * FROM oc_url_alias WHERE query = '-c[$catid]'";
              
                    $result2 = $conn->query($sql);
                     while($seourl = $result2->fetch_assoc()) {
                          echo $seourl['keyword'];      
                         
                     }
                 
                              
                         ?>
                              
                              "><?php echo $row['name'];?></a>
                              </li>
                        <?php
                            }
                        }
                        ?>    
                       </ul>
                    </div>
                   <a class="padright" id="wishlist-total" href="#">My Feed</a>
                   <a href="#">Popular</a>
                   <div class="btn-group little-select">
                          <a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
                             <i class="icon-reorder"></i>
                          </a>         
                          <!-- <ul class="dropdown-menu">
                             <form enctype="multipart/form-data" method="post" action="#">
                                <div id="currency">Currency<br>
                                   <a title="Euro">€</a>
                                   <a title="Pound Sterling">£</a>
                                   <a title="US Dollar"><b>$</b></a>
                                </div>
                             </form>
                          </ul> -->
                          <div id="option-list">
                              <div class="menuWrapper">
                                  <ul class="menuTopIcons ">
                                   <li><a href="#">
                                         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
                                         <div class="menuTopIconTitle">My Feed</div>
                                    </a></li>
                                   <li><a href="#">
                                         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
                                         <div class="menuTopIconTitle">Popular</div>
                                    </a></li>
                                   <li><a href="#">
                                         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
                                         <div class="menuTopIconTitle">Fresh</div>
                                    </a></li>
                                   <li><a href="#">
                                         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
                                         <div class="menuTopIconTitle">Heating Up</div>
                                    </a></li>
                                   <li><a href="#">
                                         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
                                         <div class="menuTopIconTitle">Coupon Codes</div>
                                    </a></li>
                                   <li><a href="#">
                                        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
                                        <div class="menuTopIconTitle">Printable Coupons</div>
                                    </a></li>
                                   <li><a href="#">
                                        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
                                        <div class="menuTopIconTitle">Interests</div>
                                      </a></li>
                                </ul><!-- #menuTopIcons -->
                                <ul>
                                   <li><a href="#">Apps</a></li>
                                   <li><a href="#">Automotive</a></li>
                                   <li><a href="#">Bed &amp; Bath</a></li>
                                   <li><a href="#">Computers &amp; Software</a></li>
                                   <li><a href="#">Electronics</a></li>
                                   <li><a href="#">Entertainment</a></li>
                                   <li><a href="#">Freebies</a></li>
                                   <li><a href="#">Furniture &amp; Decor</a></li>
                                   <li><a href="#">Games</a></li>
                                </ul>
                                <ul>
                                   <li><a href="#">Gifts &amp; Flowers</a></li>
                                   <li><a href="#">Grocery &amp; Food</a></li>
                                   <li><a href="#">Health &amp; Beauty</a></li>
                                   <li><a href="#">Home &amp; Garden</a></li>
                                   <li><a href="#">Kids &amp; Baby</a></li>
                                   <li><a href="#">Kitchen &amp; Dining</a></li>
                                   <li><a href="#">Laptop</a></li>
                                   <li><a href="#">Men</a></li>
                                   <li><a href="#">News</a></li>
                                </ul>
                                <ul>
                                   <li><a href="#">Office &amp; School</a></li>
                                   <li><a href="#">Other</a></li>
                                   <li><a href="#">Pets</a></li>
                                   <li><a href="#">Sports &amp; Outdoor</a></li>
                                   <li><a href="#">Tax &amp; Finance</a></li>
                                   <li><a href="#">Toys</a></li>
                                   <li><a href="#">Travel &amp; Tickets</a></li>
                                   <li><a href="#">TV</a></li>
                                   <li><a href="#">Women</a></li>
                                   <li><a href="#">DealsPlus Exclusive</a></li>
                                </ul>
                              </div><!-- #menuWrapper -->
                              <div class="aboutUsLinks">
                               <div class="aboutUsSection">
                                  <a href="about.php">About Us</a>
                                  <span>|</span>
                                  <a target="_blank" href="blog.php">Blog</a>
                                  <span>|</span>
                                  <a href="#">Contact</a>
                                  <span>|</span>
                                  <a href="privacy.php">Privacy Policy</a>
                                  <span>|</span>
                                  <a href="tos.php">Terms of Use</a>
                                  <div class="categoryMenuSocial">
                                     <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
                                     <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
                                     <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
                                     <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
                                  </div>
                               </div>
                            </div><!-- #aboutUsLinks -->
                          </div><!-- #option-list -->
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div><!-- #navbar-fixed-top -->

    <div class="navbar navbar-static-top menubar responsive-menu">
        <div class="navbar-inner">  
            <div class="dropdown drsp">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
                </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                        print_r($cats);
                        if ($cats->num_rows > 0) {
                            while($row = $cats->fetch_assoc()) { ?>
                              <li>
                                  <a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
                              </li>
                    <?php
                            }
                        }
                    ?> 
                </ul>
                <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
                <a href="#">My Account</a>
                <div class="btn-group little-select">
                    <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
                    <ul class="dropdown-menu">
                        <form enctype="multipart/form-data" method="post" action="index.php">
                              <div id="currency">Currency<br>
                                <a title="Euro">€</a>
                                <a title="Pound Sterling">£</a>
                                <a title="US Dollar"><b>$</b></a>
                              </div>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- #responsive-menu -->

    <div class="navbar navbar-static-top menubar responsive-search">
        <div class="navbar-inner">   
            <div class="container container2">
                <div align="center" id="header">
                    <div class="pull-left fullwidth nopad5" id="search">
                        <div class="input-prepend">
                            <span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
                            <input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- #responsive-search -->

    <div class="dialogHolder" id="addToDPDialog">
       <div class="dialogContent">
          <div id="addToDPDialogContent">
             <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
             <div id="addToDPButtons">
                <div class="dialogBody">
                   <div class="addDialog">
                      <h4>What are you adding?</h4>
                      <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
                   </div>
                   <div class="addItems">
                      <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
                         <h6 class="itemName">Deal / Product</h6>
                         <div class="itemImage">
                            <img src="images/dealIcon.png">
                         </div>
                         <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
                      </a>
                      <a href="#" class="itemContainer" id="addCouponLink">
                         <h6 class="itemName">Coupon</h6>
                         <div class="itemImage">
                            <img src="images/couponIcon.png">
                         </div>
                         <div class="itemDescription">A coupon to be used online or in store</div>
                      </a>
                      <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
                         <h6 class="itemName">Topic / Photo</h6>
                         <div class="itemImage">
                            <img src="images/topicIcon.png">
                         </div>
                         <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
                      </a>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>

    <div class="afterheader"></div>
    <div id="container">
        <div class="container container2">
            <div style="margin:0 auto;">
                <?php
                    if ($products->num_rows > 0) {
                        
                        
                ?>
                <div class="grid" id="pinboard">
                    <?php                                
                        while($row = $products->fetch_assoc()) {
                            
                           
                        ?>
                        <div class="grid-item">
                            <div id="lnk" class="name">
                                <a href="<?php
                                
                            $product_id = $row['product_id'];
                      $sql =  "SELECT * FROM oc_url_alias WHERE query = '-p[$product_id]'";
              
                    $result2 = $conn->query($sql);
                     while($seourl = $result2->fetch_assoc()) {
                          echo $seourl['keyword'];      
                         
                     }
                 
                              
                         ?>">
                                    <?php echo utf8_encode($row['name']);?>
                                </a>
                            </div>
                            <div class="image">
                                <a href="<?php
                                
                            $product_id = "product_id=".$row['product_id'];
                      $sql =  "SELECT * FROM oc_url_alias WHERE query = '$product_id'";
                  
                    $result2 = $conn->query($sql);
                     while($seourl = $result2->fetch_assoc()) {
                          echo $seourl['keyword'];      
                         
                     }
                 
                              
                         ?>">
                                    <img alt="White flowers cake" src="http://localhost/couponpicks/image/<?php echo utf8_encode($row['image']);?>" />
                                </a>
                            </div>
                            <div class="pull-left">
                                <?php if($row['price'] > 0) { ?>
                                    <div class="price">$<?php
                         
                                   $price =  $row['price'];
                                $price =    number_format((float)$price, 2, '.', ''); 
                                     echo   $price; ?></div>
                                    <?php } else { echo " ";}
                                ?>
                            </div>
                            <div class="social pull-right">
                                <a href=""><i class="icon-gift icon-large"></i></a>
                                <a href="#"><i class="icon-thumbs-up icon-large"></i></a>
                            </div>
                            <div style="clear:both"></div>
                            <div class="cart handpoint"><i class="icon-shopping-cart laks"></i>Add to Cart</div>
                        </div><!--  #grid-item -->
                    <?php }?>
                </div>
                <?php 
                    }else{
                        echo '<h1 class="not_product">No product found!</h1>';
                    }
                ?>
                
            </div>
        </div><!-- #container2 -->
        <div id="footericon" class="bigfooticon colorbackwhite handpoint">
            <i class="icon-eject"></i>
        </div><!-- #footericon -->
        <div id="footer" class="colorback" style="display: block;">
            <div class="container">
                <div class="inner footmenu">
                    <div class="span3">
                        <div class="btn-group hsds">
                           <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
                           </a>
                           <ul class="dropdown-menu bottom-up">
                              <li><a href="about.php">About Us</a></li>
                              <li><a href="#">Delivery Information</a></li>
                              <li><a href="privacy.php">Privacy Policy</a></li>
                              <li><a href="tos.php">Terms &amp; Conditions</a></li>
                           </ul>
                        </div>
                    </div>
                    <div class="span3">
                       <div class="btn-group hsds">
                          <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
                          </a>
                          <ul class="dropdown-menu bottom-up">
                             <li><a href="#">Contact Us</a></li>
                             <li><a href="#">Returns</a></li>
                             <li><a href="#">Site Map</a></li>
                          </ul>
                       </div>
                    </div>
                    <div class="span3">
                       <div class="btn-group hsds">
                          <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
                          </a>
                          <ul class="dropdown-menu bottom-up">
                             <li><a href="#">Brands</a></li>
                             <li><a href="#">Gift Vouchers</a></li>
                             <li><a href="#">Affiliates</a></li>
                             <li><a href="#">Specials</a></li>
                          </ul>
                       </div>
                    </div>
                    <div class="span3">
                       <div class="btn-group hsds">
                          <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
                          </a>
                          <ul class="dropdown-menu bottom-up">
                             <li><a href="#">My Account</a></li>
                             <li><a href="#">Order History</a></li>
                             <li><a href="#">Wish List</a></li>
                             <li><a href="#">Newsletter</a></li>
                          </ul>
                       </div>
                    </div>
                </div><!-- #footmenu -->
            </div>
        </div><!-- #footer -->
        <div id="powered" class="powered_pos"><p>Powered By <a href="index.html">Couponpicks</a> Your Store &copy; 2015</p></div>
    </div><!-- #container -->
    <a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
        <span id="toTopHover"></span>
        <i class="icon-chevron-up icon-large"></i>
    </a>
<nav id="page-nav">
  <a href="index.php?page=<?php echo $page+1;?>"></a>
</nav>
<script src="js/jquery.js"></script>
<script>
  $(function(){
    
    var $container = $('#pinboard');
    
    $container.imagesLoaded(function(){
      $container.masonry({
        itemSelector: '.grid-item'
      });
    });
    
    $container.infinitescroll({
      navSelector  : '#page-nav',    // selector for the paged navigation 
      nextSelector : '#page-nav a',  // selector for the NEXT link (to page 2)
      itemSelector : '.grid-item',     // selector for all items you'll retrieve
      loading: {
          finishedMsg: 'No more pages to load.',
          img: 'http://i.imgur.com/6RMhx.gif'
        }
      },
      // trigger Masonry as a callback
      function( newElements ) {
        // hide new items while they are loading
        var $newElems = $( newElements ).css({ opacity: 0 });
        // ensure that images load before adding to masonry layout
        $newElems.imagesLoaded(function(){
          // show elems now they're ready
          $newElems.animate({ opacity: 1 });
          $container.masonry( 'appended', $newElems, true ); 
        });
      }
    );
    
  });
</script>
</body>
</html>      
