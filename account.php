


<div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<?php if(isset($_REQUEST['id']))
{
     $id=$_REQUEST['id'];
 if($id=="forgot-password") 
 {  
 ?> 
<div class="row sign-groups" id="sign-in-form" style="display:none;">
    <div class="col-xs-12">
        <span class="clickable pull-right" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#create-account-form').show();">or Create Account</span>
        <h3>Sign In</h3>

        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">
            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="Password" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#forgot-password-form').show();">Forgot password?</span>

            <button type="button" class="btn btn-primary pull-right" onclick="sign_in()">Sign In</button>

        </form>

    </div>
</div>
<div class="row sign-groups" id="forgot-password-form" >
    <div class="col-xs-12">
        <h3>Forgot Password</h3>

        <p>Enter your email and we will send you password reset instructions.</p>
        <br>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal" onkeypress="return event.keyCode != 13;">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I know my log in details</span>

            <button type="button" class="btn btn-primary pull-right" onclick="forgot_password()">Reset Password</button>

        </form>

    </div>
</div>
<div class="row sign-groups" id="create-account-form" style="display:none;">
    <div class="col-xs-12">
        <h3>Create Account</h3>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                    <p style="margin-top: 10px">* Required to receive promo codes</p>
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="8 characters minimum" autocomplete="off">
                    <input type="checkbox" name="mc_check" id="mc_check" checked=""><label for="mc_check" style="margin: 10px 0 0 10px;">Get the hottest daily offer updates sent to my inbox</label>
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I already have an account</span>

            <button type="button" class="btn btn-primary pull-right" onclick="create_account()">Create Account</button>

        </form>

    </div>
</div>
<?php }
else if($id=="sign-in")
{
?>
<div class="row sign-groups" id="sign-in-form" >
    <div class="col-xs-12">
        <span class="clickable pull-right" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#create-account-form').show();">or Create Account</span>
        <h3>Sign In</h3>

        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">
            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="Password" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#forgot-password-form').show();">Forgot password?</span>

            <button type="button" class="btn btn-primary pull-right" onclick="sign_in()">Sign In</button>

        </form>

    </div>
</div>
<div class="row sign-groups" id="forgot-password-form" style="display:none;">
    <div class="col-xs-12">
        <h3>Forgot Password</h3>

        <p>Enter your email and we will send you password reset instructions.</p>
        <br>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal" onkeypress="return event.keyCode != 13;">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I know my log in details</span>

            <button type="button" class="btn btn-primary pull-right" onclick="forgot_password()">Reset Password</button>

        </form>

    </div>
</div>
<div class="row sign-groups" id="create-account-form" style="display:none;">
    <div class="col-xs-12">
        <h3>Create Account</h3>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                    <p style="margin-top: 10px">* Required to receive promo codes</p>
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="8 characters minimum" autocomplete="off">
                    <input type="checkbox" name="mc_check" id="mc_check" checked=""><label for="mc_check" style="margin: 10px 0 0 10px;">Get the hottest daily offer updates sent to my inbox</label>
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I already have an account</span>

            <button type="button" class="btn btn-primary pull-right" onclick="create_account()">Create Account</button>

        </form>

    </div>
</div>
<?php }
else if($id=="create-account")
{
?>
<div class="row sign-groups" id="sign-in-form" style="display:none;">
    <div class="col-xs-12">
        <span class="clickable pull-right" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#create-account-form').show();">or Create Account</span>
        <h3>Sign In</h3>

        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">
            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="Password" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#forgot-password-form').show();">Forgot password?</span>

            <button type="button" class="btn btn-primary pull-right" onclick="sign_in()">Sign In</button>

        </form>

    </div>
</div>
<div class="row sign-groups" id="forgot-password-form" style="display: none;" >
    <div class="col-xs-12">
        <h3>Forgot Password</h3>

        <p>Enter your email and we will send you password reset instructions.</p>
        <br>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal" onkeypress="return event.keyCode != 13;">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I know my log in details</span>

            <button type="button" class="btn btn-primary pull-right" onclick="forgot_password()">Reset Password</button>

        </form>

    </div>
</div>
<div class="row sign-groups" id="create-account-form" >
    <div class="col-xs-12">
        <h3>Create Account</h3>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                    <p style="margin-top: 10px">* Required to receive promo codes</p>
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="8 characters minimum" autocomplete="off">
                    <input type="checkbox" name="mc_check" id="mc_check" checked=""><label for="mc_check" style="margin: 10px 0 0 10px;">Get the hottest daily offer updates sent to my inbox</label>
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I already have an account</span>

            <button type="button" class="btn btn-primary pull-right" onclick="create_account()">Create Account</button>

        </form>

    </div>
</div>
<?php }?>
<?php }?>


