<?php
require_once('Mail.php');
require_once('Mail/mime.php');

$from = 'Sender <admin@couponpicks.com>';
$to = 'Receiver <brian@cellularfactory.com>';
$subject = 'Sent from PHP on my machine';

$text = 'This is a message I sent from <a href="http://www.php.net/">PHP</a> '
      . 'using the PEAR Mail package and SMTP through Gmail. Enjoy!';

$message = new Mail_mime();
$message->setTXTBody(strip_tags($text)); // for plain-text
$message->setHTMLBody($text);
$body = $message->get();

$host = 'mail.emailsrvr.com';
$port = 25 ; //According to Google you need to use 465 or 587
$username = 'admin@couponpicks.com';
$password = 'm0r3m0n3y';

$headers = array('From' => $from,
    'To' => $to,
    'Subject' => $subject);
$headers = $message->headers($headers);

$smtp = Mail::factory('smtp',
    array(
        'host' => $host,
        'port' => $port,
        'auth' => true,
        'username' => $username,
        'password' => $password));

$mail = $smtp->send($to, $headers, $body);

if (PEAR::isError($mail)) {
    echo $mail->getMessage();
} else {
    echo "Message sent successfully!";
}

echo "\n";

?>
