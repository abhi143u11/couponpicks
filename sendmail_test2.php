<?php
 require_once "Mail.php";
 require_once "Mail/mime.php";
 
 // see http://pear.php.net/manual/en/package.mail.mail-mime.php
 // for further extended documentation on Mail_Mime

 $from = "Web Master <webmaster@example.com>";
 $to = "Brian <brian@cellularfactory.com>";
 $subject = "Test HTML email using PHP Pear w/ SMTP\r\n\r\n";
 $text = "This is a text test email message";
 $html = "<html><body><p>This is an html test email message
 <a href=\"http://www.rackspace.com\">This Is A Link</a></p></body></html>";
 $crlf = "\n";

 // create a new Mail_Mime for use
 $mime = new Mail_mime($crlf); 
 // define body for Text only receipt
 $mime->setTXTBody($text); 
 // define body for HTML capable recipients
 $mime->setHTMLBody($html);
 
 // specify a file to attach below, relative to the script's location
 // if not using an attachment, comment these lines out
 // set appropriate MIME type for attachment you are using below, if applicable
 // for reference see http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types

 $file = "attachment.jpg";
 $mimetype = "text/html";
 //$mime->addAttachment($file, $mimetype); 

 // specify the SMTP server credentials to be used for delivery
 // if using a third party mail service, be sure to use their hostname
 $host = "mail.emailsrvr.com";
 $username = "admin@couponpicks.com";
 $password = "m0r3m0n3y";
 
 $headers = array ('From' => $from,
  'To' => $to,
  'Subject' => $subject);
 $smtp = Mail::factory('smtp',
  array ('host' => $host,
    'auth' => true,
    'username' => $username,
    'password' => $password));

 
 $body = $mime->get();
 $headers = $mime->headers($headers); 
 
 $mail = $smtp->send($to, $headers, $body);
 
 if (PEAR::isError($mail)) {
  echo("

" . $mail->getMessage() . "
");
} else {
  echo("

Message successfully sent!
");
}
?>
