<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>About | Couponpicks</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {
			$(".various").fancybox({
				maxWidth	: 630,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		});
	</script>
</head>
<body>
	<div class="navbar navbar-fixed-top topbar">
	   <div class="navbar-inner kasdf">
	      <div class="container container2 wrap-menu">
	         <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
	         <div class="pull-left firstsearch" id="search">
               	<div class="input-prepend">
	               	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
	               	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
               	</div>
            </div>
	         <div id="headerunder" class="pull-right colorback"></div>
	         <div id="header" class="pull-right topcart colorback">
	            <ul id="userNav">
				   <li>
				      <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
				      </a>
				      <div class="userSubMenu menu">
				         <div class="menuWrapper">
				            <ul>
				               <li><a href="#">My Profile</a></li>
				               <li><a href="#">My Deals</a></li>
				               <li><a href="#">My Coupons</a></li>
				               <li><a href="#">Notifications</a></li>
				               <li><a href="#">Messages</a></li>
				               <li><a href="#">Saved Stores</a></li>
				               <li><a href="#">Find Friends</a></li>
				               <li><a class="subUserMenuLink" href="#">Settings</a></li>
				               <li><a href="#">Sign Out</a></li>
				            </ul>
				         </div>
				      </div>
				   </li>
				</ul>
				<a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
					<img class="icon-plus-button" alt="add" src="images/blank.png">
				</a>
	         </div>
	         <div class="topmenu">
	            <div class="dropdown">
	            	<div class="click-toggle">
	            		<a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
		               <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
		               	<?php
							if ($cats->num_rows > 0) {
								while($row = $cats->fetch_assoc()) { ?>
			                  <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
			                  </li>
		                <?php
							}
						}
						?>	
		               </ul>
	            	</div>
	               <a class="padright" id="wishlist-total" href="#">My Feed</a>
	               <a href="#">Popular</a>
	               <div class="btn-group little-select">
	                  	<a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
	             			<i class="icon-reorder"></i>
	                  	</a>     	
	                  	<!-- <ul class="dropdown-menu">
	                     	<form enctype="multipart/form-data" method="post" action="#">
	                        	<div id="currency">Currency<br>
		                           <a title="Euro">€</a>
		                           <a title="Pound Sterling">£</a>
		                           <a title="US Dollar"><b>$</b></a>
	                        	</div>
	                     	</form>
	                  	</ul> -->
	                  	<div id="option-list">
	                  		<div class="menuWrapper">
	                  			<ul class="menuTopIcons ">
								   <li><a href="#">
								         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">My Feed</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Popular</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Fresh</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Heating Up</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Coupon Codes</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Printable Coupons</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Interests</div>
								      </a></li>
								</ul><!-- #menuTopIcons -->
								<ul>
								   <li><a href="#">Apps</a></li>
								   <li><a href="#">Automotive</a></li>
								   <li><a href="#">Bed &amp; Bath</a></li>
								   <li><a href="#">Computers &amp; Software</a></li>
								   <li><a href="#">Electronics</a></li>
								   <li><a href="#">Entertainment</a></li>
								   <li><a href="#">Freebies</a></li>
								   <li><a href="#">Furniture &amp; Decor</a></li>
								   <li><a href="#">Games</a></li>
								</ul>
								<ul>
								   <li><a href="#">Gifts &amp; Flowers</a></li>
								   <li><a href="#">Grocery &amp; Food</a></li>
								   <li><a href="#">Health &amp; Beauty</a></li>
								   <li><a href="#">Home &amp; Garden</a></li>
								   <li><a href="#">Kids &amp; Baby</a></li>
								   <li><a href="#">Kitchen &amp; Dining</a></li>
								   <li><a href="#">Laptop</a></li>
								   <li><a href="#">Men</a></li>
								   <li><a href="#">News</a></li>
								</ul>
								<ul>
								   <li><a href="#">Office &amp; School</a></li>
								   <li><a href="#">Other</a></li>
								   <li><a href="#">Pets</a></li>
								   <li><a href="#">Sports &amp; Outdoor</a></li>
								   <li><a href="#">Tax &amp; Finance</a></li>
								   <li><a href="#">Toys</a></li>
								   <li><a href="#">Travel &amp; Tickets</a></li>
								   <li><a href="#">TV</a></li>
								   <li><a href="#">Women</a></li>
								   <li><a href="#">DealsPlus Exclusive</a></li>
								</ul>
	                  		</div><!-- #menuWrapper -->
	                  		<div class="aboutUsLinks">
							   <div class="aboutUsSection">
							      <a href="about.php">About Us</a>
							      <span>|</span>
							      <a target="_blank" href="blog.php">Blog</a>
							      <span>|</span>
							      <a href="#">Contact</a>
							      <span>|</span>
							      <a href="privacy.php">Privacy Policy</a>
							      <span>|</span>
							      <a href="tos.php">Terms of Use</a>
							      <div class="categoryMenuSocial">
							         <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
							         <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
							         <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
							         <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
							      </div>
							   </div>
							</div><!-- #aboutUsLinks -->
	                  	</div><!-- #option-list -->
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div><!-- #navbar-fixed-top -->

	<div class="navbar navbar-static-top menubar responsive-menu">
		<div class="navbar-inner">  
		    <div class="dropdown drsp">
		        <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
		        </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                    	print_r($cats);
						if ($cats->num_rows > 0) {
							while($row = $cats->fetch_assoc()) { ?>
		                  	<li>
		                  		<a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
		                  	</li>
		            <?php
							}
						}
					?> 
                </ul>
		        <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
		        <a href="#">My Account</a>
		        <div class="btn-group little-select">
		            <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
		            <ul class="dropdown-menu">
		                <form enctype="multipart/form-data" method="post" action="index.php">
						  	<div id="currency">Currency<br>
				                <a title="Euro">€</a>
				                <a title="Pound Sterling">£</a>
				                <a title="US Dollar"><b>$</b></a>
						  	</div>
						</form>
		            </ul>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-menu -->

	<div class="navbar navbar-static-top menubar responsive-search">
		<div class="navbar-inner">   
		    <div class="container container2">
		        <div align="center" id="header">
		            <div class="pull-left fullwidth nopad5" id="search">
		                <div class="input-prepend">
		                	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
		                	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-search -->

	<div class="dialogHolder" id="addToDPDialog">
	   <div class="dialogContent">
	      <div id="addToDPDialogContent">
	         <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
	         <div id="addToDPButtons">
	            <div class="dialogBody">
	               <div class="addDialog">
	                  <h4>What are you adding?</h4>
	                  <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
	               </div>
	               <div class="addItems">
	                  <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
	                     <h6 class="itemName">Deal / Product</h6>
	                     <div class="itemImage">
	                        <img src="images/dealIcon.png">
	                     </div>
	                     <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
	                  </a>
	                  <a href="#" class="itemContainer" id="addCouponLink">
	                     <h6 class="itemName">Coupon</h6>
	                     <div class="itemImage">
	                        <img src="images/couponIcon.png">
	                     </div>
	                     <div class="itemDescription">A coupon to be used online or in store</div>
	                  </a>
	                  <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
	                     <h6 class="itemName">Topic / Photo</h6>
	                     <div class="itemImage">
	                        <img src="images/topicIcon.png">
	                     </div>
	                     <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
	                  </a>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="afterheader"></div>
	<div id="container">
		<div class="container container2">
			<div id="oldMainContent">
				<div id="main_content" class="content">
					<div class="content_left">
					    <div class="padb10 padt5 bold font14">
					        <div class="fright"><a class="bold font14" href="#">Post New Topic</a></div>
					        <div class="clear"></div>
					    </div>
					    <div>
					        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="questionsTable">
					            <tbody>
					                <tr>
					                    <th align="center">Answer</th>
					                    <th width="380">Title</th>
					                    <th align="center">View</th>
					                    <th align="center">Score</th>
					                    <th align="left">Last Post</th>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">0</div></td>
					                    <td class="title"><a href="#">Be WARNED....</a>
					                        <div class="lt_grey font11 mart3">LilRed77</div>
					                    </td>
					                    <td class="view">231</td>
					                    <td class="vote">4</td>
					                    <td class="posted"><span class="time">May 27, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">52</div></td>
					                    <td class="title"><a href="#">Again New Change? (Suggestions)</a>
					                        <div class="lt_grey font11 mart3">praveensapkota</div>
					                    </td>
					                    <td class="view">2461</td>
					                    <td class="vote">35</td>
					                    <td class="posted"><span class="time">May 07, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">0</div></td>
					                    <td class="title"><a href="#">Kohl's Coupon Page</a>
					                        <div class="lt_grey font11 mart3">tr1plication</div>
					                    </td>
					                    <td class="view">256</td>
					                    <td class="vote">0</td>
					                    <td class="posted"><span class="time">Apr 15, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">1</div></td>
					                    <td class="title"><a href="#">Duplicating..</a>
					                        <div class="lt_grey font11 mart3">taratstock</div>
					                    </td>
					                    <td class="view">437</td>
					                    <td class="vote">8</td>
					                    <td class="posted"><span class="time">Mar 09, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">0</div></td>
					                    <td class="title"><a href="#">Leonard Nimoy, Actor, Director, and 'Star Trek' Icon, Dies at 83</a>
					                        <div class="lt_grey font11 mart3">THartz606</div>
					                    </td>
					                    <td class="view">512</td>
					                    <td class="vote">6</td>
					                    <td class="posted"><span class="time">Feb 27, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">2</div></td>
					                    <td class="title"><a href="#">Can we add photo in comment section?</a>
					                        <div class="lt_grey font11 mart3">LilRed77</div>
					                    </td>
					                    <td class="view">460</td>
					                    <td class="vote">5</td>
					                    <td class="posted"><span class="time">Feb 15, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">6</div></td>
					                    <td class="title"><a href="#">Months of having issues posting!</a>
					                        <div class="lt_grey font11 mart3">taratstock</div>
					                    </td>
					                    <td class="view">842</td>
					                    <td class="vote">4</td>
					                    <td class="posted"><span class="time">Feb 05, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">3</div></td>
					                    <td class="title"><a href="#">Tax Info</a>
					                        <div class="lt_grey font11 mart3">AudJand</div>
					                    </td>
					                    <td class="view">420</td>
					                    <td class="vote">5</td>
					                    <td class="posted"><span class="time">Feb 03, 2015</span></td>
					                </tr>
					                <tr>
					                    <td class="answer"><div class="numAnswer">0</div></td>
					                    <td class="title">
					                        <a href="#">Interests Dissapeared?</a>
					                        <div class="lt_grey font11 mart3">alecupope</div>
					                    </td>
					                    <td class="view">
					                        370
					                    </td>
					                    <td class="vote">
					                        0
					                    </td>
					                    <td class="posted">
					                        <span class="time">Feb 03, 2015</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">5</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">How to delete Expired Deals/Coupons from Your "Interest"</a>
					                        <div class="lt_grey font11 mart3">blackfoot</div>
					                    </td>
					                    <td class="view">
					                        458
					                    </td>
					                    <td class="vote">
					                        6
					                    </td>
					                    <td class="posted">
					                        <span class="time">Feb 02, 2015</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">0</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">Why only articles in front page? Are those deals?</a>
					                        <div class="lt_grey font11 mart3">praveensapkota</div>
					                    </td>
					                    <td class="view">
					                        421
					                    </td>
					                    <td class="vote">
					                        5
					                    </td>
					                    <td class="posted">
					                        <span class="time">Jan 27, 2015</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">5</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">January 2015 Payment not showing in PayPal???</a>
					                        <div class="lt_grey font11 mart3">LindaKNor</div>
					                    </td>
					                    <td class="view">
					                        816
					                    </td>
					                    <td class="vote">
					                        8
					                    </td>
					                    <td class="posted">
					                        <span class="time">Jan 15, 2015</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">4</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">Happy New Year 2015 to all DealsPlus Family</a>
					                        <div class="lt_grey font11 mart3">praveensapkota</div>
					                    </td>
					                    <td class="view">
					                        832
					                    </td>
					                    <td class="vote">
					                        41
					                    </td>
					                    <td class="posted">
					                        <span class="time">Jan 01, 2015</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">3</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">Has anyone purchased from Cowboom?</a>
					                        <div class="lt_grey font11 mart3">praveensapkota</div>
					                    </td>
					                    <td class="view">
					                        571
					                    </td>
					                    <td class="vote">
					                        1
					                    </td>
					                    <td class="posted">
					                        <span class="time">Dec 29, 2014</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">0</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">Store Pages Unblocked</a>
					                        <div class="lt_grey font11 mart3">tr1plication</div>
					                    </td>
					                    <td class="view">
					                        557
					                    </td>
					                    <td class="vote">
					                        13
					                    </td>
					                    <td class="posted">
					                        <span class="time">Dec 19, 2014</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">3</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">Why Do My Deals Disappear?</a>
					                        <div class="lt_grey font11 mart3">taratstock</div>
					                    </td>
					                    <td class="view">
					                        501
					                    </td>
					                    <td class="vote">
					                        0
					                    </td>
					                    <td class="posted">
					                        <span class="time">Dec 18, 2014</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">1</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">Is it OK if we remove Deals/Coupons From Interests We Moderate?</a>
					                        <div class="lt_grey font11 mart3">alecupope</div>
					                    </td>
					                    <td class="view">
					                        775
					                    </td>
					                    <td class="vote">
					                        -1
					                    </td>
					                    <td class="posted">
					                        <span class="time">Dec 18, 2014</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">7</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">"Interests"</a>
					                        <div class="lt_grey font11 mart3">erick99</div>
					                    </td>
					                    <td class="view">
					                        727
					                    </td>
					                    <td class="vote">
					                        15
					                    </td>
					                    <td class="posted">
					                        <span class="time">Dec 05, 2014</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">5</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">Deals not posting now?</a>
					                        <div class="lt_grey font11 mart3">dddsss</div>
					                    </td>
					                    <td class="view">
					                        669
					                    </td>
					                    <td class="vote">
					                        9
					                    </td>
					                    <td class="posted">
					                        <span class="time">Dec 03, 2014</span>
					                    </td>
					                </tr>
					                <tr>
					                    <td class="answer">
					                        <div class="numAnswer">2</div>
					                    </td>
					                    <td class="title">
					                        <a href="#">W-8 Form</a>
					                        <div class="lt_grey font11 mart3">smart_code</div>
					                    </td>
					                    <td class="view">
					                        682
					                    </td>
					                    <td class="vote">
					                        0
					                    </td>
					                    <td class="posted">
					                        <span class="time">Dec 03, 2014</span>
					                    </td>
					                </tr>
					            </tbody>
					        </table>
					    </div>
					    <div class="padl3 padb15 padt15">
					        <span class="box_selected">1</span> 
					        <a class="box_a" href="#">2</a> 
					        <a class="box_a" href="#">3</a> 
					        <a class="box_a" href="#">4</a> 
					        <a class="box_a" href="#">5</a> 
					        <a class="box_a" href="#">6</a> 
					        <a class="box_a" href="#">7</a> 
					        <a class="box_a" href="#">8</a> 
					        <a class="box_a" href="#">9</a> 
					        <a class="box_a" href="#0">10</a> 
					        <a class="box_a" href="#">Next »</a>
					    </div>
					</div><!-- #content_left -->
					<div class="content_right">
					    <div class="padb15">
					        <form action="/answer" method="get">
					            <input type="text" onfocus="javascript:this.value=''" value="Search Ask &amp; Share" autocomplete="off" id="searchQAInput" name="keyword" maxlength="100">
					            <input type="submit" class="purpleButton" value="Search">
					        </form>
					    </div>
					    <div class="module_sm">
					        <div class="content">
					            <h3>Categories</h3>
					            <ul id="questionCategory">
					                <li><a href="#">All</a></li>
					                <li><a href="#">Tips To Save Money</a></li>
					                <li><a href="#">Recommended Products</a></li>
					                <li><a href="#">I Saved Money!</a></li>
					                <li><a href="#">Shopping Experiences</a></li>
					                <li><a href="#">Product/Gift Advice</a></li>
					                <li><a href="#">Deals and Coupons</a></li>
					                <li><a href="#">Site Questions or Suggestions</a></li>
					                <li><a href="#">Mom Chat</a></li>
					                <li><a href="#">Off Topic</a></li>
					            </ul>
					        </div>
					    </div>
					</div><!-- #content_right -->
				</div><!-- #main_content -->
			</div><!-- #oldMainContent -->
		</div><!-- #container2 -->
		<div id="footericon" class="bigfooticon colorbackwhite handpoint">
			<i class="icon-eject"></i>
		</div><!-- #footericon -->
		<div id="footer" class="colorback" style="display: block;">
			<div class="container">
				<div class="inner footmenu">
					<div class="span3">
						<div class="btn-group hsds">
						   <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
						   </a>
						   <ul class="dropdown-menu bottom-up">
						      <li><a href="about.php">About Us</a></li>
						      <li><a href="#">Delivery Information</a></li>
						      <li><a href="privacy.php">Privacy Policy</a></li>
						      <li><a href="tos.php">Terms &amp; Conditions</a></li>
						   </ul>
						</div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Contact Us</a></li>
					         <li><a href="#">Returns</a></li>
					         <li><a href="#">Site Map</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Brands</a></li>
					         <li><a href="#">Gift Vouchers</a></li>
					         <li><a href="#">Affiliates</a></li>
					         <li><a href="#">Specials</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">My Account</a></li>
					         <li><a href="#">Order History</a></li>
					         <li><a href="#">Wish List</a></li>
					         <li><a href="#">Newsletter</a></li>
					      </ul>
					   </div>
					</div>
				</div><!-- #footmenu -->
			</div>
		</div><!-- #footer -->
<div id="powered" class="powered_pos"><p>Powered By <a href="/" title="Best Deals & Coupons">Couponpicks.com</a> &copy; 2015</p></div>
	</div><!-- #container -->
	<a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
		<span id="toTopHover"></span>
		<i class="icon-chevron-up icon-large"></i>
	</a>
</body>
</html>
