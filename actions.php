<?php

    /*
    * To change this template, choose Tools | Templates
    * and open the template in the editor.
    */
    error_reporting(0);
    session_start(); 
    include("constants.php");

    if(isset($_REQUEST["action"]))
    {
        $action = $_REQUEST["action"];
    }
    else 
    {
        $action = "exit";
    }
               
    include("classes/DatabaseDAO.php");
    $db = new DatabaseDAO();

    if($action=="createAccount")
    {      
        $name   = $_POST['name']; 
        $lname   = $_POST['lname'];
        $email   = $_POST['email'];
        $mobile  = $_POST['mobile'];       
        $pwd   = $_POST['pwd'];
        $re_pwd   = $_POST['re_pwd'];
        $gender   = $_POST['gender'];
        $city  = $_POST['city'];

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
        {
            echo "<em>Please Enter Valid Email ID</em>";
            exit();
        } 
        if($pwd!=$re_pwd)
        {
            echo "<em>Password and Confirm Password Not matching</em>";
            exit();
        }
        $email1 = $db ->quote($email);        
        $query1 = "select * from users where EmailId=$email1 ";  

        $rows = $db -> select($query1);  

        $loop=count($rows);

        if($loop==1)
        {
            echo "<em>Account already exists with this Email-ID</em>";
            exit();
        }

        $id = session_id();
        $query = "INSERT into `users`(FirstName,LastName,EmailId,Password,Gender,verifyCode,mobile,city) 
        values('$name','$lname','$email','$pwd','$gender','$id','$mobile','$city') " ;      
        // Update the Status into the database
        $result = $db -> query($query);
        $_SESSION["verifyID"] = $id;
        // To send HTML mail, the Content-type header must be set

        require_once("Mail.php");  
        $from = "Playstudio Admin <playstudio@tlrfindia.com>"; 
        $to = $name." ".$lname. " <".$email.">" . ""; 
        $subject = SIGNUP_SUBJECT; 
        $body = "Hi " . $name . ", <br><br> Thank you for registering on Playstud.io. <br><br>If you've made it this far, then you're surely capable of more. <a href='".SIGNUP_VERIFY_LINK.$id."&emailid=" . $email . "'>Click here to verify your account</a>, and start making an impact immediately. <br><br> Regards,<br> Playstud.io Team.";  
        $host = SMTP_HOST; 
        $username = SMTP_USER; 
        $password = SMTP_PASS;  
        $headers = array('MIME-Version' => '1.0', 'Content-Type' => "text/html; charset=\"ISO-8859-1\"", 'From' => $from,   'To' => $to, 'Subject' => $subject);
        $smtp = Mail::factory('smtp',   array ('host' => $host,     'auth' => true,     'username' => $username,     'password' => $password));  
        $mail = $smtp->send($to, $headers, $body);  
        if (PEAR::isError($mail)) {   
            // echo("<p>" . $mail->getMessage() . "</p>");  
            echo ("<p>ERROR : Internal error occurred while sending verification mail.</p>");
        } 
        else {   echo("<p>Account has been created. Please verify your email account to login.</p>");  }
        exit();
    }
    if($action=="forgotPassword")
    {             
        $email   = $_POST['forgot_email'];

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) 
        {
            echo "<em>Please Enter Valid Email ID</em>";
            exit();
        }           
        $query = "select Password from users where EmailId=".$db ->quote($email);       
        $rows = $db -> select($query);        
        $loop=count($rows);
        $password= ""; 
        if($loop==0)
        {        
            echo "<em>Entered Email ID not found in our PlayStudio</em>";
            exit();
        }
        else
        {
            $password = $rows[0]["Password"];
        }

        // To send HTML mail, the Content-type header must be set
        $headers  = 'MIME-Version: 1.0' . "\r\n";    
        $headers .= 'From: Admin <admin@playstudio.com>' . "\r\n";
        $headers .= 'To: '.$email.' <'.$email.'>' . "\r\n";   

        $msg = "Your Password is :".$password;
        if(!mail($email,FORGOT_PASSWORD,$msg,$headers))
        {
            echo "<em>ERROR : Internal Error occurred while sending Welcome mail</em>";
        }
        else {
            echo "<h1>Mail Sent Successfully</h1>";
        }
        exit();
    }

  


    if($action=="loginAccount")
    {                                                                                                                      
        if(!isset($_POST["login_uid"]) && !isset($_POST["login_pwd"]) ){
            echo "<em>Please Fill All Mandatory fields</em>";    
            exit();
        }
        else
        {
            $login_uid     = $db -> quote($_POST['login_uid']);
            $login_pwd   = $db -> quote($_POST['login_pwd']);       
        }
        $query = "SELECT * FROM users where `EmailId`=$login_uid  and `Password`=$login_pwd ;";
        $rows = $db -> select($query);        
        $loop=count($rows);
        if($loop==1)
        {         
            if($rows[0]["verifyStatus"]==1)
            {
                $_SESSION["userID"] = $rows[0]["user_id"]; 
                $_SESSION["userPic"] = $rows[0]["ProfilePic"];
                $_SESSION["userName"] = $rows[0]["FirstName"]." ".$rows[0]["LastName"];
                $_SESSION["userEmail"] = $rows[0]["EmailId"];
                $_SESSION["userMobile"]=$rows[0]["mobile"];
                $_SESSION["userCity"]=$rows[0]["city"];

                 
                  $head_user_id = $_SESSION["userID"];
                  $head_query ="SELECT SUM(ABS(`ps_downloaded` + `qb_downloaded`)) AS `total`  ,mobile
                    FROM `users`  WHERE `user_id` = $head_user_id";
                  $head_records = $db -> select($head_query);
                  $_SESSION["impact_total"] = $head_records[0]['total'];

                                    
               

            }            
            else
            {
                echo "<em>Please verify your EMAIL in order to access PlayStudio system</em>";   
                exit();
            }    
            echo "<h1>Login Successfully ...</h1>";   
        }
        else
        {
            echo "<em>Invalid Login Credentials</em>";    
        }
        exit();
    }
          

?>
