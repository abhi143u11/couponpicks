<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>About | Couponpicks</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {
			$(".various").fancybox({
				maxWidth	: 630,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		});
	</script>
</head>
<body>
	<div class="navbar navbar-fixed-top topbar">
	   <div class="navbar-inner kasdf">
	      <div class="container container2 wrap-menu">
	         <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
	         <div class="pull-left firstsearch" id="search">
               	<div class="input-prepend">
	               	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
	               	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
               	</div>
            </div>
	         <div id="headerunder" class="pull-right colorback"></div>
	         <div id="header" class="pull-right topcart colorback">
	            <ul id="userNav">
				   <li>
				      <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
				      </a>
				      <div class="userSubMenu menu">
				         <div class="menuWrapper">
				            <ul>
				               <li><a href="#">My Profile</a></li>
				               <li><a href="#">My Deals</a></li>
				               <li><a href="#">My Coupons</a></li>
				               <li><a href="#">Notifications</a></li>
				               <li><a href="#">Messages</a></li>
				               <li><a href="#">Saved Stores</a></li>
				               <li><a href="#">Find Friends</a></li>
				               <li><a class="subUserMenuLink" href="#">Settings</a></li>
				               <li><a href="#">Sign Out</a></li>
				            </ul>
				         </div>
				      </div>
				   </li>
				</ul>
				<a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
					<img class="icon-plus-button" alt="add" src="images/blank.png">
				</a>
	         </div>
	         <div class="topmenu">
	            <div class="dropdown">
	            	<div class="click-toggle">
	            		<a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
		               <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
		               	<?php
							if ($cats->num_rows > 0) {
								while($row = $cats->fetch_assoc()) { ?>
			                  <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
			                  </li>
		                <?php
							}
						}
						?>	
		               </ul>
	            	</div>
	               <a class="padright" id="wishlist-total" href="#">My Feed</a>
	               <a href="#">Popular</a>
	               <div class="btn-group little-select">
	                  	<a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
	             			<i class="icon-reorder"></i>
	                  	</a>     	
	                  	<!-- <ul class="dropdown-menu">
	                     	<form enctype="multipart/form-data" method="post" action="#">
	                        	<div id="currency">Currency<br>
		                           <a title="Euro">€</a>
		                           <a title="Pound Sterling">£</a>
		                           <a title="US Dollar"><b>$</b></a>
	                        	</div>
	                     	</form>
	                  	</ul> -->
	                  	<div id="option-list">
	                  		<div class="menuWrapper">
	                  			<ul class="menuTopIcons ">
								   <li><a href="#">
								         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">My Feed</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Popular</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Fresh</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Heating Up</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Coupon Codes</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Printable Coupons</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Interests</div>
								      </a></li>
								</ul><!-- #menuTopIcons -->
								<ul>
								   <li><a href="#">Apps</a></li>
								   <li><a href="#">Automotive</a></li>
								   <li><a href="#">Bed &amp; Bath</a></li>
								   <li><a href="#">Computers &amp; Software</a></li>
								   <li><a href="#">Electronics</a></li>
								   <li><a href="#">Entertainment</a></li>
								   <li><a href="#">Freebies</a></li>
								   <li><a href="#">Furniture &amp; Decor</a></li>
								   <li><a href="#">Games</a></li>
								</ul>
								<ul>
								   <li><a href="#">Gifts &amp; Flowers</a></li>
								   <li><a href="#">Grocery &amp; Food</a></li>
								   <li><a href="#">Health &amp; Beauty</a></li>
								   <li><a href="#">Home &amp; Garden</a></li>
								   <li><a href="#">Kids &amp; Baby</a></li>
								   <li><a href="#">Kitchen &amp; Dining</a></li>
								   <li><a href="#">Laptop</a></li>
								   <li><a href="#">Men</a></li>
								   <li><a href="#">News</a></li>
								</ul>
								<ul>
								   <li><a href="#">Office &amp; School</a></li>
								   <li><a href="#">Other</a></li>
								   <li><a href="#">Pets</a></li>
								   <li><a href="#">Sports &amp; Outdoor</a></li>
								   <li><a href="#">Tax &amp; Finance</a></li>
								   <li><a href="#">Toys</a></li>
								   <li><a href="#">Travel &amp; Tickets</a></li>
								   <li><a href="#">TV</a></li>
								   <li><a href="#">Women</a></li>
								   <li><a href="#">DealsPlus Exclusive</a></li>
								</ul>
	                  		</div><!-- #menuWrapper -->
	                  		<div class="aboutUsLinks">
							   <div class="aboutUsSection">
							      <a href="about.php">About Us</a>
							      <span>|</span>
							      <a target="_blank" href="blog.php">Blog</a>
							      <span>|</span>
							      <a href="#">Contact</a>
							      <span>|</span>
							      <a href="privacy.php">Privacy Policy</a>
							      <span>|</span>
							      <a href="tos.php">Terms of Use</a>
							      <div class="categoryMenuSocial">
							         <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
							         <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
							         <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
							         <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
							      </div>
							   </div>
							</div><!-- #aboutUsLinks -->
	                  	</div><!-- #option-list -->
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div><!-- #navbar-fixed-top -->

	<div class="navbar navbar-static-top menubar responsive-menu">
		<div class="navbar-inner">  
		    <div class="dropdown drsp">
		        <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
		        </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                    	print_r($cats);
						if ($cats->num_rows > 0) {
							while($row = $cats->fetch_assoc()) { ?>
		                  	<li>
		                  		<a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
		                  	</li>
		            <?php
							}
						}
					?> 
                </ul>
		        <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
		        <a href="#">My Account</a>
		        <div class="btn-group little-select">
		            <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
		            <ul class="dropdown-menu">
		                <form enctype="multipart/form-data" method="post" action="index.php">
						  	<div id="currency">Currency<br>
				                <a title="Euro">€</a>
				                <a title="Pound Sterling">£</a>
				                <a title="US Dollar"><b>$</b></a>
						  	</div>
						</form>
		            </ul>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-menu -->

	<div class="navbar navbar-static-top menubar responsive-search">
		<div class="navbar-inner">   
		    <div class="container container2">
		        <div align="center" id="header">
		            <div class="pull-left fullwidth nopad5" id="search">
		                <div class="input-prepend">
		                	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
		                	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-search -->

	<div class="dialogHolder" id="addToDPDialog">
	   <div class="dialogContent">
	      <div id="addToDPDialogContent">
	         <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
	         <div id="addToDPButtons">
	            <div class="dialogBody">
	               <div class="addDialog">
	                  <h4>What are you adding?</h4>
	                  <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
	               </div>
	               <div class="addItems">
	                  <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
	                     <h6 class="itemName">Deal / Product</h6>
	                     <div class="itemImage">
	                        <img src="images/dealIcon.png">
	                     </div>
	                     <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
	                  </a>
	                  <a href="#" class="itemContainer" id="addCouponLink">
	                     <h6 class="itemName">Coupon</h6>
	                     <div class="itemImage">
	                        <img src="images/couponIcon.png">
	                     </div>
	                     <div class="itemDescription">A coupon to be used online or in store</div>
	                  </a>
	                  <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
	                     <h6 class="itemName">Topic / Photo</h6>
	                     <div class="itemImage">
	                        <img src="images/topicIcon.png">
	                     </div>
	                     <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
	                  </a>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="afterheader"></div>
	<div id="container">
		<div class="container container2">
			<div style="margin:10px auto;">
				<div class="infoMenu box expandMenuBar">
				   <div class="menuSection">
				      <h3>CouponPicks</h3>
				      <ul>
				         <li><a href="about.php">About Us</a></li>
				         <li><a href="jobs.php">Jobs</a></li>
				         <li><a target="_blank" href="#">Blog</a></li>
				         <li><a href="cp_tools.php">Tools</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Help</h3>
				      <ul>
				         <li><a href="#">Ask &amp; Share</a></li>
				         <li><a href="#">Contact</a></li>
				         <li><a href="faqs.php">FAQs</a></li>
				         <li><a href="privacy.php">Privacy Policy</a></li>
				         <li><a href="tos.php">Terms of Use</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Business With Us</h3>
				      <ul>
				         <li>
				            <a href="retailers.php">Retailers</a>
				         </li>
				         <li><a href="#">Advertisers</a></li>
				      </ul>
				   </div>
				</div><!-- #infoMenu -->
				<div class="infoContent box">
				   <h2>We're hiring!</h2>
				   <div class="jobDesc">
				      DealsPlus is always looking for fun and exceptional people to grow with our expanding company.  If you're interested in joining our team, please send your resume to: <a href="mailto:jobs@couponpicks.com">jobs@couponpicks.com</a>
				   </div>
				   <div class="jobRow">
				      <h3>Operations Engineer</h3>
				      <p>We're currently looking for a highly self-motivated, driven individual who is passionate about system administration, networking, and computer technology.</p>
				      <p></p>
				      <p>You will be a core member of our development team and help design, build, and maintain systems necessary to continue our rapid growth.</p>
				      <br>
				      <b>Requirements:</b>
				      <ul>
				         <li>Must have a bachelor degree or in the process of obtaining one (a degree in a Technology related field is a plus)</li>
				         <li>Have strong Linux system administration skills</li>
				         <li>Some understanding of networking hardware, software, and protocols</li>
				         <li>Have strong trouble shooting skills and attention to detail</li>
				         <li>Team player - ability to work cooperatively with the other engineers</li>
				         <li>Hands-on. Completes task quickly and thoroughly</li>
				      </ul>
				      <br>
				      <b>Extra Credit:</b>
				      <ul>
				         <li>Red Hat Certification</li>
				         <li>Knowledge or experience with Chef (configuration management)</li>
				         <li>Experience building fault tolerant, scalable systems</li>
				         <li>Experience with VMware or KVM</li>
				      </ul>
				      <br>
				      <b>Bonus Points:</b>
				      <ul>
				         <li>DBA skills</li>
				         <li>Proficiency in Ruby, PHP, or Python</li>
				         <li>Familiarity with Git or other version control system</li>
				      </ul>
				      <br>
				      <p>Compensation: commensurate with experience</p>
				      <p>This is a full-time job.</p>
				   </div>
				   <div class="jobRow">
				      <h3>Web Developer</h3>
				      <p>As a web developer at Sazze, you will be a core member of our development team and help design, build, and maintain software necessary to continue our rapid growth.</p>
				      <br>
				      <b>Requirements:</b>
				      <ul>
				         <li>Have strong HTML, CSS, Javascript, and PHP skills</li>
				         <li>Experience with a major Javascript framework</li>
				         <li>Extensive knowledge of Mysql and Redis</li>
				         <li>Have strong OOP skills</li>
				         <li>Have strong trouble shooting skills and attention to detail</li>
				         <li>Team player - ability to work cooperatively with the other engineers</li>
				         <li>Hands-on. Completes task quickly and thoroughly</li>
				         <li>A quick study and can be self taught</li>
				      </ul>
				      <br>
				      <b>Desired Skills &amp; Experience:</b>
				      <ul>
				         <li>Experience with jQuery</li>
				         <li>Solid understanding of PHP 5.3.3+ OOP features</li>
				         <li>Experience with developing high traffic web sites</li>
				         <li>Familiarity with Git or other version control system</li>
				         <li>Some understanding of networking protocols</li>
				         <li>Experience with Photoshop or other image manipulation tools</li>
				      </ul>
				      <br>
				      <p>Compensation: commensurate with experience</p>
				      <p>This is a full-time job.</p>
				   </div>
				   <div class="jobRow">
				      <h3>Affiliate Marketing Manager</h3>
				      <p>dealsplus is currently seeking an experienced Affiliate Marketing Manager. The qualified candidate will already have established relationships within all the major affiliate networks. The Affiliate Marketing Manager will be responsible for contacting merchants and Affiliate Managers to set up and/or maintain the various affiliate networks/promotions on dealsplus.</p>
				      <br>
				      <b>Job description, including but not limited to:</b>
				      <ul>
				         <li>Identify key affiliate partners</li>
				         <li>Contacting new and existing merchants</li>
				         <li>Channel sales management, forecasting, and achievement of performance goals</li>
				         <li>Prioritize initiatives based on clear objectives and performance numbers</li>
				         <li>Work closely with site moderators to maximize sales/performance</li>
				      </ul>
				      <br>
				      <b>Qualifications:</b>
				      <ul>
				         <li>Bachelor Degree in Business or marketing or equivalent experience</li>
				         <li>Excellent communication skills (written and verbal)</li>
				         <li>Understanding of e-commerce, the retail space and deal sites</li>
				         <li>Self-motivated</li>
				         <li>Excellent People Skills:</li>
				         <li>3-5 years of Affiliate Marketing experience</li>
				         <li>Strong online research skills</li>
				         <li>Strong merchant relationship building skills</li>
				         <li>Strong knowledge of online advertising</li>
				      </ul>
				      <p>Compensation: commensurate with experience</p>
				      <p>This is a full-time job.</p>
				   </div>
				   <div class="jobRow">
				      <h3>System Administrator (Linux)</h3>
				      <p>We are currently looking for a highly self-motivated, driven individual who is passionate about system administration, networking, and computer technology.</p>
				      <p></p>
				      <p>You will be a core member of our development team and help design, build, and maintain systems necessary to continue our rapid growth.</p>
				      <br>
				      <b>Requirements:</b>
				      <ul>
				         <li>Have strong Linux system administration skills</li>
				         <li>Proficiency in Ruby, Python, Perl, or PHP</li>
				         <li>Some understanding of networking hardware, software, and protocols</li>
				         <li>Have strong trouble-shooting skills and attention to detail</li>
				         <li>Ability to work cooperatively with other engineers</li>
				      </ul>
				      <br>
				      <b>Desired Skills &amp; Experience:</b>
				      <ul>
				         <li>Red Hat Certification</li>
				         <li>Experience with Chef, Puppet, or similar configuration management system</li>
				         <li>Familiarity with Git or other version control system</li>
				         <li>DBA skills (Mysql, Redis)</li>
				         <li>Experience with RedHat KVM or VMware</li>
				         <li>Experience building fault tolerant, scalable systems</li>
				      </ul>
				      <br>
				      <p>Compensation: commensurate with experience</p>
				      <p>This is a full-time job.</p>
				   </div>
				   <div class="jobRow">
				      <h3>MySQL Database Administrator</h3>
				      <p>You will be a core member of our development team and will work with the Engineering/Development and Operations teams to manage their database systems.</p>
				      <br>
				      <b>Responsibilities:</b>
				      <ul>
				         <li>Identify and recommend tools and services for managing database infrastructure</li>
				         <li>Hardware sizing requirements for database loads based on efficient use of multicore systems</li>
				         <li>Update and tune MySQL instances in a high volume environment</li>
				         <li>Have strong trouble-shooting skills and attention to detail</li>
				         <li>Design, architect, and build MySQL databases</li>
				      </ul>
				      <br>
				      <b>Requirements:</b>
				      <ul>
				         <li>3+ years MYSQL DBA experience in a LAMP environment working with Centos/Fedora/RedHat</li>
				         <li>Network and I/O performance tuning</li>
				         <li>MySQL replication (MasterMaster, MasterSlave, cluster replication) and sharding</li>
				         <li>Solid knowledge of physical and logical database design</li>
				         <li>Solid knowledge in schema design principles with ability to apply best practices for scalable designs</li>
				         <li>MySQL 5.x (InnoDB and MyISAM engines)</li>
				         <li>Proficiency in a scripting language (php or ruby preferred)</li>
				         <li>Have strong trouble shooting skills and attention to detail</li>
				         <li>Team player ability to work cooperatively with the other engineers</li>
				         <li>Handson. Completes task quickly and thoroughly</li>
				         <li>A quick study and can be self taught</li>
				      </ul>
				      <br>
				      <b>Extra Credit:</b>
				      <ul>
				         <li>Experience with MySQL Galera replication (MultiMaster)</li>
				         <li>Experience with Redis programming and management</li>
				         <li>Experience with monitoring systems</li>
				         <li>Experience with RDBMS in support of high traffic web sites</li>
				         <li>Familiarity with Git or other version control system</li>
				         <li>Familiarity with php application integration</li>
				         <li>Familiarity with nosql databases</li>
				      </ul>
				      <br>
				      <p>Compensation: commensurate with experience</p>
				      <p>This is a full-time job.</p>
				   </div>
				</div><!-- #infoContent -->
			</div>
		</div><!-- #container2 -->
		<div id="footericon" class="bigfooticon colorbackwhite handpoint">
			<i class="icon-eject"></i>
		</div><!-- #footericon -->
		<div id="footer" class="colorback" style="display: block;">
			<div class="container">
				<div class="inner footmenu">
					<div class="span3">
						<div class="btn-group hsds">
						   <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
						   </a>
						   <ul class="dropdown-menu bottom-up">
						      <li><a href="about.php">About Us</a></li>
						      <li><a href="#">Delivery Information</a></li>
						      <li><a href="privacy.php">Privacy Policy</a></li>
						      <li><a href="tos.php">Terms &amp; Conditions</a></li>
						   </ul>
						</div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Contact Us</a></li>
					         <li><a href="#">Returns</a></li>
					         <li><a href="#">Site Map</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Brands</a></li>
					         <li><a href="#">Gift Vouchers</a></li>
					         <li><a href="#">Affiliates</a></li>
					         <li><a href="#">Specials</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">My Account</a></li>
					         <li><a href="#">Order History</a></li>
					         <li><a href="#">Wish List</a></li>
					         <li><a href="#">Newsletter</a></li>
					      </ul>
					   </div>
					</div>
				</div><!-- #footmenu -->
			</div>
		</div><!-- #footer -->
<div id="powered" class="powered_pos"><p>Powered By <a href="/" title="Best Deals & Coupons">Couponpicks.com</a> &copy; 2015</p></div>
	</div><!-- #container -->
	<a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
		<span id="toTopHover"></span>
		<i class="icon-chevron-up icon-large"></i>
	</a>
</body>
</html>
