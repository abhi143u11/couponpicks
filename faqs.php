<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>About | Couponpicks</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {
			$(".various").fancybox({
				maxWidth	: 630,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		});
	</script>
	<script type="application/javascript">
         function installFirefox (aEvent)
         {
             for (var a = aEvent.target; a.href === undefined;) a = a.parentNode;
             var params = {
                 "Foo": { URL: aEvent.target.href,
                     IconURL: aEvent.target.getAttribute("iconURL"),
                     Hash: aEvent.target.getAttribute("hash"),
                     toString: function () { return this.URL; }
                 }
             };
             InstallTrigger.install(params);
         
             return false;
        }
    </script>
    <script type="text/javascript">
      $('.showBookmarklet').click(function() {
          if ($('.bookmarklet').is(':visible')) {
              $('.bookmarklet').slideUp();
          } else {
              $('.bookmarklet').slideDown();
          }
      });
      
      $('.bookmarkletAddLink').hover(function() {
          $(this).find('a').text('Drag Me to Bookmark Bar');
          $('.dragInstruction').toggle();
      }, function() {
          $(this).find('a').text('Add To CouponPicks');
      });
	</script>
</head>
<body>
	<div class="navbar navbar-fixed-top topbar">
	   <div class="navbar-inner kasdf">
	      <div class="container container2 wrap-menu">
	         <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
	         <div class="pull-left firstsearch" id="search">
               	<div class="input-prepend">
	               	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
	               	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
               	</div>
            </div>
	         <div id="headerunder" class="pull-right colorback"></div>
	         <div id="header" class="pull-right topcart colorback">
	            <ul id="userNav">
				   <li>
				      <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
				      </a>
				      <div class="userSubMenu menu">
				         <div class="menuWrapper">
				            <ul>
				               <li><a href="#">My Profile</a></li>
				               <li><a href="#">My Deals</a></li>
				               <li><a href="#">My Coupons</a></li>
				               <li><a href="#">Notifications</a></li>
				               <li><a href="#">Messages</a></li>
				               <li><a href="#">Saved Stores</a></li>
				               <li><a href="#">Find Friends</a></li>
				               <li><a class="subUserMenuLink" href="#">Settings</a></li>
				               <li><a href="#">Sign Out</a></li>
				            </ul>
				         </div>
				      </div>
				   </li>
				</ul>
				<a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
					<img class="icon-plus-button" alt="add" src="images/blank.png">
				</a>
	         </div>
	         <div class="topmenu">
	            <div class="dropdown">
	            	<div class="click-toggle">
	            		<a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
		               <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
		               	<?php
							if ($cats->num_rows > 0) {
								while($row = $cats->fetch_assoc()) { ?>
			                  <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
			                  </li>
		                <?php
							}
						}
						?>	
		               </ul>
	            	</div>
	               <a class="padright" id="wishlist-total" href="#">My Feed</a>
	               <a href="#">Popular</a>
	               <div class="btn-group little-select">
	                  	<a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
	             			<i class="icon-reorder"></i>
	                  	</a>     	
	                  	<!-- <ul class="dropdown-menu">
	                     	<form enctype="multipart/form-data" method="post" action="#">
	                        	<div id="currency">Currency<br>
		                           <a title="Euro">€</a>
		                           <a title="Pound Sterling">£</a>
		                           <a title="US Dollar"><b>$</b></a>
	                        	</div>
	                     	</form>
	                  	</ul> -->
	                  	<div id="option-list">
	                  		<div class="menuWrapper">
	                  			<ul class="menuTopIcons ">
								   <li><a href="#">
								         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">My Feed</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Popular</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Fresh</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Heating Up</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Coupon Codes</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Printable Coupons</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Interests</div>
								      </a></li>
								</ul><!-- #menuTopIcons -->
								<ul>
								   <li><a href="#">Apps</a></li>
								   <li><a href="#">Automotive</a></li>
								   <li><a href="#">Bed &amp; Bath</a></li>
								   <li><a href="#">Computers &amp; Software</a></li>
								   <li><a href="#">Electronics</a></li>
								   <li><a href="#">Entertainment</a></li>
								   <li><a href="#">Freebies</a></li>
								   <li><a href="#">Furniture &amp; Decor</a></li>
								   <li><a href="#">Games</a></li>
								</ul>
								<ul>
								   <li><a href="#">Gifts &amp; Flowers</a></li>
								   <li><a href="#">Grocery &amp; Food</a></li>
								   <li><a href="#">Health &amp; Beauty</a></li>
								   <li><a href="#">Home &amp; Garden</a></li>
								   <li><a href="#">Kids &amp; Baby</a></li>
								   <li><a href="#">Kitchen &amp; Dining</a></li>
								   <li><a href="#">Laptop</a></li>
								   <li><a href="#">Men</a></li>
								   <li><a href="#">News</a></li>
								</ul>
								<ul>
								   <li><a href="#">Office &amp; School</a></li>
								   <li><a href="#">Other</a></li>
								   <li><a href="#">Pets</a></li>
								   <li><a href="#">Sports &amp; Outdoor</a></li>
								   <li><a href="#">Tax &amp; Finance</a></li>
								   <li><a href="#">Toys</a></li>
								   <li><a href="#">Travel &amp; Tickets</a></li>
								   <li><a href="#">TV</a></li>
								   <li><a href="#">Women</a></li>
								   <li><a href="#">DealsPlus Exclusive</a></li>
								</ul>
	                  		</div><!-- #menuWrapper -->
	                  		<div class="aboutUsLinks">
							   <div class="aboutUsSection">
							      <a href="about.php">About Us</a>
							      <span>|</span>
							      <a target="_blank" href="blog.php">Blog</a>
							      <span>|</span>
							      <a href="#">Contact</a>
							      <span>|</span>
							      <a href="privacy.php">Privacy Policy</a>
							      <span>|</span>
							      <a href="tos.php">Terms of Use</a>
							      <div class="categoryMenuSocial">
							         <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
							         <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
							         <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
							         <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
							      </div>
							   </div>
							</div><!-- #aboutUsLinks -->
	                  	</div><!-- #option-list -->
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div><!-- #navbar-fixed-top -->

	<div class="navbar navbar-static-top menubar responsive-menu">
		<div class="navbar-inner">  
		    <div class="dropdown drsp">
		        <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
		        </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                    	print_r($cats);
						if ($cats->num_rows > 0) {
							while($row = $cats->fetch_assoc()) { ?>
		                  	<li>
		                  		<a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
		                  	</li>
		            <?php
							}
						}
					?> 
                </ul>
		        <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
		        <a href="#">My Account</a>
		        <div class="btn-group little-select">
		            <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
		            <ul class="dropdown-menu">
		                <form enctype="multipart/form-data" method="post" action="index.php">
						  	<div id="currency">Currency<br>
				                <a title="Euro">€</a>
				                <a title="Pound Sterling">£</a>
				                <a title="US Dollar"><b>$</b></a>
						  	</div>
						</form>
		            </ul>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-menu -->

	<div class="navbar navbar-static-top menubar responsive-search">
		<div class="navbar-inner">   
		    <div class="container container2">
		        <div align="center" id="header">
		            <div class="pull-left fullwidth nopad5" id="search">
		                <div class="input-prepend">
		                	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
		                	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-search -->

	<div class="dialogHolder" id="addToDPDialog">
	   <div class="dialogContent">
	      <div id="addToDPDialogContent">
	         <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
	         <div id="addToDPButtons">
	            <div class="dialogBody">
	               <div class="addDialog">
	                  <h4>What are you adding?</h4>
	                  <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
	               </div>
	               <div class="addItems">
	                  <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
	                     <h6 class="itemName">Deal / Product</h6>
	                     <div class="itemImage">
	                        <img src="images/dealIcon.png">
	                     </div>
	                     <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
	                  </a>
	                  <a href="#" class="itemContainer" id="addCouponLink">
	                     <h6 class="itemName">Coupon</h6>
	                     <div class="itemImage">
	                        <img src="images/couponIcon.png">
	                     </div>
	                     <div class="itemDescription">A coupon to be used online or in store</div>
	                  </a>
	                  <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
	                     <h6 class="itemName">Topic / Photo</h6>
	                     <div class="itemImage">
	                        <img src="images/topicIcon.png">
	                     </div>
	                     <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
	                  </a>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="afterheader"></div>
	<div id="container">
		<div class="container container2">
			<div style="margin:10px auto;">
				<div class="infoMenu box expandMenuBar">
				   <div class="menuSection">
				      <h3>CouponPicks</h3>
				      <ul>
				         <li><a href="about.php">About Us</a></li>
				         <li><a href="jobs.php">Jobs</a></li>
				         <li><a target="_blank" href="#">Blog</a></li>
				         <li><a href="cp_tools.php">Tools</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Help</h3>
				      <ul>
				         <li><a href="#">Ask &amp; Share</a></li>
				         <li><a href="#">Contact</a></li>
				         <li><a href="faqs.php">FAQs</a></li>
				         <li><a href="privacy.php">Privacy Policy</a></li>
				         <li><a href="tos.php">Terms of Use</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Business With Us</h3>
				      <ul>
				         <li>
				            <a href="retailers.php">Retailers</a>
				         </li>
				         <li><a href="#">Advertisers</a></li>
				      </ul>
				   </div>
				</div><!-- #infoMenu -->
				<div class="infoContent box">
				    <h3>Frequently Asked Questions</h3>
				    <ul id="faq-questions">
				        <li><a href="#faq-1">What is CouponPicks?</a></li>
				        <li><a href="#faq-45">How do I search for questions and answers about a specific topic?</a></li>
				        <li><a href="#faq-27">What's "reputation"?</a></li>
				        <li><a href="#faq-29">How do I follow a user?</a></li>
				        <li><a href="#faq-28">What happens when I follow a user?</a></li>
				        <li><a href="#faq-3">What is the "+" button?</a></li>
				        <li><a href="#faq-2">What can I do on CouponPicks?</a></li>
				        <li><a href="#faq-4">Is CouponPicks free to use?</a></li>
				        <li><a href="#faq-12">What are "Hot" and "Fresh" deals?  And what's "Heating Up"?</a></li>
				        <li><a href="#faq-6">Why is the price on CouponPicks different from the price on the retailer's website?</a></li>
				        <li><a href="#faq-8">What are custom alerts?</a></li>
				        <li><a href="#faq-10">Does CouponPicks have RSS Feeds and/or daily email subscriptions?</a></li>
				        <li><a href="#faq-30">What if I forgot my password?</a></li>
				        <li><a href="#faq-13">Does CouponPicks have its own language or something ... ?</a></li>
				        <li><a href="#faq-14">How do I submit a deal?</a></li>
				        <li><a href="#faq-15">What information should I include when I submit a deal?</a></li>
				        <li><a href="#faq-16">I just submitted a deal, and its not showing?!</a></li>
				        <li><a href="#faq-17">Oops! I messed up ... How do I edit my deal?</a></li>
				        <li><a href="#faq-18">How does a deal become a "Hot Deal"?  Is there anything I can do to get my deal on the front page?</a></li>
				        <li><a href="#faq-19">What are those plusses under my avatar on my profile page??</a></li>
				        <li><a href="#faq-20">Are there any advantages to being a top CouponPicks user?</a></li>
				        <li><a href="#faq-35">What are CouponPicks Tools?</a></li>
				        <li><a href="#faq-31">What is the CouponPicks Bookmarklet?</a></li>
				        <li><a href="#faq-32">Why isn't the bookmarklet accurate all the time?</a></li>
				        <li><a href="#faq-33">Any tips and tricks for using the CouponPicks Bookmarklet?</a></li>
				        <li><a href="#faq-34">I tried to submit a deal using the bookmarklet, and the submission page came up blank. What gives?</a></li>
				        <li><a href="#faq-39">How do I ask a question or post a tip?</a></li>
				        <li><a href="#faq-40">What kinds of questions can I ask?</a></li>
				        <li><a href="#faq-50">What kinds of tips can I share?</a></li>
				        <li><a href="#faq-41">How do I share an answer?</a></li>
				    </ul>
				    <div>
				        <div class="faq-section">
				            <a name="faq-1"></a>
				            <p>
				                <span class="bold">What is CouponPicks?</span> - <a onclick="$('#answer1').toggle(); return false;" id="answer_link_1" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer1">
				                <div>
				                    CouponPicks is a user-generated social shopping site where users can find, share, discuss, and enjoy the best deals and coupons available online. It is FREE to use, and <a href="#">FREE to join</a>!
				                    <br>
				                    <br> Keep in mind that we are <b>NOT</b> an auction or retailer site, and we don't sell any products directly. We help you find the best bargains out there, then send you to retailers' sites to purchase those products at their discounted prices. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-45"></a>
				            <p>
				                <span class="bold">How do I search for questions and answers about a specific topic?</span> - <a onclick="$('#answer45').toggle(); return false;" id="answer_link_45" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer45">
				                <div>
				                    Just use the search field on the <a href="answer.php">Ask a Question</a> homepage. We’ll do our best to show you questions and answers related to the topic you’re searching. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-27"></a>
				            <p>
				                <span class="bold">What's "reputation"?</span> - <a onclick="$('#answer27').toggle(); return false;" id="answer_link_27" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer27">
				                <div>
				                    Your "reputation" is an ever-changing score that reflects your activity and credibility on CouponPicks. When other users "plus" deals you've submitted or updated, or thumbs up your coupons or comments, your reputation increases. Of course, when other users choose to thumbs down your coupons or comments, your reputation decreases. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-29"></a>
				            <p>
				                <span class="bold">How do I follow a user?</span> - <a onclick="$('#answer29').toggle(); return false;" id="answer_link_29" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer29">
				                <div>
				                    Just click the "follow" button! On a deal page or a user's profile page, you'll find it below the user's name and next to their number of fans. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-28"></a>
				            <p>
				                <span class="bold">What happens when I follow a user?</span> - <a onclick="$('#answer28').toggle(); return false;" id="answer_link_28" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer28">
				                <div>
				                    When you follow a user you become their fan (everyone likes having fans, right?). Also, when viewing Hot or Fresh deals, you can choose to only see those posted by the users you're following. Additionally, you can only receive messages from users that you are following. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-3"></a>
				            <p>
				                <span class="bold">What is the "+" button?</span> - <a onclick="$('#answer3').toggle(); return false;" id="answer_link_3" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height: 18px; display: none;" id="answer3">
				                <div>
				                    When you click the "+" button (or "plus button"), it's kinda like saying "thanks," "awesome," "good find," or simply, "I like it!" You're also casting a vote to have that deal featured in <a href="#">Popular Deals</a>. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-2"></a>
				            <p>
				                <span class="bold">What can I do on CouponPicks?</span> - <a onclick="$('#answer2').toggle(); return false;" id="answer_link_2" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer2">
				                <div>
				                    As a registered CouponPicks member, you can submit deals and coupons, <a href="answer.php">ask questions</a>, share shopping tips, plus the deals you like, follow your favorite users, receive e-mail alerts, and even <a href="#">make money</a>! And if you're not a registered user, you can still browse our huge database of money saving deals and coupons. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-4"></a>
				            <p>
				                <span class="bold">Is CouponPicks free to use?</span> - <a onclick="$('#answer4').toggle(); return false;" id="answer_link_4" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer4">
				                <div>
				                    Yes! CouponPicks is FREE to use and <a href="#">FREE to join</a>. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-12"></a>
				            <p>
				                <span class="bold">What are "Hot" and "Fresh" deals?  And what's "Heating Up"?</span> - <a onclick="$('#answer12').toggle(); return false;" id="answer_link_12" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer12">
				                <div>
				                    <a href="#"><b>Hot Deals</b></a> are the deals you see right on the CouponPicks homepage. Through saves and views, the CouponPicks community has declared these to be the best deals of the moment!
				                    <br>
				                    <br>
				                    <a href="#"><strong>Fresh Deals</strong></a> are recently submitted deals that have not yet become Hot Deals. If you see a Fresh Deal that you think deserves an upgrade to the homepage, make sure to save it, view it, and maybe even leave a comment.
				                    <br>
				                    <br> Deals that are <a href="#"><strong>Heating Up</strong></a> are the ones well on their way to being Hot. If you think a deal that's Heating Up is homepage worthy, then go ahead and give it a thumbs up. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-6"></a>
				            <p>
				                <span class="bold">Why is the price on CouponPicks different from the price on the retailer's website?</span> - <a onclick="$('#answer6').toggle(); return false;" id="answer_link_6" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer6">
				                <div>
				                    Here are the most likely reasons for mismatched prices ...
				                    <ul>
				                        <li>You may need to add the product to your cart, use a coupon code, or send in a mail in rebate in order to get the product at its reduced price. Make sure you read the details in the deal post to see what you need to do in order to receive your discount.
				                        </li>
				                        <li>The price may have dropped! If you see a lower price on the retailers site than on CouponPicks, then it looks like you've found a price drop. Make sure to edit the deal with the new price or leave a comment to let other users know that the product is available at a lower price.
				                        </li>
				                        <li>The deal may have expired! If the retailers price is higher than on CouponPicks, and there are no codes or rebates, then unfortunately, the deal has probably expired. Flag the deal as expired, or leave a comment saying that the deal is no longer available.</li>
				                    </ul>
				                </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-8"></a>
				            <p>
				                <span class="bold">What are custom alerts?</span> - <a onclick="$('#answer8').toggle(); return false;" id="answer_link_8" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer8">
				                <div>
				                    Custom alerts allow you to add a keyword to your alert list. That way, if a deal is added to CouponPicks with your keyword in it, you will be alerted immediately. For example, if you add the keyword "laptop" to your list, you will receive an e-mail every time there's a deal with the word "laptop" in it. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-10"></a>
				            <p>
				                <span class="bold">Does CouponPicks have RSS Feeds and/or daily email subscriptions?</span> - <a onclick="$('#answer10').toggle(); return false;" id="answer_link_10" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer10">
				                <div>
				                    Yes! In fact, we have both.
				                    <br>
				                    <br> CouponPicks has multiple RSS feeds, which you can subscribe to using iGoogle, My Yahoo!, Bloglines, and more:
				                    <br>
				                    <br>
				                    <ul><a class="rss-icon-sm" href="#">Deals</a>
				                        <br>
				                        <a class="rss-icon-sm" href="#">All Deals</a>
				                        <br>
				                        <a class="rss-icon-sm" href="#">Coupons</a>
				                        <br>
				                        <a class="rss-icon-sm" href="#">List of individual category RSS feeds</a>.</ul>
				                    <br> You can also follow your favorite users by subscribing to RSS feeds of their submitted or plussed deals. Simply go to a user's profile, and click the RSS button next to "Submitted Deals" or "Saved Deals"
				                    <br>
				                    <br> We also offer a daily e-mail subscription: To subscribe to "Daily Deals," enter your email at the very bottom of any page, and click Submit. To remove yourself from email subscriptions, click the link: "To stop receiving these emails, you may unsubscribe now." at the bottom of your email. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-30"></a>
				            <p>
				                <span class="bold">What if I forgot my password?</span> - <a onclick="$('#answer30').toggle(); return false;" id="answer_link_30" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer30">
				                <div>
				                    Happens to the best of us. Just go <a href="#">here</a> and enter the email address that you used to sign up for CouponPicks. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-13"></a>
				            <p>
				                <span class="bold">Does CouponPicks have its own language or something ... ?</span> - <a onclick="$('#answer13').toggle(); return false;" id="answer_link_13" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer13">
				                <div>
				                    Sort of! Here's some terminology you might find helpful.
				                    <br>
				                    <ul><b>DP</b> - CouponPicks
				                        <br>
				                        <b>Plusses</b> - The number of times deal has been voted for.
				                        <br>
				                        <b>FS</b> - Free Shipping
				                        <br>
				                        <b>MIR</b> - Mail in Rebate - Provided by the manufacturer. Generally, you must show proof of purchase (receipt, UPC bar code) in order to receive a rebate.
				                        <br>
				                        <b>AR</b> - After Rebate
				                        <br>
				                        <b>BOGO</b> - Buy One Get One
				                        <br>
				                        <b>S/H</b> - Shipping and Handling
				                        <br>
				                        <b>YMMV</b> - Your mileage may vary
				                        <br>
				                        <b>OOS</b> - Out of stock
				                        <br>
				                        <b>FAR</b> - Free After Rebate
				                        <br>
				                        <b>AC</b> - After Coupon
				                        <br>
				                        <b>OTD</b> - Out the Door
				                        <br>
				                        <b>PM</b> - Private Message
				                        <br>
				                        <b>OP</b> - Original Poster
				                        <br>
				                    </ul>
				                </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-14"></a>
				            <p>
				                <span class="bold">How do I submit a deal?</span> - <a onclick="$('#answer14').toggle(); return false;" id="answer_link_14" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer14">
				                <div>
				                    Just click the big "+" button at the top right of any page, then select "Deals." Next, you'll paste the URL of the product you'd like to post, then click "Continue". If your deal isn't a duplicate, it will be posted right to "Fresh Deals"! If the deal is a duplicate, a message will pop up, giving you the option to continue, submit a new deal, or edit. If you'd like to update the price, or edit the details of the deal, go for it. Please only select to continue if the deal you're submitting is different from the duplicate deal displayed. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-15"></a>
				            <p>
				                <span class="bold">What information should I include when I submit a deal?</span> - <a onclick="$('#answer15').toggle(); return false;" id="answer_link_15" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer15">
				                <div>
				                    CouponPicks makes its best attempt to grab all the information about your product that it can, but it's far from perfect. Make sure you give your deal a once over to correct any mistakes and to add any missing information before you submit it. Common mistakes include: missing dollar signs '$', incomplete or inaccurate titles, missing images, etc. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-16"></a>
				            <p>
				                <span class="bold">I just submitted a deal, and its not showing?!</span> - <a onclick="$('#answer16').toggle(); return false;" id="answer_link_16" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer16">
				                <div>
				                    If you are a newly registered member, all of your deal submissions will be moderated by the CouponPicks team, in order to prevent spam. If you are not a newly registered member and your deals still don't seem to be showing up, you can <a href="#">contact Technical Support</a>. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-17"></a>
				            <p>
				                <span class="bold">Oops! I messed up ... How do I edit my deal?</span> - <a onclick="$('#answer17').toggle(); return false;" id="answer_link_17" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer17">
				                <div>
				                    To edit a deal, simply find the deal page and click "Edit," on the right hand side. You can edit most deal information, but only original posters can change/edit deal images. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-18"></a>
				            <p>
				                <span class="bold">How does a deal become a "Hot Deal"?  Is there anything I can do to get my deal on the front page?</span> - <a onclick="$('#answer18').toggle(); return false;" id="answer_link_18" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer18">
				                <div>
				                    There are all kinds of factors used to determine what deals make it to the front page, including the number of saves and clicks a deal receives within a certain period of time, and the number of flags or spam reports associated with it. So when it comes down to it, if you'd like to see your deal on the front page, the best thing you can do is post a really good deal! </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-19"></a>
				            <p>
				                <span class="bold">What are those plusses under my avatar on my profile page??</span> - <a onclick="$('#answer19').toggle(); return false;" id="answer_link_19" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer19">
				                <div>
				                    Those plusses indicate your CouponPicks "level." The more active you are, the more plusses you'll have, and the higher your level will be. When you plus a deal the plus rating of that deal increases by your "level" amount. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-20"></a>
				            <p>
				                <span class="bold">Are there any advantages to being a top CouponPicks user?</span> - <a onclick="$('#answer20').toggle(); return false;" id="answer_link_20" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer20">
				                <div>
				                    There will be soon! Stay tuned, because top users are about to get some special privileges. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-35"></a>
				            <p>
				                <span class="bold">What are CouponPicks Tools?</span> - <a onclick="$('#answer35').toggle(); return false;" id="answer_link_35" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer35">
				                <div>
				                    Our goal is to improve your online shopping experience. And right now, we offer <a href="cp_tools.php">two tools</a> -- the <a href="cp_tools.php">CouponPicks bookmarklet</a> and the <a href="cp_tools.php">CouponPicks Toolbar</a>. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-31"></a>
				            <p>
				                <span class="bold">What is the CouponPicks Bookmarklet?</span> - <a onclick="$('#answer31').toggle(); return false;" id="answer_link_31" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer31">
				                <div>
				                    The CouponPicks Bookmarklet makes it quick and easy to share deals, by letting you upload products to CouponPicks with just the click of a button. By following the directions for your specific browser, you can download the bookmarklet and immediately start submitting deals. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-32"></a>
				            <p>
				                <span class="bold">Why isn't the bookmarklet accurate all the time?</span> - <a onclick="$('#answer32').toggle(); return false;" id="answer_link_32" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer32">
				                <div>
				                    Unfortunately, the bookmarklet won't ever be 100% accurate. This is due to the fact that different websites are structured differently, making it difficult for the bookmarklet to consistently grab the correct information. Because the information is sometimes inaccurate, we suggest that you give your deal a once over before submitting it to make sure everything is correct. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-33"></a>
				            <p>
				                <span class="bold">Any tips and tricks for using the CouponPicks Bookmarklet?</span> - <a onclick="$('#answer33').toggle(); return false;" id="answer_link_33" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer33">
				                <div>
				                    For sure! One helpful hint is that you can choose to grab specific text to include in your deal by selecting it with you mouse before clicking "Add to CouponPicks." This will automatically post the selected text in either the Title or the Description section of your deal. The bookmarklet will automatically choose the section you're most likely selecting for. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-34"></a>
				            <p>
				                <span class="bold">I tried to submit a deal using the bookmarklet, and the submission page came up blank. What gives?</span> - <a onclick="$('#answer34').toggle(); return false;" id="answer_link_34" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer34">
				                <div>
				                    There could be a number of things going on here. One possibility is that you may have tried to submit a deal from a non-retail product page, which the bookmarklet is unable to read. Another thing to keep in mind, the bookmarklet has a timeout of 20 seconds, which means that if the server of the URL you are trying to submit is slow or down, the CouponPicks bookmarklet will only keep trying for 20 seconds before it defaults to a blank page. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-39"></a>
				            <p>
				                <span class="bold">How do I ask a question or post a tip?</span> - <a onclick="$('#answer39').toggle(); return false;" id="answer_link_39" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer39">
				                <div>
				                    Click the "+" button on the upper right hand corner of any page and then click to add a Topic/Picture. Now write out your question and tag it with the interests that it is most relevant to. Someone will respond quickly. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-40"></a>
				            <p>
				                <span class="bold">What kinds of questions can I ask?</span> - <a onclick="$('#answer40').toggle(); return false;" id="answer_link_40" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer40">
				                <div>
				                    Feel free to ask questions about CouponPicks, specific deals or coupons, products you’re interested in, stores or websites where you shop, hobbies DIY projects, in short, anything you are curious about. All we ask is that you try and ask the question in the interest where its most relevant so that you get the most relevant answer. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-50"></a>
				            <p>
				                <span class="bold">What kinds of tips can I share?</span> - <a onclick="$('#answer50').toggle(); return false;" id="answer_link_50" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer50">
				                <div>
				                    Our community is made up of savvy shoppers looking for the best bargains. We encourage you to share any and all tips and tricks that you use to shop smart and save money. </div>
				            </div>
				        </div>
				        <div class="faq-section">
				            <a name="faq-41"></a>
				            <p>
				                <span class="bold">How do I share an answer?</span> - <a onclick="$('#answer41').toggle(); return false;" id="answer_link_41" class="font11" href="javascript:void(0);">Show/hide</a>
				            </p>
				            <div style="line-height:18px;" id="answer41">
				                <div>
				                    Simple! Just reply in the comments and the questioner will receive a notification of your reply. </div>
				            </div>
				        </div>
				    </div>
				</div><!-- #infoContent -->
			</div>
		</div><!-- #container2 -->
		<div id="footericon" class="bigfooticon colorbackwhite handpoint">
			<i class="icon-eject"></i>
		</div><!-- #footericon -->
		<div id="footer" class="colorback" style="display: block;">
			<div class="container">
				<div class="inner footmenu">
					<div class="span3">
						<div class="btn-group hsds">
						   <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
						   </a>
						   <ul class="dropdown-menu bottom-up">
						      <li><a href="about.php">About Us</a></li>
						      <li><a href="#">Delivery Information</a></li>
						      <li><a href="privacy.php">Privacy Policy</a></li>
						      <li><a href="tos.php">Terms &amp; Conditions</a></li>
						   </ul>
						</div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Contact Us</a></li>
					         <li><a href="#">Returns</a></li>
					         <li><a href="#">Site Map</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Brands</a></li>
					         <li><a href="#">Gift Vouchers</a></li>
					         <li><a href="#">Affiliates</a></li>
					         <li><a href="#">Specials</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">My Account</a></li>
					         <li><a href="#">Order History</a></li>
					         <li><a href="#">Wish List</a></li>
					         <li><a href="#">Newsletter</a></li>
					      </ul>
					   </div>
					</div>
				</div><!-- #footmenu -->
			</div>
		</div><!-- #footer -->
<div id="powered" class="powered_pos"><p>Powered By <a href="/" title="Best Deals & Coupons">Couponpicks.com</a> &copy; 2015</p></div>
	</div><!-- #container -->
	<a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
		<span id="toTopHover"></span>
		<i class="icon-chevron-up icon-large"></i>
	</a>
</body>
</html>
