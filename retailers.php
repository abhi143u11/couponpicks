<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>About | Couponpicks</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {
			$(".various").fancybox({
				maxWidth	: 630,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		});
	</script>
</head>
<body>
	<div class="navbar navbar-fixed-top topbar">
	   <div class="navbar-inner kasdf">
	      <div class="container container2 wrap-menu">
	         <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
	         <div class="pull-left firstsearch" id="search">
               	<div class="input-prepend">
	               	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
	               	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
               	</div>
            </div>
	         <div id="headerunder" class="pull-right colorback"></div>
	         <div id="header" class="pull-right topcart colorback">
	            <ul id="userNav">
				   <li>
				      <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
				      </a>
				      <div class="userSubMenu menu">
				         <div class="menuWrapper">
				            <ul>
				               <li><a href="#">My Profile</a></li>
				               <li><a href="#">My Deals</a></li>
				               <li><a href="#">My Coupons</a></li>
				               <li><a href="#">Notifications</a></li>
				               <li><a href="#">Messages</a></li>
				               <li><a href="#">Saved Stores</a></li>
				               <li><a href="#">Find Friends</a></li>
				               <li><a class="subUserMenuLink" href="#">Settings</a></li>
				               <li><a href="#">Sign Out</a></li>
				            </ul>
				         </div>
				      </div>
				   </li>
				</ul>
				<a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
					<img class="icon-plus-button" alt="add" src="images/blank.png">
				</a>
	         </div>
	         <div class="topmenu">
	            <div class="dropdown">
	            	<div class="click-toggle">
	            		<a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
		               <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
		               	<?php
							if ($cats->num_rows > 0) {
								while($row = $cats->fetch_assoc()) { ?>
			                  <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
			                  </li>
		                <?php
							}
						}
						?>	
		               </ul>
	            	</div>
	               <a class="padright" id="wishlist-total" href="#">My Feed</a>
	               <a href="#">Popular</a>
	               <div class="btn-group little-select">
	                  	<a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
	             			<i class="icon-reorder"></i>
	                  	</a>     	
	                  	<!-- <ul class="dropdown-menu">
	                     	<form enctype="multipart/form-data" method="post" action="#">
	                        	<div id="currency">Currency<br>
		                           <a title="Euro">€</a>
		                           <a title="Pound Sterling">£</a>
		                           <a title="US Dollar"><b>$</b></a>
	                        	</div>
	                     	</form>
	                  	</ul> -->
	                  	<div id="option-list">
	                  		<div class="menuWrapper">
	                  			<ul class="menuTopIcons ">
								   <li><a href="#">
								         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">My Feed</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Popular</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Fresh</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Heating Up</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Coupon Codes</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Printable Coupons</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Interests</div>
								      </a></li>
								</ul><!-- #menuTopIcons -->
								<ul>
								   <li><a href="#">Apps</a></li>
								   <li><a href="#">Automotive</a></li>
								   <li><a href="#">Bed &amp; Bath</a></li>
								   <li><a href="#">Computers &amp; Software</a></li>
								   <li><a href="#">Electronics</a></li>
								   <li><a href="#">Entertainment</a></li>
								   <li><a href="#">Freebies</a></li>
								   <li><a href="#">Furniture &amp; Decor</a></li>
								   <li><a href="#">Games</a></li>
								</ul>
								<ul>
								   <li><a href="#">Gifts &amp; Flowers</a></li>
								   <li><a href="#">Grocery &amp; Food</a></li>
								   <li><a href="#">Health &amp; Beauty</a></li>
								   <li><a href="#">Home &amp; Garden</a></li>
								   <li><a href="#">Kids &amp; Baby</a></li>
								   <li><a href="#">Kitchen &amp; Dining</a></li>
								   <li><a href="#">Laptop</a></li>
								   <li><a href="#">Men</a></li>
								   <li><a href="#">News</a></li>
								</ul>
								<ul>
								   <li><a href="#">Office &amp; School</a></li>
								   <li><a href="#">Other</a></li>
								   <li><a href="#">Pets</a></li>
								   <li><a href="#">Sports &amp; Outdoor</a></li>
								   <li><a href="#">Tax &amp; Finance</a></li>
								   <li><a href="#">Toys</a></li>
								   <li><a href="#">Travel &amp; Tickets</a></li>
								   <li><a href="#">TV</a></li>
								   <li><a href="#">Women</a></li>
								   <li><a href="#">DealsPlus Exclusive</a></li>
								</ul>
	                  		</div><!-- #menuWrapper -->
	                  		<div class="aboutUsLinks">
							   <div class="aboutUsSection">
							      <a href="about.php">About Us</a>
							      <span>|</span>
							      <a target="_blank" href="blog.php">Blog</a>
							      <span>|</span>
							      <a href="#">Contact</a>
							      <span>|</span>
							      <a href="privacy.php">Privacy Policy</a>
							      <span>|</span>
							      <a href="tos.php">Terms of Use</a>
							      <div class="categoryMenuSocial">
							         <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
							         <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
							         <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
							         <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
							      </div>
							   </div>
							</div><!-- #aboutUsLinks -->
	                  	</div><!-- #option-list -->
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div><!-- #navbar-fixed-top -->

	<div class="navbar navbar-static-top menubar responsive-menu">
		<div class="navbar-inner">  
		    <div class="dropdown drsp">
		        <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
		        </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                    	print_r($cats);
						if ($cats->num_rows > 0) {
							while($row = $cats->fetch_assoc()) { ?>
		                  	<li>
		                  		<a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
		                  	</li>
		            <?php
							}
						}
					?> 
                </ul>
		        <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
		        <a href="#">My Account</a>
		        <div class="btn-group little-select">
		            <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
		            <ul class="dropdown-menu">
		                <form enctype="multipart/form-data" method="post" action="index.php">
						  	<div id="currency">Currency<br>
				                <a title="Euro">€</a>
				                <a title="Pound Sterling">£</a>
				                <a title="US Dollar"><b>$</b></a>
						  	</div>
						</form>
		            </ul>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-menu -->

	<div class="navbar navbar-static-top menubar responsive-search">
		<div class="navbar-inner">   
		    <div class="container container2">
		        <div align="center" id="header">
		            <div class="pull-left fullwidth nopad5" id="search">
		                <div class="input-prepend">
		                	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
		                	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-search -->

	<div class="dialogHolder" id="addToDPDialog">
	   <div class="dialogContent">
	      <div id="addToDPDialogContent">
	         <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
	         <div id="addToDPButtons">
	            <div class="dialogBody">
	               <div class="addDialog">
	                  <h4>What are you adding?</h4>
	                  <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
	               </div>
	               <div class="addItems">
	                  <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
	                     <h6 class="itemName">Deal / Product</h6>
	                     <div class="itemImage">
	                        <img src="images/dealIcon.png">
	                     </div>
	                     <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
	                  </a>
	                  <a href="#" class="itemContainer" id="addCouponLink">
	                     <h6 class="itemName">Coupon</h6>
	                     <div class="itemImage">
	                        <img src="images/couponIcon.png">
	                     </div>
	                     <div class="itemDescription">A coupon to be used online or in store</div>
	                  </a>
	                  <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
	                     <h6 class="itemName">Topic / Photo</h6>
	                     <div class="itemImage">
	                        <img src="images/topicIcon.png">
	                     </div>
	                     <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
	                  </a>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="afterheader"></div>
	<div id="container">
		<div class="container container2">
			<div style="margin:10px auto;">
				<div class="infoMenu box expandMenuBar">
				   <div class="menuSection">
				      <h3>CouponPicks</h3>
				      <ul>
				         <li><a href="about.php">About Us</a></li>
				         <li><a href="jobs.php">Jobs</a></li>
				         <li><a target="_blank" href="#">Blog</a></li>
				         <li><a href="cp_tools.php">Tools</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Help</h3>
				      <ul>
				         <li><a href="answer.php">Ask &amp; Share</a></li>
				         <li><a href="#">Contact</a></li>
				         <li><a href="faqs.php">FAQs</a></li>
				         <li><a href="privacy.php">Privacy Policy</a></li>
				         <li><a href="tos.php">Terms of Use</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Business With Us</h3>
				      <ul>
				         <li>
				            <a href="retailers.php">Retailers</a>
				         </li>
				         <li><a href="advertisers.php">Advertisers</a></li>
				      </ul>
				   </div>
				</div><!-- #infoMenu -->
				<div class="infoContent box">
				    <div class="tabMenu">
					    <ul class="subMenu visible">
					        <li class="active"><a href="#tab1"> Why Join Us?</a>
					        </li>
					        <li><a href="#tab2">Opportunities</a>
					        </li>
					        <li><a href="#tab3">Create a Store Page</a>
					        </li>
					    </ul>

					    <ul class="subMenu ">
					        <li><a href="#tab1">Why Join Us?</a>
					        </li>
					        <li><a href="#tab2">Opportunities</a>
					        </li>
					        <li><a href="#tab3">Contact Us</a>
					        </li>
					    </ul>
					</div>
					<div class="retailerAd" >
			            <h1>Retailers and Advertisers</h1>
			        </div>
			        <div class="retailerContent">
					    <div class="conRetailContent">
					        <div class="retailers">RETAILERS</div>
					        <div class="tabJoinUs tabRetailer" id="tab1">
					            <div class="title-topRetailer">
					                <h5>Why add your store to CouponPicks?</h5>
					            </div>
					            <div class="middleRetailer">
					                <p><span class="bullet">•</span>Tap into our community of shoppers looking to make purchases in the near future</p>
					                <p><span class="bullet">•</span>Promote your deals and coupons, while allowing the community to share them as well</p>
					                <p><span class="bullet">•</span>Enjoy visibility across our site, as well as in our wide-reaching emails</p>
					                <p><span class="bullet">•</span>Grow your community fan base, helping to maximize sales and conversions</p>
					            </div>
					            <div class="whoshop" >
					                <h5>Who's shopping here?</h5>
					            </div>
					            <div class="bottomRetail">
					                <p><span class="bullet">•</span>Over 6 million people visit our site each month</p>
					                <p><span class="bullet">•</span>70% of our users are female and 30% are male</p>
					                <p><span class="bullet">•</span>The median age of our users is 30</p>
					                <p><span class="bullet">•</span>The average annual income of our users is $80,000</p>
					                <p><span class="bullet">•</span>Over 90% of our users tell their friends about deals they find on CouponPicks.</p>
					            </div>
					        </div><!-- #tabJoinUs -->
					        <div class="tabOpportuniti tabRetailer" id="tab2">
							    <h5>Create a Store Page</h5>
							    <img alt="Store Page" src="images/storePage.jpg">
							    <br>
							    <br>
							    <h5>Grow Your Deals Page</h5>
							    <img alt="Deal Page" src="images/dealsPage.jpg">
							    <br>
							    <br>
							    <h5>Get Included in Our Emails</h5>
							    <img alt="Email" src="images/adverEmail.jpg">
							    <p>Interested in advertising on CouponPicks? Click "Advertisers" in the menu on your left for more information.</p>
							</div><!-- #tabOpportuniti -->
							<div class="tabStorePage tabRetailer" id="tab3">
								<div class="top-storePage">
								    <h5>Create a Store</h5>
								    <p>Complete the form below and we'll contact you when your Store Page has been approved.</p>
								    <p>Fields marked with red (<span>*</span>) are required.</p>
								</div>
								<div id="form">
								    <form method="post" >
								        <div class="createStoreTable">
								            <div class="inputDiv">
								                <label for="storeName"><span>*</span>Store Name:</label>
								                <input type="text" id="storeName" name="storeName">
								            </div>

								            <div class="inputDiv">
								                <label for="storeURL"><span>*</span>Store URL:</label>
								                <input type="text" id="storeURL" name="storeURL">
								            </div>

								            <div class="inputDiv">
								                <label for="contactName"><span>*</span>Contact Name:</label>
								                <input type="text" id="contactName" name="contactName">
								            </div>

								            <div class="inputDiv">
								                <label for="contactEmail"><span>*</span>Contact Email:</label>
								                <input type="email" value="" id="contactEmail" name="contactEmail">
								            </div>

								            <div class="inputDiv">
								                <label for="contactPhone">Contact Phone:</label>
								                <input type="text" id="contactPhone" name="contactPhone">
								            </div>

								            <div class="inputDiv">
								                <label class="textareaLabel" for="message">Are you part of an affiliate network? If so, please provide details:</label>
								                <textarea id="message" name="message"></textarea>
								            </div>

								            <input type="submit" class="adverSubmit purpleButton" value="Submit">
								        </div>
								    </form>
								</div>
								<div class="q-business">
				                    <h5>Are you a local business?</h5>
				                </div>
				                <div class="sitTight">
				                    <p>Sit tight!  We're currently working on creating opportunities for local businesses that may not have an online storefront.  Please check back in soon.</p>
				                </div>
							</div><!-- #tabStorePage -->
					        <div style="background-color: #f0f0f0; height: 8px; margin-bottom: 15px;"></div>
					    </div>
					    <div style="clear:both;"></div>
					</div>
				</div><!-- #infoContent -->
			</div>
		</div><!-- #container2 -->
		<div id="footericon" class="bigfooticon colorbackwhite handpoint">
			<i class="icon-eject"></i>
		</div><!-- #footericon -->
		<div id="footer" class="colorback" style="display: block;">
			<div class="container">
				<div class="inner footmenu">
					<div class="span3">
						<div class="btn-group hsds">
						   <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
						   </a>
						   <ul class="dropdown-menu bottom-up">
						      <li><a href="about.php">About Us</a></li>
						      <li><a href="#">Delivery Information</a></li>
						      <li><a href="privacy.php">Privacy Policy</a></li>
						      <li><a href="tos.php">Terms &amp; Conditions</a></li>
						   </ul>
						</div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Contact Us</a></li>
					         <li><a href="#">Returns</a></li>
					         <li><a href="#">Site Map</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Brands</a></li>
					         <li><a href="#">Gift Vouchers</a></li>
					         <li><a href="#">Affiliates</a></li>
					         <li><a href="#">Specials</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">My Account</a></li>
					         <li><a href="#">Order History</a></li>
					         <li><a href="#">Wish List</a></li>
					         <li><a href="#">Newsletter</a></li>
					      </ul>
					   </div>
					</div>
				</div><!-- #footmenu -->
			</div>
		</div><!-- #footer -->
<div id="powered" class="powered_pos"><p>Powered By <a href="/" title="Best Deals & Coupons">Couponpicks.com</a> &copy; 2015</p></div>
	</div><!-- #container -->
	<a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
		<span id="toTopHover"></span>
		<i class="icon-chevron-up icon-large"></i>
	</a>
</body>
</html>
