<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>About | Couponpicks</title>
    <link rel="stylesheet" type="text/css" href="style.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="js/javascript.js"></script>
    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
    <link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
    <script type="text/javascript">
        $(document).ready(function() {
            $(".various").fancybox({
                maxWidth    : 630,
                fitToView   : false,
                width       : '70%',
                height      : '70%',
                autoSize    : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
            });
        });
    </script>
    <script type="application/javascript">
         function installFirefox (aEvent)
         {
             for (var a = aEvent.target; a.href === undefined;) a = a.parentNode;
             var params = {
                 "Foo": { URL: aEvent.target.href,
                     IconURL: aEvent.target.getAttribute("iconURL"),
                     Hash: aEvent.target.getAttribute("hash"),
                     toString: function () { return this.URL; }
                 }
             };
             InstallTrigger.install(params);
         
             return false;
        }
    </script>
    <script type="text/javascript">
      $('.showBookmarklet').click(function() {
          if ($('.bookmarklet').is(':visible')) {
              $('.bookmarklet').slideUp();
          } else {
              $('.bookmarklet').slideDown();
          }
      });
      
      $('.bookmarkletAddLink').hover(function() {
          $(this).find('a').text('Drag Me to Bookmark Bar');
          $('.dragInstruction').toggle();
      }, function() {
          $(this).find('a').text('Add To CouponPicks');
      });
    </script>
</head>
<body>
    <div class="navbar navbar-fixed-top topbar">
     <div class="navbar-inner kasdf">
        <div class="container container2 wrap-menu">
           <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
           <div class="pull-left firstsearch" id="search">
                <div class="input-prepend">
                  <input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
                  <span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
                </div>
            </div>
           <div id="headerunder" class="pull-right colorback"></div>
           <div id="header" class="pull-right topcart colorback">
              <ul id="userNav">
           <li>
              <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
              </a>
              <div class="userSubMenu menu">
                 <div class="menuWrapper">
                    <ul>
                       <li><a href="#">My Profile</a></li>
                       <li><a href="#">My Deals</a></li>
                       <li><a href="#">My Coupons</a></li>
                       <li><a href="#">Notifications</a></li>
                       <li><a href="#">Messages</a></li>
                       <li><a href="#">Saved Stores</a></li>
                       <li><a href="#">Find Friends</a></li>
                       <li><a class="subUserMenuLink" href="#">Settings</a></li>
                       <li><a href="#">Sign Out</a></li>
                    </ul>
                 </div>
              </div>
           </li>
        </ul>
        <a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
          <img class="icon-plus-button" alt="add" src="images/blank.png">
        </a>
           </div>
           <div class="topmenu">
              <div class="dropdown">
                <div class="click-toggle">
                  <a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
                   <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
                    <?php
              if ($cats->num_rows > 0) {
                while($row = $cats->fetch_assoc()) { ?>
                        <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
                        </li>
                    <?php
              }
            }
            ?>  
                   </ul>
                </div>
                 <a class="padright" id="wishlist-total" href="#">My Feed</a>
                 <a href="#">Popular</a>
                 <div class="btn-group little-select">
                      <a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
                    <i class="icon-reorder"></i>
                      </a>      
                      <!-- <ul class="dropdown-menu">
                        <form enctype="multipart/form-data" method="post" action="#">
                            <div id="currency">Currency<br>
                               <a title="Euro">€</a>
                               <a title="Pound Sterling">£</a>
                               <a title="US Dollar"><b>$</b></a>
                            </div>
                        </form>
                      </ul> -->
                      <div id="option-list">
                        <div class="menuWrapper">
                          <ul class="menuTopIcons ">
                   <li><a href="#">
                         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
                         <div class="menuTopIconTitle">My Feed</div>
                    </a></li>
                   <li><a href="#">
                         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
                         <div class="menuTopIconTitle">Popular</div>
                    </a></li>
                   <li><a href="#">
                         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
                         <div class="menuTopIconTitle">Fresh</div>
                    </a></li>
                   <li><a href="#">
                         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
                         <div class="menuTopIconTitle">Heating Up</div>
                    </a></li>
                   <li><a href="#">
                         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
                         <div class="menuTopIconTitle">Coupon Codes</div>
                    </a></li>
                   <li><a href="#">
                        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
                        <div class="menuTopIconTitle">Printable Coupons</div>
                    </a></li>
                   <li><a href="#">
                        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
                        <div class="menuTopIconTitle">Interests</div>
                      </a></li>
                </ul><!-- #menuTopIcons -->
                <ul>
                   <li><a href="#">Apps</a></li>
                   <li><a href="#">Automotive</a></li>
                   <li><a href="#">Bed &amp; Bath</a></li>
                   <li><a href="#">Computers &amp; Software</a></li>
                   <li><a href="#">Electronics</a></li>
                   <li><a href="#">Entertainment</a></li>
                   <li><a href="#">Freebies</a></li>
                   <li><a href="#">Furniture &amp; Decor</a></li>
                   <li><a href="#">Games</a></li>
                </ul>
                <ul>
                   <li><a href="#">Gifts &amp; Flowers</a></li>
                   <li><a href="#">Grocery &amp; Food</a></li>
                   <li><a href="#">Health &amp; Beauty</a></li>
                   <li><a href="#">Home &amp; Garden</a></li>
                   <li><a href="#">Kids &amp; Baby</a></li>
                   <li><a href="#">Kitchen &amp; Dining</a></li>
                   <li><a href="#">Laptop</a></li>
                   <li><a href="#">Men</a></li>
                   <li><a href="#">News</a></li>
                </ul>
                <ul>
                   <li><a href="#">Office &amp; School</a></li>
                   <li><a href="#">Other</a></li>
                   <li><a href="#">Pets</a></li>
                   <li><a href="#">Sports &amp; Outdoor</a></li>
                   <li><a href="#">Tax &amp; Finance</a></li>
                   <li><a href="#">Toys</a></li>
                   <li><a href="#">Travel &amp; Tickets</a></li>
                   <li><a href="#">TV</a></li>
                   <li><a href="#">Women</a></li>
                   <li><a href="#">DealsPlus Exclusive</a></li>
                </ul>
                        </div><!-- #menuWrapper -->
                        <div class="aboutUsLinks">
                 <div class="aboutUsSection">
                    <a href="about.php">About Us</a>
                    <span>|</span>
                    <a target="_blank" href="blog.php">Blog</a>
                    <span>|</span>
                    <a href="#">Contact</a>
                    <span>|</span>
                    <a href="privacy.php">Privacy Policy</a>
                    <span>|</span>
                    <a href="tos.php">Terms of Use</a>
                    <div class="categoryMenuSocial">
                       <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
                       <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
                       <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
                       <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
                    </div>
                 </div>
              </div><!-- #aboutUsLinks -->
                      </div><!-- #option-list -->
                 </div>
              </div>
           </div>
        </div>
     </div>
  </div><!-- #navbar-fixed-top -->

    <div class="navbar navbar-static-top menubar responsive-menu">
        <div class="navbar-inner">  
            <div class="dropdown drsp">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
                </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                        print_r($cats);
                        if ($cats->num_rows > 0) {
                            while($row = $cats->fetch_assoc()) { ?>
                            <li>
                                <a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
                            </li>
                    <?php
                            }
                        }
                    ?> 
                </ul>
                <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
                <a href="#">My Account</a>
                <div class="btn-group little-select">
                    <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
                    <ul class="dropdown-menu">
                        <form enctype="multipart/form-data" method="post" action="index.php">
                            <div id="currency">Currency<br>
                                <a title="Euro">€</a>
                                <a title="Pound Sterling">£</a>
                                <a title="US Dollar"><b>$</b></a>
                            </div>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- #responsive-menu -->

    <div class="navbar navbar-static-top menubar responsive-search">
        <div class="navbar-inner">   
            <div class="container container2">
                <div align="center" id="header">
                    <div class="pull-left fullwidth nopad5" id="search">
                        <div class="input-prepend">
                            <span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
                            <input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- #responsive-search -->

    <div class="dialogHolder" id="addToDPDialog">
       <div class="dialogContent">
          <div id="addToDPDialogContent">
             <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
             <div id="addToDPButtons">
                <div class="dialogBody">
                   <div class="addDialog">
                      <h4>What are you adding?</h4>
                      <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
                   </div>
                   <div class="addItems">
                      <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
                         <h6 class="itemName">Deal / Product</h6>
                         <div class="itemImage">
                            <img src="images/dealIcon.png">
                         </div>
                         <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
                      </a>
                      <a href="#" class="itemContainer" id="addCouponLink">
                         <h6 class="itemName">Coupon</h6>
                         <div class="itemImage">
                            <img src="images/couponIcon.png">
                         </div>
                         <div class="itemDescription">A coupon to be used online or in store</div>
                      </a>
                      <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
                         <h6 class="itemName">Topic / Photo</h6>
                         <div class="itemImage">
                            <img src="images/topicIcon.png">
                         </div>
                         <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
                      </a>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>

    <div class="afterheader"></div>
    <div id="container">
        <div class="container container2">
            <div style="margin:10px auto;">
                <div class="infoMenu box expandMenuBar">
                   <div class="menuSection">
                      <h3>CouponPicks</h3>
                      <ul>
                         <li><a href="about.php">About Us</a></li>
                         <li><a href="jobs.php">Jobs</a></li>
                         <li><a target="_blank" href="#">Blog</a></li>
                         <li><a href="cp_tools.php">Tools</a></li>
                      </ul>
                   </div>
                   <div class="menuSection">
                      <h3>Help</h3>
                      <ul>
                         <li><a href="#">Ask &amp; Share</a></li>
                         <li><a href="#">Contact</a></li>
                         <li><a href="faqs.php">FAQs</a></li>
                         <li><a href="privacy.php">Privacy Policy</a></li>
                         <li><a href="tos.php">Terms of Use</a></li>
                      </ul>
                   </div>
                   <div class="menuSection">
                      <h3>Business With Us</h3>
                      <ul>
                         <li>
                            <a href="retailers.php">Retailers</a>
                         </li>
                         <li><a href="#">Advertisers</a></li>
                      </ul>
                   </div>
                </div><!-- #infoMenu -->
                <div class="infoContent box">
                  <h2>Terms of Use</h2>
                  <p>To better serve you and to clarify our service, we have drawn up an agreement of our terms and conditions. In order to use our service, you must read and accept all of the terms and conditions of this agreement and in the Privacy Policy. If you do not agree to be bound by the terms after you read this Agreement, you may not use our service.</p>
                  <br>
                  <h3>1. The Service and Terms</h3>
                  <p> Fusion Web, LLC (referred to hereafter as DealsPlus) provides a collection of online resources, including classified ads, affiliate ads, a blog, a forum and a business directory (referred to hereafter as "the Service"). The Service is provided by DealsPlus, and is subject to the following Terms of Use ("Terms"), which may be updated by DealsPlus from time to time. DealsPlus will provide notice of materially significant changes to the Terms by posting notice on the DealsPlus homepage. You must be at least 14 years of age to use this Service. By using the Service in any way, you are agreeing to comply with these terms.</p>
                  <h3>2. Content</h3>
                  <p>You understand that all postings, messages, text, files, images, photos, video, sounds, or other materials ("Content") posted on, transmitted through, or linked from the Service, are the sole responsibility of the person from whom such Content originated. You are entirely responsible for all Content that you post or email on the Service. DealsPlus does not control, and is not responsible for Content made available through the Service. By using the Service, you may be exposed to Content that is offensive, indecent, inaccurate, misleading, or otherwise objectionable. Furthermore, the DealsPlus site and Content available through the Service may contain links to other websites. DealsPlus makes no representation or warranty as to the accuracy, completeness or authenticity of the information contained in any such site. Your linking to any other websites is at your own risk and we recommend that you do so carefully. DealsPlus will not be liable in any way for any Content or for any loss or damage of any kind incurred as a result of the use of any Content posted, emailed or otherwise made available via the Service. DealsPlus does not pre-screen or approve Content, but it has the right (but not the obligation) in its sole discretion to refuse, delete or move any Content that is available via the Service, for violating the letter or spirit of the Terms or for any other reason.</p>
                  <p>By continuing to use DealsPlus you hereby grant to DealsPlus a non-revocable, non-exclusive, transferable, royalty-free and sub-licensable worldwide license to all text, photographs, film, audio, or other recordings, still or moving that you submit (the “Media”) for DealsPlus’ unqualified use or modification in any medium including digital, electronic, print, television, film, radio and other medium now known or to be invented for any purpose (except pornographic or defamatory) which may include, among others, advertising, promotion and marketing. You agree that the Media may be combined with other images, text, graphics, film, audio, audio-visual works; and may be cropped, altered or modified. You acknowledge and agree that you have no further right to additional consideration or accounting, and that you will make no further claim for any reason to DealsPlus. You acknowledge and agree that this release is binding upon your heirs and assigns.</p>
                  <h3>3. Copyrights and Trademarks</h3>
                  <p>Content displayed on or through the Service is protected by copyright as a collective work and/or compilation, pursuant to copyrights laws, and international conventions. DealsPlus, the Plus Side, the DealsPlus logo, the Plus Side logo, and icons and marks identifying DealsPlus or Plus Side products and services are trademarks of DealsPlus and may not be used without the written consent of the DealsPlus. You may not copy, reproduce, distribute, or create derivative works of the Service without DealsPlus' express written authorization. You may not reverse engineer, decompile, alter, modify, disassemble, or otherwise attempt to derive source code from the Service. The DealsPlus respects the intellectual property of others and we require that our users do the same. You may not post, distribute, or reproduce in any way any copyrighted material, trademarks, or other proprietary information that you do not have legal authorization to use.</p>
                  <h3>4. Notification of Claims of Infringement</h3>
                  <p>If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please notify the DealsPlus' agent for notice of claims of copyright or other intellectual property infringement ("Agent") by clicking <a href="#">HERE</a> and selecting "questionable content" from the drop down topic list . Please provide our Agent with the following Notice:</p>
                  <ul>
                      <li> Identify the copyrighted work or other intellectual property that you claim has been infringed;
                      </li>
                      <li> Identify the material on the DealsPlus site that you claim is infringing, with enough detail so that we may locate it on the website;
                      </li>
                      <li> A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;
                      </li>
                      <li> A statement by you declaring under penalty of perjury that (a) the above information in your Notice is accurate, and (b) that you are the owner of the copyright interest involved or that you are authorized to act on behalf of that owner;
                      </li>
                      <li> Your address, telephone number, and email address; and
                      </li>
                      <li> Your physical or electronic signature. </li>
                  </ul>
                  <br>
                  <p>The DealsPlus will remove the infringing posting(s), subject to the the procedures outlined in the Digital Millennium Copyright Act (DMCA).</p>
                  <h3>5. Privacy and Information Disclosure</h3>
                  <p>Please review our Privacy Policy. Your use of the DealsPlus website or the Service signifies acknowledgement of and agreement to our Privacy Policy.</p>
                  <h3>6 . Conduct</h3>
                  <p>You agree not to post, email, or otherwise make available Content:</p>
                  <p>
                  </p>
                  <ul>
                      <li> that is unlawful, harmful, threatening, abusive, harassing, defamatory, pornographic, libelous, invasive of another's privacy, or harms minors in any way;
                      </li>
                      <li> that harasses, degrades, intimidates or is hateful toward an individual or group of individuals on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability;
                      </li>
                      <li> that impersonates any person or entity, including, but not limited to, a DealsPlus employee, or falsely states or otherwise misrepresents your affiliation with a person or entity;
                      </li>
                      <li> that includes personal or identifying information about another person without that person's explicit consent.
                      </li>
                      <li> that is false, deceptive, misleading, deceitful, or misinformative.
                      </li>
                      <li> that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party, or Content that you do not have a right to make available under any law or under contractual or fiduciary relationships.
                      </li>
                      <li> that constitutes or contains "affiliate marketing," "link referral code," "junk mail," "spam," "chain letters," "pyramid schemes," or unsolicited commercial advertisement.
                      </li>
                      <li> that constitutes or contains any form of advertising or solicitation if (1) posted in areas of the DealsPlus sites which are not designated for such purposes; or (2) emailed to DealsPlus users who have requested not to be contacted about other services, products or commercial interests.
                      </li>
                      <li> that includes links to commercial services or web sites, except as allowed in "services".
                      </li>
                      <li> that advertises any illegal services or the sale of any items the sale of which is prohibited or restricted by applicable law, including without limitation items the sale of which is prohibited or regulated by law.
                      </li>
                      <li> that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;
                      </li>
                      <li> that may be deemed to be engaging in solicitation activities in California that refer potential customers to out-of-state DealsPlus retailers, including, but not limited to, distributing flyers, coupons, newsletters, and other printed promotional materials or electronic equivalents, verbal soliciting (for example, in-person referrals), initiating telephone calls, and sending emails.
                      </li>
                      <li> that disrupts the normal flow of dialogue with an excessive number of messages to the Service, or that otherwise negatively affects other users' ability to use the Service; or
                      </li>
                      <li> that employs misleading email addresses, or forged headers or otherwise manipulated identifiers in order to disguise the origin of Content transmitted through the Service. </li>
                  </ul>
                  <p> Furthermore, you acknowledge that DealsPlus has financial relationships with some of the merchants on the Site and that DealsPlus may be compensated if you choose to utilize the links on this site and/or generate sales for such merchants.</p>
                  <h3>7 . No Spam Policy</h3>
                  <p>Any unauthorized use of DealsPlus computer systems, including but not limited to the sending of unsolicited email advertisements to DealsPlus email addresses or through DealsPlus computer systems, is a violation of these Terms and certain federal and state laws, including but not limited to the Computer Fraud and Abuse Act (18 U.S.C. § 1030 et seq.). Such violations may subject the sender and his or her agents to civil and criminal penalties.</p>
                  <h3>8 . Limitations on Service</h3>
                  <p>You acknowledge that the DealsPlus may establish limits concerning use of the Service, including the maximum number of days that Content will be retained by the Service, the maximum number and size of postings, email messages, or other Content that may be transmitted or stored by the Service, and the frequency with which you may access the Service. You agree that the DealsPlus has no responsibility or liability for the deletion or failure to store any Content maintained or transmitted by the Service. You acknowledge that the DealsPlus reserves the right at any time to modify or discontinue the Service (or any part thereof) with or without notice, and that DealsPlus.com shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Service.</p>
                  <h3>9 . Termination of Service</h3>
                  <p>You agree that the DealsPlus in its sole discretion, has the right (but not the obligation) to delete or deactivate your account, block your email or IP address, or otherwise terminate your access to or use of the Service (or any part thereof), immediately and without notice, and remove and discard any Content within the Service, for any reason, including, without limitation, if the DealsPlus believes that you have violated these Terms. Further, you agree that the DealsPlus shall not be liable to you or any third-party for any termination of your access to the Service. Further, you agree not to attempt to use the Service after said termination.</p>
                  <h3>10 . Dealings with Organizations and Individuals</h3>
                  <p>Your interactions with organizations and/or individuals found on or through the Service, including payment and delivery of goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such organizations and/or individuals. You agree that the DealsPlus shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings. If there is a dispute between participants on this site, or between users and any third party, you understand and agree that the DealsPlus is under no obligation to become involved. In the event that you have a dispute with one or more other users, you hereby release the DealsPlus, its officers, employees, agents and successors in rights from claims, demands and damages (actual and consequential) of every kind or nature, known or unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way related to such disputes and / or our service.</p>
                  <h3>11 . Disclaimer of Warranties</h3>
                  <p>YOU AGREE THAT USE OF THE DEALSPLUS SITE AND THE SERVICE IS ENTIRELY AT YOUR OWN RISK, WITHOUT WARRANTIES OF ANY KIND. ALL EXPRESS AND IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT OF PROPRIETARY RIGHTS ARE EXPRESSLY DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. TO THE FULLEST EXTENT PERMITTED BY LAW, DEALSPLUS DIS CLAIMS ANY WARRANTIES FOR THE SECURITY, RELIABILITY, TIMELINESS, ACCURACY, AND PERFORMANCE OF THE DEALSPLUS SITE AND THE SERVICE. TO THE FULLEST EXTENT PERMITTED BY LAW, DEALSPLUS DISCLAIMS ANY WARRANTIES FOR OTHER SERVICES OR GOODS RECEIVED THROUGH OR ADVERTISED ON THE DEALSPLUS SITE OR THE SITES OR SERVICE, OR ACCESSED THROUGH ANY LINKS ON THE DEALSPLUS SITE. TO THE FULLEST EXTENT PERMITTED BY LAW, THE DEALSPLUS DISCLAIMS ANY WARRANTIES FOR VIRUSES OR OTHER HARMFUL COMPONENTS IN CONNECTION WITH THE DEALSPLUS SITE OR THE SERVICE. Some jurisdictions do not allow the disclaimer of implied warranties. In such jurisdictions, some of the foregoing disclaimers may not apply to you insofar as they relate to implied warranties.</p>
                  <h3>12 . Limitations of Liability</h3>
                  <p>IN NO EVENT SHALL THE DEALSPLUS BE LIABLE FOR DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES , RESULTING FROM ANY ASPECT OF YOUR USE OF THE DEALSPLUS SITE OR THE SERVICE, WHETH ER THE DAMAGES ARISE FROM USE OR MISUSE OF THE DEALSPLUS SITE OR THE SERVICE, FROM INABILITY TO USE THE DEALSPLUS SITE OR THE SERVICE, OR THE INTERRUPTION, SUSPENSION, MODIFICATION, ALTERATION, OR TERMINATION OF THE DEALSPLUS SITE OR THE SERVICE. SUCH LIMITATION SHALL ALSO APPLY WITH RESPECT TO DAMAGES INCURRED BY REASON OF OTHER SERVICES OR PRODUCTS RECEIVED THROUGH OR ADVERTISED IN CONNECTION WITH THE DEALSPLUS SITE OR THE SERVICE OR ANY LINKS ON THE DEALSPLUS SITE, AS WELL AS BY REASON OF ANY INFORMATION OR ADVICE RECEIVED THROUGH OR ADVERTISED IN CONNECTION WITH THE DEALSPLUS SITE OR THE SERVICE OR ANY LINKS ON THE DEALSPLUS SITE. THESE LIMITATIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW. In some jurisdiction, limitations of liability are not permitted. In such jurisdictions, some of the foregoing limitation may not apply to you.</p>
                  <h3>13 . Indemnity</h3>
                  <p>You agree to indemnify and hold the DealsPlus site , its officers, subsidiaries, affiliates, successors, assigns, directors, officers, agents, service providers, suppliers and employees, harmless from any claim or demand, including reasonable attorney fees and court costs, made by any third party due to or arising out of Content you submit, post or make available through the Service, your use of the Service, your violation of the Terms, your breach of any of the representations and warranties herein, or your violation of any rights of another.</p>
                  <h3>14 . General Information</h3>
                  <p>The Terms and the relationship between you and the DealsPlus shall be governed by the laws of the State of Delaware without regard to its conflict of law provisions. You and the DealsPlus agree to submit to the personal and exclusive jurisdiction of the courts located within the county of New Castle, Delaware. The failure of the DealsPlus to exercise or enforce any right or provision of the Terms shall not constitute a waiver of such right or provision. If any provision of the Terms is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions of the Terms remain in full force and effect.</p>
                  <h3>15 . Violations of Terms</h3>
                  <h3>
              <p>Please report any known violations of the Terms, by clicking <a href="#">HERE</a>.</p>
              <p>*<a href="#">Contest rules</a></p>
              Terms and Conditions Last Updated on July 16, 2013</h3>
              </div><!-- #infoContent -->
            </div>
        </div><!-- #container2 -->
        <div id="footericon" class="bigfooticon colorbackwhite handpoint">
            <i class="icon-eject"></i>
        </div><!-- #footericon -->
        <div id="footer" class="colorback" style="display: block;">
          <div class="container">
            <div class="inner footmenu">
              <div class="span3">
                <div class="btn-group hsds">
                   <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
                   </a>
                   <ul class="dropdown-menu bottom-up">
                      <li><a href="about.php">About Us</a></li>
                      <li><a href="#">Delivery Information</a></li>
                      <li><a href="privacy.php">Privacy Policy</a></li>
                      <li><a href="tos.php">Terms &amp; Conditions</a></li>
                   </ul>
                </div>
              </div>
              <div class="span3">
                 <div class="btn-group hsds">
                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
                    </a>
                    <ul class="dropdown-menu bottom-up">
                       <li><a href="#">Contact Us</a></li>
                       <li><a href="#">Returns</a></li>
                       <li><a href="#">Site Map</a></li>
                    </ul>
                 </div>
              </div>
              <div class="span3">
                 <div class="btn-group hsds">
                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
                    </a>
                    <ul class="dropdown-menu bottom-up">
                       <li><a href="#">Brands</a></li>
                       <li><a href="#">Gift Vouchers</a></li>
                       <li><a href="#">Affiliates</a></li>
                       <li><a href="#">Specials</a></li>
                    </ul>
                 </div>
              </div>
              <div class="span3">
                 <div class="btn-group hsds">
                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
                    </a>
                    <ul class="dropdown-menu bottom-up">
                       <li><a href="#">My Account</a></li>
                       <li><a href="#">Order History</a></li>
                       <li><a href="#">Wish List</a></li>
                       <li><a href="#">Newsletter</a></li>
                    </ul>
                 </div>
              </div>
            </div><!-- #footmenu -->
          </div>
        </div><!-- #footer -->
<div id="powered" class="powered_pos"><p>Powered By <a href="/" title="Best Deals & Coupons">Couponpicks.com</a> &copy; 2015</p></div>
    </div><!-- #container -->
    <a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
        <span id="toTopHover"></span>
        <i class="icon-chevron-up icon-large"></i>
    </a>
</body>
</html>
