<?php
  
?>
 <html>
 <head>
    <meta charset="UTF-8">
    <title> Amazon Coupons & Deals | Couponpicks</title>
   
   
         <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
             <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
         <link rel="stylesheet" type="text/css" href="css/style.css">
           <link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
           <link rel="stylesheet" type="text/css" href="css/default.css">
            <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</head>
<body>
<div class="modal fade in" id="postSignInModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modalName">You've signed in!</h4>
                    </div>
                    <div class="modal-body">
                        <p>Start requesting vouchers for promo codes by clicking the <strong>Review Now</strong> buttons on products you want.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="show_last_product();">Got it!</button>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>    
      
            </body>
            </html>
