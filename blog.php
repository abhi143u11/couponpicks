<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>About | Couponpicks</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
	
	<!-- use jssor.slider.mini.js (40KB) instead for release -->
    <!-- jssor.slider.mini.js = (jssor.js + jssor.slider.js) -->
    <script type="text/javascript" src="js/jssor.js"></script>
    <script type="text/javascript" src="js/jssor.slider.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1
                $Loop: 0,                                       //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 5, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always

                    $Loop: 2,                                       //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
                    $AutoCenter: 3,                                 //[Optional] Auto center thumbnail items in the thumbnail navigator container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 3
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange thumbnails, default value is 1
                    $SpacingX: 4,                                   //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
                    $SpacingY: 4,                                   //[Optional] Vertical space between each thumbnail in pixel, default value is 0
                    $DisplayPieces: 4,                              //[Optional] Number of pieces to display, default value is 1
                    $ParkingPosition: 0,                            //[Optional] The offset position to park thumbnail
                    $Orientation: 2,                                //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
                    $DisableDrag: false                             //[Optional] Disable drag or not, default value is false
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
                if (parentWidth) {
                    var sliderWidth = parentWidth;

                    //keep the slider width no more than 810
                    sliderWidth = Math.min(sliderWidth, 810);

                    jssor_slider1.$ScaleWidth(sliderWidth);
                }
                else
                    window.setTimeout(ScaleSlider, 30);
            }
            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>
</head>
<body>
	<div class="navbar navbar-fixed-top topbar">
	   <div class="navbar-inner kasdf">
	      <div class="container container2 wrap-menu">
	         <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
	         <div class="pull-left firstsearch" id="search">
               	<div class="input-prepend">
	               	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
	               	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
               	</div>
            </div>
	         <div id="headerunder" class="pull-right colorback"></div>
	         <div id="header" class="pull-right topcart colorback">
	            <ul id="userNav">
				   <li>
				      <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
				      </a>
				      <div class="userSubMenu menu">
				         <div class="menuWrapper">
				            <ul>
				               <li><a href="#">My Profile</a></li>
				               <li><a href="#">My Deals</a></li>
				               <li><a href="#">My Coupons</a></li>
				               <li><a href="#">Notifications</a></li>
				               <li><a href="#">Messages</a></li>
				               <li><a href="#">Saved Stores</a></li>
				               <li><a href="#">Find Friends</a></li>
				               <li><a class="subUserMenuLink" href="#">Settings</a></li>
				               <li><a href="#">Sign Out</a></li>
				            </ul>
				         </div>
				      </div>
				   </li>
				</ul>
				<a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
					<img class="icon-plus-button" alt="add" src="images/blank.png">
				</a>
	         </div>
	         <div class="topmenu">
	            <div class="dropdown">
	            	<div class="click-toggle">
	            		<a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
		               <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
		               	<?php
							if ($cats->num_rows > 0) {
								while($row = $cats->fetch_assoc()) { ?>
			                  <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
			                  </li>
		                <?php
							}
						}
						?>	
		               </ul>
	            	</div>
	               <a class="padright" id="wishlist-total" href="#">My Feed</a>
	               <a href="#">Popular</a>
	               <div class="btn-group little-select">
	                  	<a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
	             			<i class="icon-reorder"></i>
	                  	</a>     	
	                  	<!-- <ul class="dropdown-menu">
	                     	<form enctype="multipart/form-data" method="post" action="#">
	                        	<div id="currency">Currency<br>
		                           <a title="Euro">€</a>
		                           <a title="Pound Sterling">£</a>
		                           <a title="US Dollar"><b>$</b></a>
	                        	</div>
	                     	</form>
	                  	</ul> -->
	                  	<div id="option-list">
	                  		<div class="menuWrapper">
	                  			<ul class="menuTopIcons ">
								   <li><a href="#">
								         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">My Feed</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Popular</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Fresh</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Heating Up</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Coupon Codes</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Printable Coupons</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Interests</div>
								      </a></li>
								</ul><!-- #menuTopIcons -->
								<ul>
								   <li><a href="#">Apps</a></li>
								   <li><a href="#">Automotive</a></li>
								   <li><a href="#">Bed &amp; Bath</a></li>
								   <li><a href="#">Computers &amp; Software</a></li>
								   <li><a href="#">Electronics</a></li>
								   <li><a href="#">Entertainment</a></li>
								   <li><a href="#">Freebies</a></li>
								   <li><a href="#">Furniture &amp; Decor</a></li>
								   <li><a href="#">Games</a></li>
								</ul>
								<ul>
								   <li><a href="#">Gifts &amp; Flowers</a></li>
								   <li><a href="#">Grocery &amp; Food</a></li>
								   <li><a href="#">Health &amp; Beauty</a></li>
								   <li><a href="#">Home &amp; Garden</a></li>
								   <li><a href="#">Kids &amp; Baby</a></li>
								   <li><a href="#">Kitchen &amp; Dining</a></li>
								   <li><a href="#">Laptop</a></li>
								   <li><a href="#">Men</a></li>
								   <li><a href="#">News</a></li>
								</ul>
								<ul>
								   <li><a href="#">Office &amp; School</a></li>
								   <li><a href="#">Other</a></li>
								   <li><a href="#">Pets</a></li>
								   <li><a href="#">Sports &amp; Outdoor</a></li>
								   <li><a href="#">Tax &amp; Finance</a></li>
								   <li><a href="#">Toys</a></li>
								   <li><a href="#">Travel &amp; Tickets</a></li>
								   <li><a href="#">TV</a></li>
								   <li><a href="#">Women</a></li>
								   <li><a href="#">DealsPlus Exclusive</a></li>
								</ul>
	                  		</div><!-- #menuWrapper -->
	                  		<div class="aboutUsLinks">
							   <div class="aboutUsSection">
							      <a href="about.php">About Us</a>
							      <span>|</span>
							      <a target="_blank" href="blog.php">Blog</a>
							      <span>|</span>
							      <a href="#">Contact</a>
							      <span>|</span>
							      <a href="privacy.php">Privacy Policy</a>
							      <span>|</span>
							      <a href="tos.php">Terms of Use</a>
							      <div class="categoryMenuSocial">
							         <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
							         <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
							         <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
							         <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
							      </div>
							   </div>
							</div><!-- #aboutUsLinks -->
	                  	</div><!-- #option-list -->
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div><!-- #navbar-fixed-top -->

	<div class="navbar navbar-static-top menubar responsive-menu">
		<div class="navbar-inner">  
		    <div class="dropdown drsp">
		        <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
		        </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                    	print_r($cats);
						if ($cats->num_rows > 0) {
							while($row = $cats->fetch_assoc()) { ?>
		                  	<li>
		                  		<a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
		                  	</li>
		            <?php
							}
						}
					?> 
                </ul>
		        <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
		        <a href="#">My Account</a>
		        <div class="btn-group little-select">
		            <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
		            <ul class="dropdown-menu">
		                <form enctype="multipart/form-data" method="post" action="index.php">
						  	<div id="currency">Currency<br>
				                <a title="Euro">€</a>
				                <a title="Pound Sterling">£</a>
				                <a title="US Dollar"><b>$</b></a>
						  	</div>
						</form>
		            </ul>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-menu -->

	<div class="navbar navbar-static-top menubar responsive-search">
		<div class="navbar-inner">   
		    <div class="container container2">
		        <div align="center" id="header">
		            <div class="pull-left fullwidth nopad5" id="search">
		                <div class="input-prepend">
		                	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
		                	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-search -->

	<div class="dialogHolder" id="addToDPDialog">
	   <div class="dialogContent">
	      <div id="addToDPDialogContent">
	         <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
	         <div id="addToDPButtons">
	            <div class="dialogBody">
	               <div class="addDialog">
	                  <h4>What are you adding?</h4>
	                  <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
	               </div>
	               <div class="addItems">
	                  <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
	                     <h6 class="itemName">Deal / Product</h6>
	                     <div class="itemImage">
	                        <img src="images/dealIcon.png">
	                     </div>
	                     <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
	                  </a>
	                  <a href="#" class="itemContainer" id="addCouponLink">
	                     <h6 class="itemName">Coupon</h6>
	                     <div class="itemImage">
	                        <img src="images/couponIcon.png">
	                     </div>
	                     <div class="itemDescription">A coupon to be used online or in store</div>
	                  </a>
	                  <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
	                     <h6 class="itemName">Topic / Photo</h6>
	                     <div class="itemImage">
	                        <img src="images/topicIcon.png">
	                     </div>
	                     <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
	                  </a>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="afterheader"></div>
	<div id="container">
		<div class="container container2">
			<div id="main">
				<div class="row">
					<div id="primary" class="c9" role="main">
						<div id="slider-wrap" class="tabberota">
							<div id="slider1_container">
							    <!-- Loading Screen -->
							    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
							        <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
							        </div>
							        <div style="position: absolute; display: block; background: url(images/loading.gif) no-repeat center center; top: 0px; left: 0px;width: 100%;height:100%;">
							        </div>
							    </div>

							    <!-- Slides Container -->
							    <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 600px; height: 300px;
							            overflow: hidden;">
							        <div>
							            <img u="image" src="images/photography/002.jpg" />
							            <div u="thumb">
							                <img class="i" src="images/photography/thumb-002.jpg" />
							                <div class="t">Banner Rotator</div>
							                <div class="c">360+ touch swipe slideshow effects</div>
							            </div>
							        </div>
							        <div>
							            <img u="image" src="images/photography/003.jpg" />
							            <div u="thumb">
							                <img class="i" src="images/photography/thumb-003.jpg" />
							                <div class="t">Image Gallery</div>
							                <div class="c">Image gallery with thumbnail navigation</div>
							            </div>
							        </div>
							        <div>
							            <img u="image" src="images/photography/004.jpg" />
							            <div u="thumb">
							                <img class="i" src="images/photography/thumb-004.jpg" />
							                <div class="t">Carousel</div>
							                <div class="c">Touch swipe, mobile device optimized</div>
							            </div>
							        </div>
							        <div>
							            <img u="image" src="images/photography/005.jpg" />
							            <div u="thumb">
							                <img class="i" src="images/photography/thumb-005.jpg" />
							                <div class="t">Themes</div>
							                <div class="c">30+ professional themems + growing</div>
							            </div>
							        </div>
							        <div>
							            <img u="image" src="images/photography/006.jpg" />
							            <div u="thumb">
							                <img class="i" src="images/photography/thumb-006.jpg" />
							                <div class="t">Tab Slider</div>
							                <div class="c">Tab slider with auto play options</div>
							            </div>
							        </div>
							    </div>
							    <div u="thumbnavigator" class="jssort11" style="left: 605px; top:0px;">
							        <!-- Thumbnail Item Skin Begin -->
							        <div u="slides" style="cursor: default;">
							            <div u="prototype" class="p" style="top: 0; left: 0;">
							                <div u="thumbnailtemplate" class="tp"></div>
							            </div>
							        </div>
							        <!-- Thumbnail Item Skin End -->
							    </div>
							</div>
						</div><!-- #slider-wrap -->
						<div class="row">
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Savvy Shopping</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> Big Screen TV vs. Projector </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Nicole Cormier" href="#">Nicole Cormier</a>&nbsp;•&nbsp;<time datetime="2014-07-16" class="published updated">July 16, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="187" alt="Untitled" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/Untitled3-200x187.jpg">
						            </a>
						            <div class="excerpt">
						                <p>When you’re in the market for a big TV, there are many options available, making the decision on what to buy one that deserves research. Maybe you’ve decided that you’re going to forgo the screen and go with a projector instead. The following list will break down the differences between a big screen and a projector. </p>
						                <p class="more-link-p"><a href="#" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Fashion &amp; Beauty</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> At Home Facial Mask Recipes </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Stacy Brecht" href="#">Stacy Brecht</a>&nbsp;•&nbsp;<time datetime="2014-07-14" class="published updated">July 14, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="263" alt="iStock_000014159226Small" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/iStock_000014159226Small-200x263.jpg">
						            </a>
						            <div class="excerpt">
						                <p>Treat yourself to special at home pampering with these easy-to-make facial masks. Treat yourself to some special at-home pampering with these easy-to-make facial masks. Ideal for a variety of skin types, the combinations below help reveal skin’s natural glow in 15 minutes and contain far less chemicals than many skin care regimes. </p>
						                <p class="more-link-p"><a href="#" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Savvy Shopping</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> Apples to Robots: Pros and Cons of switching from Apple to Android </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Alli Sands" href="#">Alli Sands</a>&nbsp;•&nbsp;<time datetime="2014-07-09" class="published updated">July 9, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="241" alt="apple-vs-android1" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/apple-vs-android1-200x241.jpg">
						            </a>
						            <div class="excerpt">
						                <p>Considering trying an Android phone but still love your iPhone? Learn what sets these smartphones apart. </p>
						                <p class="more-link-p"><a href="#" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Savvy Shopping</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> Which Digital TV Streaming Service is Right For You? </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Jennifer Eberhart" href="#">Jennifer Eberhart</a>&nbsp;•&nbsp;<time datetime="2014-07-07" class="published updated">July 7, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="143" alt="Untitled-3" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/Untitled-3-200x143.jpg">
						            </a>
						            <div class="excerpt">
						                <p>If, like many, you’ve decided to ditch pricey cable packages in favor for online streaming services, you have quite a few choices at your disposal. With everything from Netflix and Hulu to Apple TV, it&nbsp;can get pretty difficult to figure out just what shows to watch, let alone how you should watch them. Luckily, you’ve…</p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Buyer's Guides</a>, <a rel="category tag" href="#">How To</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> How to Make Coupons a Habit </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Nicole Cormier" href="#">Nicole Cormier</a>&nbsp;•&nbsp;<time datetime="2014-07-02" class="published updated">July 2, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="299" alt="Printable Coupons" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/iStock_000015996900Small-200x299.jpg">
						            </a>
						            <div class="excerpt">
						                <p>Saving money is a habit worth making, and it’s not too difficult for anyone to master. </p>
						                <p class="more-link-p"><a href="//www.dealsplus.com/blog/make-coupons-habit/" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Health and Fitness</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> Work Out Trend: CrossFit </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Alli Sands" href="#">Alli Sands</a>&nbsp;•&nbsp;<time datetime="2014-06-27" class="published updated">June 27, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="200" alt="Workout Rope" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/rope-excersise-200x200.jpg">
						            </a>
						            <div class="excerpt">
						                <p>You’ll sweat, breath heavily, and probably ache a little bit but CrossFit is worth it all! </p>
						                <p class="more-link-p"><a href="#" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Home and Garden</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> Apartment Dwelling: Tricks to Make Your Teeny Space Look Bigger </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Jennifer Eberhart" href="#">Jennifer Eberhart</a>&nbsp;•&nbsp;<time datetime="2014-06-26" class="published updated">June 26, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="198" alt="Room Organize Shelf" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/Untitled2-200x198.jpg">
						            </a>
						            <div class="excerpt">
						                <p>The trick to apartment living is to make that teeny space look as large as it possibly can. There are a few things you can do to keep yourself both sane and organized in a small apartment &ndash; read on to find out how. </p>
						                <p class="more-link-p"><a href="#" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Savvy Shopping</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> The Best Speakers for Your Home Entertainment Needs </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Jennifer Geisman" href="#">Jennifer Geisman</a>&nbsp;•&nbsp;<time datetime="2014-06-20" class="published updated">June 20, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="200" alt="350-669972-847__1" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/350-669972-847__1-200x200.jpg">
						            </a>
						            <div class="excerpt">
						                <p>Before you kick off the season of barbecues and swim soirées, it is time to assess your entertaining capabilities and the first place to begin is making sure making the music will rock your next party. </p>
						                <p class="more-link-p"><a href="#" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">Buyer's Guides</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> Save Money at the Market: Learn the Art of Meal Planning </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Jennifer Eberhart" href="#">Jennifer Eberhart</a>&nbsp;•&nbsp;<time datetime="2014-06-17" class="published updated">June 17, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="133" alt="Meal Planning" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/04/iStock_000013950444Small-200x133.jpg">
						            </a>
						            <div class="excerpt">
						                <p>If you’re like most people, you probably spend a ton of money on food day in and day out. Learn the art of meal planning to both better organize your life and save up those hard-earned dollars. </p>
						                <p class="more-link-p"><a href="#" class="more-link">Read more →</a>
						                </p>
						            </div>
						        </div>
						    </article>
						    <article class="c12 bottom-border">
						        <header>
						            <h3 class="post-category"><i class="icon-file"></i><a rel="category tag" href="#">News &amp; Trends</a>, <a rel="category tag" href="#">Savvy Shopping</a></h3>
						            <h1 class="entry-title"> <a rel="bookmark" href="#"> New DealsPlus iPhone App Available Now! Win an iPad Mini with Retina Display </a> </h1>
						            <h2 class="entry-meta"> by <a rel="author" title="Posts by Rene Kirschbaum" href="#">Rene Kirschbaum</a>&nbsp;•&nbsp;<time datetime="2014-06-05" class="published updated">June 5, 2014</time> </h2> </header>
						        <div class="entry-content">
						            <a class="image-anchor" href="#"><img width="200" height="107" alt="dealsplusipadgiveaway-1" class="alignleft wp-post-image" src="//www.dealsplus.com/blog/wp-content/uploads/2014/06/dealsplusipadgiveaway-1-200x107.jpg">
						            </a>
						            <div class="excerpt">
						                <p>We’ve released an entirely new DealsPlus iPhone app&nbsp;with a whole new look.&nbsp;We gave the app a completely new look and design. The app carefully balances a visually modern and beautiful shopping experience with simplicity and ease of use.&nbsp;Click here to download on iTunes Users can seamlessly browse popular deals of the day as well as…</p>
						            </div>
						        </div>
						    </article>
						</div><!-- #row-->
						<div id="pagination">
						    <div class="total-pages">Page 1 of 14</div>
						  <span class="page-numbers current">1</span> 
						  <a href="//www.dealsplus.com/blog/page/2/" class="page-numbers">2</a> 
						  <a href="//www.dealsplus.com/blog/page/3/" class="page-numbers">3</a> 
						  <span class="page-numbers dots">…</span> 
						  <a href="//www.dealsplus.com/blog/page/14/" class="page-numbers">14</a> 
						  <a href="//www.dealsplus.com/blog/page/2/" class="next page-numbers">»</a>
						</div><!-- #pagination-->
					</div><!-- #primary -->
					<div role="complementary" class="c3 end" id="secondary">
					    <aside class="widget widget_categories">
					        <h3 class="widget-title">Categories</h3>
					        <ul>
					            <a href="#">
					                <li class="cat-item cat-item-10 dpCatLink dpCatIcons dpCatIcon-buyersguides"> <span>Buyer's Guides</span> <span class="dpCatNum">39 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-12 dpCatLink dpCatIcons dpCatIcon-fashion"> <span>Fashion &amp; Beauty</span> <span class="dpCatNum">22 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-14 dpCatLink dpCatIcons dpCatIcon-food"> <span>Food</span> <span class="dpCatNum">28 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-7 dpCatLink dpCatIcons dpCatIcon-giftguides"> <span>Gift Guides</span> <span class="dpCatNum">11 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-15 dpCatLink dpCatIcons dpCatIcon-health"> <span>Health and Fitness</span> <span class="dpCatNum">20 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-13 dpCatLink dpCatIcons dpCatIcon-home"> <span>Home and Garden</span> <span class="dpCatNum">19 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-6 dpCatLink dpCatIcons dpCatIcon-howto"> <span>How To</span> <span class="dpCatNum">17 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-2 dpCatLink dpCatIcons dpCatIcon-infographic"> <span>Infographic</span> <span class="dpCatNum">1 Post</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-9 dpCatLink dpCatIcons dpCatIcon-news"> <span>News &amp; Trends</span> <span class="dpCatNum">9 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-16 dpCatLink dpCatIcons dpCatIcon-parenting"> <span>Parenting</span> <span class="dpCatNum">10 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-8 dpCatLink dpCatIcons dpCatIcon-savvyshopping"> <span>Savvy Shopping</span> <span class="dpCatNum">23 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-11 dpCatLink dpCatIcons dpCatIcon-travel"> <span>Travel</span> <span class="dpCatNum">13 Posts</span> </li>
					            </a>
					            <a href="#">
					                <li class="cat-item cat-item-1 dpCatLink dpCatIcons dpCatIcon-uncategorized"> <span>Uncategorized</span> <span class="dpCatNum">4 Posts</span> </li>
					            </a>
					        </ul>
					    </aside>
					    <aside class="widget widget_dpad_widget">
					        <div class="dpAdWidget">
					            <a href="https://itunes.apple.com/us/app/dealspl.us-coupons/id496056416" class="image-anchor"><img src="//dealspl.us/images/v3/iphoneApp.png" class="dpAdWidgetContent">
					            </a>
					        </div>
					    </aside>
					    <aside class="widget widget_recent_entries" >
					        <h3 class="widget-title">Recent Posts</h3>
					        <ul>
					            <li> <a href="#">Big Screen TV vs. Projector</a> </li>
					            <li> <a href="#">At Home Facial Mask Recipes</a> </li>
					            <li> <a href="#">Apples to Robots: Pros and Cons of switching from Apple to Android</a> </li>
					            <li> <a href="#">Which Digital TV Streaming Service is Right For You?</a> </li>
					            <li> <a href="#">How to Make Coupons a Habit</a> </li>
					        </ul>
					    </aside>
					    <aside class="widget widget_dptd_widget" >
					        <h3 class="widget-title">Trending Deals</h3>
					        <a href="#">
					            <div class="dpTrendingDeal dpLink">
					                <div class="dpDealPlusContainer">
					                    <div class="dpDealPlus">5</div>
					                    <div class="dpDealPlusLabel">plus'd</div>
					                </div>
					                <div class="dpDealTitle"> Pier 1 Imports | Up to 70% off Outdoor Furniture </div>
					                <div class="dpClear"></div>
					            </div>
					        </a>
					        <a href="#">
					            <div class="dpTrendingDeal dpLink">
					                <div class="dpDealPlusContainer">
					                    <div class="dpDealPlus">5</div>
					                    <div class="dpDealPlusLabel">plus'd</div>
					                </div>
					                <div class="dpDealTitle"> Nautica Ahoy 21 inch Hardside Spinner Suitcase (5 Colors) </div>
					                <div class="dpClear"></div>
					            </div>
					        </a>
					        <a href="#">
					            <div class="dpTrendingDeal dpLink">
					                <div class="dpDealPlusContainer">
					                    <div class="dpDealPlus">5</div>
					                    <div class="dpDealPlusLabel">plus'd</div>
					                </div>
					                <div class="dpDealTitle"> 20-50% Off JCPenney Site-Wide Coupon Code + 20% Off Clearance </div>
					                <div class="dpClear"></div>
					            </div>
					        </a>
					        <a href="#">
					            <div class="dpTrendingDeal dpLink">
					                <div class="dpDealPlusContainer">
					                    <div class="dpDealPlus">5</div>
					                    <div class="dpDealPlusLabel">plus'd</div>
					                </div>
					                <div class="dpDealTitle"> Groupon Apple Event | Save Up to 75% Off </div>
					                <div class="dpClear"></div>
					            </div>
					        </a>
					        <a href="#">
					            <div class="dpTrendingDeal dpLink">
					                <div class="dpDealPlusContainer">
					                    <div class="dpDealPlus">5</div>
					                    <div class="dpDealPlusLabel">plus'd</div>
					                </div>
					                <div class="dpDealTitle"> Adidas | Up to 40% Off Sale items </div>
					                <div class="dpClear"></div>
					            </div>
					        </a>
					        <a href="#">
					            <div class="dpTrendingDeal dpLink">
					                <div class="dpDealPlusContainer">
					                    <div class="dpDealPlus">3</div>
					                    <div class="dpDealPlusLabel">plus'd</div>
					                </div>
					                <div class="dpDealTitle"> 30% Off Oakley Vault Men's Clearance </div>
					                <div class="dpClear"></div>
					            </div>
					        </a>
					        <a href="#">
					            <div class="dpTrendingDeal dpLink">
					                <div class="dpDealPlusContainer">
					                    <div class="dpDealPlus">5</div>
					                    <div class="dpDealPlusLabel">plus'd</div>
					                </div>
					                <div class="dpDealTitle"> Extra 30% Off Forever21 Sale w/ Free Shipping or Sign-up for Mystery Offer Discount </div>
					                <div class="dpClear"></div>
					            </div>
					        </a>
					    </aside>
					    <aside class="widget widget_text" id="text-6">
					        <div class="textwidget">
					            <img src="images/ads2.PNG" />
					        </div>
					    </aside>
					</div>
				</div>
			</div><!-- #main -->
		</div><!-- #container2 -->
		<div id="footericon" class="bigfooticon colorbackwhite handpoint">
			<i class="icon-eject"></i>
		</div><!-- #footericon -->
		<div id="footer" class="colorback" style="display: block;">
			<div class="container">
				<div class="inner footmenu">
					<div class="span3">
						<div class="btn-group hsds">
						   <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
						   </a>
						   <ul class="dropdown-menu bottom-up">
						      <li><a href="about.php">About Us</a></li>
						      <li><a href="#">Delivery Information</a></li>
						      <li><a href="privacy.php">Privacy Policy</a></li>
						      <li><a href="tos.php">Terms &amp; Conditions</a></li>
						   </ul>
						</div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Contact Us</a></li>
					         <li><a href="#">Returns</a></li>
					         <li><a href="#">Site Map</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Brands</a></li>
					         <li><a href="#">Gift Vouchers</a></li>
					         <li><a href="#">Affiliates</a></li>
					         <li><a href="#">Specials</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">My Account</a></li>
					         <li><a href="#">Order History</a></li>
					         <li><a href="#">Wish List</a></li>
					         <li><a href="#">Newsletter</a></li>
					      </ul>
					   </div>
					</div>
				</div><!-- #footmenu -->
			</div>
		</div><!-- #footer -->
<div id="powered" class="powered_pos"><p>Powered By <a href="/" title="Best Deals & Coupons">Couponpicks.com</a> &copy; 2015</p></div>
	</div><!-- #container -->
	<a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
		<span id="toTopHover"></span>
		<i class="icon-chevron-up icon-large"></i>
	</a>
</body>
</html>
