<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>About | Couponpicks</title>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
	<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="js/javascript.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" media="screen" />
	<script type="text/javascript">
		$(document).ready(function() {
			$(".various").fancybox({
				maxWidth	: 630,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		});
	</script>
	<script type="application/javascript">
         function installFirefox (aEvent)
         {
             for (var a = aEvent.target; a.href === undefined;) a = a.parentNode;
             var params = {
                 "Foo": { URL: aEvent.target.href,
                     IconURL: aEvent.target.getAttribute("iconURL"),
                     Hash: aEvent.target.getAttribute("hash"),
                     toString: function () { return this.URL; }
                 }
             };
             InstallTrigger.install(params);
         
             return false;
        }
    </script>
    <script type="text/javascript">
      $('.showBookmarklet').click(function() {
          if ($('.bookmarklet').is(':visible')) {
              $('.bookmarklet').slideUp();
          } else {
              $('.bookmarklet').slideDown();
          }
      });
      
      $('.bookmarkletAddLink').hover(function() {
          $(this).find('a').text('Drag Me to Bookmark Bar');
          $('.dragInstruction').toggle();
      }, function() {
          $(this).find('a').text('Add To CouponPicks');
      });
	</script>
</head>
<body>
	<div class="navbar navbar-fixed-top topbar">
	   <div class="navbar-inner kasdf">
	      <div class="container container2 wrap-menu">
	         <a href="index.php" class="logo"><img width="139" height="35" src="images/logocoupon.jpg"></a>
	         <div class="pull-left firstsearch" id="search">
               	<div class="input-prepend">
	               	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
	               	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
               	</div>
            </div>
	         <div id="headerunder" class="pull-right colorback"></div>
	         <div id="header" class="pull-right topcart colorback">
	            <ul id="userNav">
				   <li>
				      <a id="userNavLink" href="#"><span>admin</span><img width="26" height="26" alt="userImg" id="userImage" src="images/avatar48.gif">
				      </a>
				      <div class="userSubMenu menu">
				         <div class="menuWrapper">
				            <ul>
				               <li><a href="#">My Profile</a></li>
				               <li><a href="#">My Deals</a></li>
				               <li><a href="#">My Coupons</a></li>
				               <li><a href="#">Notifications</a></li>
				               <li><a href="#">Messages</a></li>
				               <li><a href="#">Saved Stores</a></li>
				               <li><a href="#">Find Friends</a></li>
				               <li><a class="subUserMenuLink" href="#">Settings</a></li>
				               <li><a href="#">Sign Out</a></li>
				            </ul>
				         </div>
				      </div>
				   </li>
				</ul>
				<a class="addToDPButton userPlusIcon various" id="addToDPButton" href="#addToDPDialog">
					<img class="icon-plus-button" alt="add" src="images/blank.png">
				</a>
	         </div>
	         <div class="topmenu">
	            <div class="dropdown">
	            	<div class="click-toggle">
	            		<a href="#" data-toggle="dropdown" class="dropdown-toggle padright">Categories &nbsp;<i class="icon-sort-down icon-up"></i></a>
		               <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu mega-menu">
		               	<?php
							if ($cats->num_rows > 0) {
								while($row = $cats->fetch_assoc()) { ?>
			                  <li ><a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
			                  </li>
		                <?php
							}
						}
						?>	
		               </ul>
	            	</div>
	               <a class="padright" id="wishlist-total" href="#">My Feed</a>
	               <a href="#">Popular</a>
	               <div class="btn-group little-select">
	                  	<a href="#" data-toggle="dropdown" class="btn-mini colorback button-click">
	             			<i class="icon-reorder"></i>
	                  	</a>     	
	                  	<!-- <ul class="dropdown-menu">
	                     	<form enctype="multipart/form-data" method="post" action="#">
	                        	<div id="currency">Currency<br>
		                           <a title="Euro">€</a>
		                           <a title="Pound Sterling">£</a>
		                           <a title="US Dollar"><b>$</b></a>
	                        	</div>
	                     	</form>
	                  	</ul> -->
	                  	<div id="option-list">
	                  		<div class="menuWrapper">
	                  			<ul class="menuTopIcons ">
								   <li><a href="#">
								         <span class="icon-my-feed"><img alt="My Feed Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">My Feed</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-popular"><img alt="Popular Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Popular</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-fresh"><img alt="Fresh Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Fresh</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-heating-up"><img alt="Heating Up Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Heating Up</div>
								    </a></li>
								   <li><a href="#">
								         <span class="icon-coupon-codes"><img alt="Coupon Codes Top Icon" src="images/blank.png"></span> 
								         <div class="menuTopIconTitle">Coupon Codes</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-printable-coupons"><img alt="Printable Coupons Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Printable Coupons</div>
								    </a></li>
								   <li><a href="#">
								        <span class="icon-interests"><img alt="Interests Top Icon" src="images/blank.png"></span> 
								        <div class="menuTopIconTitle">Interests</div>
								      </a></li>
								</ul><!-- #menuTopIcons -->
								<ul>
								   <li><a href="#">Apps</a></li>
								   <li><a href="#">Automotive</a></li>
								   <li><a href="#">Bed &amp; Bath</a></li>
								   <li><a href="#">Computers &amp; Software</a></li>
								   <li><a href="#">Electronics</a></li>
								   <li><a href="#">Entertainment</a></li>
								   <li><a href="#">Freebies</a></li>
								   <li><a href="#">Furniture &amp; Decor</a></li>
								   <li><a href="#">Games</a></li>
								</ul>
								<ul>
								   <li><a href="#">Gifts &amp; Flowers</a></li>
								   <li><a href="#">Grocery &amp; Food</a></li>
								   <li><a href="#">Health &amp; Beauty</a></li>
								   <li><a href="#">Home &amp; Garden</a></li>
								   <li><a href="#">Kids &amp; Baby</a></li>
								   <li><a href="#">Kitchen &amp; Dining</a></li>
								   <li><a href="#">Laptop</a></li>
								   <li><a href="#">Men</a></li>
								   <li><a href="#">News</a></li>
								</ul>
								<ul>
								   <li><a href="#">Office &amp; School</a></li>
								   <li><a href="#">Other</a></li>
								   <li><a href="#">Pets</a></li>
								   <li><a href="#">Sports &amp; Outdoor</a></li>
								   <li><a href="#">Tax &amp; Finance</a></li>
								   <li><a href="#">Toys</a></li>
								   <li><a href="#">Travel &amp; Tickets</a></li>
								   <li><a href="#">TV</a></li>
								   <li><a href="#">Women</a></li>
								   <li><a href="#">DealsPlus Exclusive</a></li>
								</ul>
	                  		</div><!-- #menuWrapper -->
	                  		<div class="aboutUsLinks">
							   <div class="aboutUsSection">
							      <a href="about.php">About Us</a>
							      <span>|</span>
							      <a target="_blank" href="blog.php">Blog</a>
							      <span>|</span>
							      <a href="#">Contact</a>
							      <span>|</span>
							      <a href="privacy.php">Privacy Policy</a>
							      <span>|</span>
							      <a href="tos.php">Terms of Use</a>
							      <div class="categoryMenuSocial">
							         <a class="socialItems grey" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Apple App" class="icon-apple"></a>
							         <a class="socialItems lightBlue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Twitter Share" class="icon-twitter-share"></a>
							         <a class="socialItems blue" target="_blank" href="#"><img width="24" height="24" src="images/blank.png" alt="Facebook Share" class="icon-facebook-share"></a>
							         <a class="socialItems red" target="_blank" data-pin-config="above" data-pin-do="buttonPin" href="#"><img width="24" height="24" src="images/blank.png" alt="Pinterest Share" class="icon-pinterest-share"></a>
							      </div>
							   </div>
							</div><!-- #aboutUsLinks -->
	                  	</div><!-- #option-list -->
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div><!-- #navbar-fixed-top -->

	<div class="navbar navbar-static-top menubar responsive-menu">
		<div class="navbar-inner">  
		    <div class="dropdown drsp">
		        <a href="#" data-toggle="dropdown" class="dropdown-toggle padright rsp-cat">Categories &nbsp;<i class="icon-sort-down icon-up"></i>
		        </a>
                <ul role="menu" class="dropdown-menu rsp-listcat mega-menu">
                    <?php
                    	print_r($cats);
						if ($cats->num_rows > 0) {
							while($row = $cats->fetch_assoc()) { ?>
		                  	<li>
		                  		<a <?php if($catid == $row['category_id']){ echo ' class="active"'; }?> href="index.php?cat=<?php echo $row['category_id'];?>"><?php echo $row['name'];?></a>
		                  	</li>
		            <?php
							}
						}
					?> 
                </ul>
		        <a class="padright" id="wishlist-total" href="#">Wish List (0)</a>
		        <a href="#">My Account</a>
		        <div class="btn-group little-select">
		            <a href="#" data-toggle="dropdown" class="btn-mini colorback ">$</a>
		            <ul class="dropdown-menu">
		                <form enctype="multipart/form-data" method="post" action="index.php">
						  	<div id="currency">Currency<br>
				                <a title="Euro">€</a>
				                <a title="Pound Sterling">£</a>
				                <a title="US Dollar"><b>$</b></a>
						  	</div>
						</form>
		            </ul>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-menu -->

	<div class="navbar navbar-static-top menubar responsive-search">
		<div class="navbar-inner">   
		    <div class="container container2">
		        <div align="center" id="header">
		            <div class="pull-left fullwidth nopad5" id="search">
		                <div class="input-prepend">
		                	<span id="buttn-search" class="add-on handpoint"><i class="icon-search icon-large icon-top"></i></span>
		                	<input type="text" onkeydown="this.style.color = '#000000';" onclick="this.value = '';" value="Search" name="filter_name" class="form-search">
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	</div><!-- #responsive-search -->

	<div class="dialogHolder" id="addToDPDialog">
	   <div class="dialogContent">
	      <div id="addToDPDialogContent">
	         <div class="dialogHeader">Add to <img width="96" height="20" src="images/logocoupon.jpg"></div>
	         <div id="addToDPButtons">
	            <div class="dialogBody">
	               <div class="addDialog">
	                  <h4>What are you adding?</h4>
	                  <p>Add it easier with our <strong><a href="#">bookmarklet</a></strong></p>
	               </div>
	               <div class="addItems">
	                  <a href="#" data-type="deal" class="itemContainer" id="addDealLink">
	                     <h6 class="itemName">Deal / Product</h6>
	                     <div class="itemImage">
	                        <img src="images/dealIcon.png">
	                     </div>
	                     <div class="itemDescription">Add a link to a Sale, Deal or Product</div>
	                  </a>
	                  <a href="#" class="itemContainer" id="addCouponLink">
	                     <h6 class="itemName">Coupon</h6>
	                     <div class="itemImage">
	                        <img src="images/couponIcon.png">
	                     </div>
	                     <div class="itemDescription">A coupon to be used online or in store</div>
	                  </a>
	                  <a href="#" data-type="link" class="itemContainer" id="addTopicLink">
	                     <h6 class="itemName">Topic / Photo</h6>
	                     <div class="itemImage">
	                        <img src="images/topicIcon.png">
	                     </div>
	                     <div class="itemDescription">Helpful tips &amp; questions about saving money</div>
	                  </a>
	               </div>
	            </div>
	         </div>
	      </div>
	   </div>
	</div>

	<div class="afterheader"></div>
	<div id="container">
		<div class="container container2">
			<div style="margin:10px auto;">
				<div class="infoMenu box expandMenuBar">
				   <div class="menuSection">
				      <h3>CouponPicks</h3>
				      <ul>
				         <li><a href="about.php">About Us</a></li>
				         <li><a href="jobs.php">Jobs</a></li>
				         <li><a target="_blank" href="#">Blog</a></li>
				         <li><a href="cp_tools.php">Tools</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Help</h3>
				      <ul>
				         <li><a href="#">Ask &amp; Share</a></li>
				         <li><a href="#">Contact</a></li>
				         <li><a href="faqs.php">FAQs</a></li>
				         <li><a href="privacy.php">Privacy Policy</a></li>
				         <li><a href="tos.php">Terms of Use</a></li>
				      </ul>
				   </div>
				   <div class="menuSection">
				      <h3>Business With Us</h3>
				      <ul>
				         <li>
				            <a href="retailers.php">Retailers</a>
				         </li>
				         <li><a href="#">Advertisers</a></li>
				      </ul>
				   </div>
				</div><!-- #infoMenu -->
				<div class="infoContent box">
				    <h2>Privacy Policy</h2>
				    <p>Here at CouponPicks we recognize that your privacy is very important, and to keep you informed about our privacy practices we ask that you please read the guidelines described below. Keep in mind that because CouponPicks is a growing website and internet technologies are constantly evolving, these guidelines are subject to change and any said changes will be posted on this page. Any material changes from current practices will be posted on the main page of this website to serve as notice. CouponPicks is not responsible for the content or the privacy policies of websites to which it may link.</p>
				    <br>
				    <h3>Personal Information</h3>
				    <p>During registration for membership, we may collect personal information such as your name, email address, birth date, gender, zip code, occupation, industry, and personal interests. For some financial products and services we may also ask for more information. We usually collect personal information directly from you although sometimes we may use agents or service providers to do this for us. When you are online, we collect information regarding the pages within our network that you visit. We may also acquire lists from other sources, both from other companies and from other public documents. For each visitor on our website, our web server automatically recognizes only the consumer's domain name, but not the e-mail address (where possible). We do not collect information on consumers who browse our website.</p>
				    <br>
				    <h3>Location Information</h3>
				    <p>Use of our mobile application may, at your election, include location-based services. When you use the app, you may transmit information about your location to CouponPicks. That information will be used to offer you deals and coupons that are close to your geographic location.</p>
				    <br>
				    <h3>Use of Information</h3>
				    <p>We use your information to provide our services to you, to fulfill administrative functions associated with these services, to enter into contracts with you or third parties, and for marketing and client relationship purposes. You may receive e-mails from us to let you know about new products, services, and upcoming events that may interest you. If you do not want to receive e-mails from us or you want to make changes to your e-mail settings, please change your setting under your setting tab. We will never sell or distribute your e-mail address to other companies or organizations, and your account password will always remain protected. We may use customer information for new, unanticipated uses not previously disclosed in our privacy notice. If our information practices change at some time in the future we will post the policy changes to our website to notify you of these changes, and provide you with the ability to opt out of these new uses. If you are concerned about how your information is used, you should check our Privacy Policy periodically.</p>
				    <br>
				    <h3>Cookies</h3>
				    <p>A cookie is a piece of data that a website transfers to an individual's hard drive for record-keeping purposes. Cookies can facilitate a user's ongoing access to and use of a site, and they allow us to track usage patterns and to compile data that can help us improve our content and target advertising. If you do not want information collected through the use of cookies, there is a simple procedure in most browsers that allows you to deny or accept the cookie feature. But you should note that cookies may be necessary to provide you with features such as merchandise transactions or registered services.</p>
				    <br>
				    <h3>Children</h3>
				    <p>The Children's Online Privacy Protection Act (COPPA) of 1998 provides safeguards to protect children who use the internet by regulating the online collection of personal information from children under the age of 13. Because of these safeguards, children under the age of 13 are not able to access or use all products and services on CouponPicks. For more information, see our Children's Policy.</p>
				    <br>
				    <h3>Use of Aggregate Data</h3>
				    <p>We may collect certain non-personal information to optimize our website for your computer (e.g., the identity of your Internet browser, the type of operating system you use, your IP address, and the domain name of your Internet service provider). We may use such non-personal information for internal purposes, including but not limited to improving the content of our website. CouponPicks may use personally identifiable information in aggregate form to improve our goods and services, including our website, and to make them more responsive to the needs of our customers. This statistical compilation and analysis of information may also be used by CouponPicks, or provided to others as a summary report for marketing, advertising, or research purposes.</p>
				    <br>
				    <h3>Confidentiality and Security</h3>
				    <p>We limit access to personal information about you to employees for whom we reasonably believe it is necessary in order to provide products or services to you. We have physical, electronic, and procedural safeguards that comply with federal regulations to protect against the loss, misuse, or alteration of information that we have collected from you at our website.</p>
				    <br>
				    <h3>What else you should know about privacy on the Internet</h3>
				    <ul>
				        <li>Remember to close your browser when you have finished your CouponPicks session. This is to ensure that others cannot access your personal information and correspondence if you share a computer with someone else or are using a computer in a public place, like a library or internet cafe. You as an individual are responsible for the security of and access to your own computer.</li>
				        <li>Whenever you voluntarily disclose personal information over the internet, this information can be collected and used by others. In short, if you post personal information in publicly accessible online forums, you may receive unsolicited messages from other parties in return.</li>
				        <li>Ultimately, you are solely responsible for maintaining the secrecy of your username and passwords and any account information.</li>
				        <li>Please be careful and responsible whenever you are using the internet.</li>
				    </ul>
				    <br><i>If you have any questions about our Privacy Policy, or you would like to cancel your account, please contact us at support@CouponPicks.com  </i>
				</div><!-- #infoContent -->
			</div>
		</div><!-- #container2 -->
		<div id="footericon" class="bigfooticon colorbackwhite handpoint">
			<i class="icon-eject"></i>
		</div><!-- #footericon -->
		<div id="footer" class="colorback" style="display: block;">
			<div class="container">
				<div class="inner footmenu">
					<div class="span3">
						<div class="btn-group hsds">
						   <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Information<i class="icon-caret-up ahdbs"></i>
						   </a>
						   <ul class="dropdown-menu bottom-up">
						      <li><a href="about.php">About Us</a></li>
						      <li><a href="#">Delivery Information</a></li>
						      <li><a href="privacy.php">Privacy Policy</a></li>
						      <li><a href="tos.php">Terms &amp; Conditions</a></li>
						   </ul>
						</div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Customer Service<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Contact Us</a></li>
					         <li><a href="#">Returns</a></li>
					         <li><a href="#">Site Map</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">Extras<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">Brands</a></li>
					         <li><a href="#">Gift Vouchers</a></li>
					         <li><a href="#">Affiliates</a></li>
					         <li><a href="#">Specials</a></li>
					      </ul>
					   </div>
					</div>
					<div class="span3">
					   <div class="btn-group hsds">
					      <a href="#" data-toggle="dropdown" class="btn dropdown-toggle colorback">My Account<i class="icon-caret-up ahdbs"></i>
					      </a>
					      <ul class="dropdown-menu bottom-up">
					         <li><a href="#">My Account</a></li>
					         <li><a href="#">Order History</a></li>
					         <li><a href="#">Wish List</a></li>
					         <li><a href="#">Newsletter</a></li>
					      </ul>
					   </div>
					</div>
				</div><!-- #footmenu -->
			</div>
		</div><!-- #footer -->
<div id="powered" class="powered_pos"><p>Powered By <a href="/" title="Best Deals & Coupons">Couponpicks.com</a> &copy; 2015</p></div>
	</div><!-- #container -->
	<a id="toTop" href="#" class="bigfooticon colorback" style="display: block;">
		<span id="toTopHover"></span>
		<i class="icon-chevron-up icon-large"></i>
	</a>
</body>
</html>
