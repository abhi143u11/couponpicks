<?php
require_once 'classes/config.php';

session_start();

 
  

?>
<html>
 <head>
    <meta charset="UTF-8">
    <title> Amazon Coupons & Deals | Couponpicks</title>
   
   
       <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
         <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
         <link rel="stylesheet" type="text/css" href="css/style.css">
           <link rel="stylesheet" type="text/css" href="css/font-awesome.css" />
           <link rel="stylesheet" type="text/css" href="css/default.css">
         
          
  <script type="text/javascript" src="js/jquery-1.6.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.min.js"></script>
   <script src="js/bootstrap.min.js"></script>

        
            <script type="text/javascript" src="https://static.getclicky.com/js"></script>
            <script type="text/javascript">try{ clicky.init(100829539); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100829539ns.gif" /></p></noscript>

            <script type="text/javascript" src="js/art.js"></script>
</head>
<body style="background-color: #E7E7E4;">
<div style="padding-top: 80px; text-align: center;">
            <h1>Your password has been updated sucessfully!</h1>
           
            <p>Sign in by clicking <a data-toggle="modal" data-target="#accountModal" id="signinbutton">here</a> to get started!</p>
        </div>

        <script type="text/javascript">
/*function showmodal()
{
   
      $("#ConfirmationModal").modal('hide');
        $("#accountModal").modal('show');
}*/
</script>   
    
        <div class="modal fade in" id="accountModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

<div class="row sign-groups" id="sign-in-form">
    <div class="col-xs-12">
        <span class="clickable pull-right" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#create-account-form').show();">or Create Account</span>
        <h3>Sign In</h3>

        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">
            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="Password" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#forgot-password-form').show();">Forgot password?</span>

            <button type="button" class="btn btn-primary pull-right" onclick="sign_in()">Sign In</button>

        </form>

    </div>
</div>


<div class="row sign-groups" id="forgot-password-form" style="display:none;">
    <div class="col-xs-12">
        <h3>Forgot Password</h3>

        <p>Enter your email and we will send you password reset instructions.</p>
        <br>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal" onkeypress="return event.keyCode != 13;">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I know my log in details</span>

            <button type="button" class="btn btn-primary pull-right" onclick="forgot_password()">Reset Password</button>

        </form>

    </div>
</div>

<div class="row sign-groups" id="create-account-form" style="display:none;">
    <div class="col-xs-12">
        <h3>Create Account</h3>
        <p class="error" style="color: #f00; display: none; padding-bottom: 8px; text-align: center;"></p>

        <form class="form-horizontal">
            <input type="hidden" name="account_token" value="284ca83da50c2cfc58e1cbf29721578f">

            <div class="form-group">
                <label for="sign_email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="sign_email" name="email" placeholder="Email" autocomplete="off">
                    <p style="margin-top: 10px">* Required to receive promo codes</p>
                </div>
            </div>

            <div class="form-group">
                <label for="sign_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="sign_password" name="password" placeholder="8 characters minimum" autocomplete="off">
                    <input type="checkbox" name="mc_check" id="mc_check" checked=""><label for="mc_check" style="margin: 10px 0 0 10px;">Get the hottest daily offer updates sent to my inbox</label>
                </div>
            </div>

            <span class="clickable col-sm-offset-3" style="padding-left: 8px; color: #3498db;" onclick="$('.sign-groups').hide(); $('#sign-in-form').show();">I already have an account</span>

            <button type="button" class="btn btn-primary pull-right" onclick="create_account()">Create Account</button>

        </form>

    </div>
</div>

</div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modalName"></h4>
                    </div>

                    <div class="modal-body">
                    </div>

                    <div class="modal-footer" style="margin-top:0">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div class="modal fade" id="postSignInModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modalName">You've signed in!</h4>
                    </div>
                    <div class="modal-body">
                        <p>Start requesting vouchers for promo codes by clicking the <strong>Review Now</strong> buttons on products you want.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="show_last_product();">Got it!</button>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
         <script type="text/javascript">
         // setInterval(function () {document.getElementById("signinbutton").click();}, 1000);
            $(document).ready(function() {
   document.getElementById("signinbutton").click();
})
        </script>
        <script type="text/javascript">
          


            // Code for form validation
            $("#createaccount").validate({
                 errorElement: 'div',
                    validClass: 'valid has-success',
                    errorClass: 'help-block danger',
                    focusInvalid: false,
                    ignore: "",
                    rules: {
                        sign_email:{
                             email:true,
                            remote: "classes/ajax_unique.php?data=sign_email"
                        }
                                       
                    },
            
                    messages: {
                        sign_email:{
                        required:"Please enter your email",                                  
                        remote:"Email address already exists"
                        }
                    
                    },
            
            
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    },
            
                    success: function (e) {
                        $(e).closest('.form-group').removeClass('has-error').addClass('has-success');
                        $(e).add();
                    },
            
                    errorPlacement: function (error, element) {                        
                          error.insertAfter(element.parent());
                    }
            
                  
                });
  $("#Forgotpassword").validate({
                 errorElement: 'div',
                    validClass: 'valid has-success',
                    errorClass: 'help-block danger',
                    focusInvalid: false,
                    ignore: "",
                    rules: {
                        forgot_email:{
                             email:true,
                            remote: "classes/ajax_unique.php?data=forgot_email"
                        }
                                       
                    },
            
                    messages: {
                        forgot_email:{
                        required:"Please enter your email",                                  
                        remote:"Email address is not valid!"
                        }
                    
                    },
            
            
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    },
            
                    success: function (e) {
                        $(e).closest('.form-group').removeClass('has-error').addClass('has-success');
                        $(e).add();
                    },
            
                    errorPlacement: function (error, element) {                        
                          error.insertAfter(element.parent());
                    }
            
                  
                });

        </script>
       
            </body>
            </html>


 